import 'react-native-gesture-handler';
import * as React from 'react';
import {
  Image,
  View,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import styles from './src/common/styles';
import Colors from './src/common/colors';
import AsyncStorage from './src/communication/AsyncStorage';
import NetworkStatus from './src/utils/NetworkStatus';
import UserDataProvider from './src/data/provider/UserDataProvider';
import DataProxy from './src/data/DataProxy';

import LoginVideo from './src/scenes/login/LoginVideo';
import Login from './src/scenes/login/Login';
import ChooseCountry from './src/scenes/login/ChooseCountry';
import LoginEnterCode from './src/scenes/login/LoginEnterCode';
import LoginChooseDetails from './src/scenes/login/LoginChooseDetails';
import Terms from './src/scenes/login/Terms';

import Dashboard from './src/scenes/dashboard/Dashboard';
import ViewVideoMix from './src/scenes/dashboard/ViewVideoMix';
import Rate from './src/scenes/dashboard/components/Rate';

import Profile from './src/scenes/profile/Profile';
import ProfileImageUploading from './src/scenes/profile/ProfileImageUploading';
import ProfileFollowers from './src/scenes/profile/ProfileFollowers';
import ProfileOtherUser from './src/scenes/profile/ProfileOtherUser';
import ProfileEditProfile from './src/scenes/profile/ProfileEditProfile';
import ProfileEditDescription from './src/scenes/profile/ProfileEditDescription';

import MixingDesk from './src/scenes/mixingdesk/MixingDesk';
import NewMix from './src/scenes/mixingdesk/NewMix';
import MixDescription from './src/scenes/mixingdesk/MixDescription';
import NewMixChooseVid from './src/scenes/mixingdesk/NewMixChooseVid';
import NewMixAddClip from './src/scenes/mixingdesk/NewMixAddClip';
import Categories from './src/scenes/mixingdesk/Categories';
import MixTags from './src/scenes/mixingdesk/MixTags';
import NewMixUploading from './src/scenes/mixingdesk/NewMixUploading';
import EditMix from './src/scenes/mixingdesk/EditMix';
import MixOrderClips from './src/scenes/mixingdesk/MixOrderClips';

import Challenges from './src/scenes/challenges/Challenges';
import Challenge from './src/scenes/challenges/Challenge';
import ChallengeActivities from './src/scenes/challenges/ChallengeActivities';

export const AuthContext = React.createContext();

export default function App() {

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'IS_AUTHENTICATED':
          return {
            ...prevState,
            isLoading: false,
            isAuthenticated: true
          };
        case 'NOT_AUTHENTICATED':
          return {
            ...prevState,
            isLoading: false,
            isAuthenticated: false
          };
      }
    },
    {
      isLoading: true,
      isAuthenticated: false
    }
  );

  const authContext = React.useMemo(
    () => ({
      signIn: () => dispatch({ type: 'IS_AUTHENTICATED' }),
      signOut: () => dispatch({ type: 'NOT_AUTHENTICATED' }),
      signUp: async data => {
        dispatch({ type: 'IS_AUTHENTICATED' })
      }
    }), []
  );

  React.useEffect(() => {
    //console.log('checking auth state')  
    var isSubscribed = true;

    //Listen for Network Status changes
    NetworkStatus.startListening((status) => {
      //console.log('@@@@', status)
      if (isSubscribed) {
        checkForToken()
          .then((token) => {
            //console.log(token);
            DataProxy.storeToken(token);
            UserDataProvider.getUser()
              .then((userData) => {
                //console.log('userData', userData);
                if (userData) {
                  dispatch({ type: 'IS_AUTHENTICATED' });
                } else {
                  dispatch({ type: 'NOT_AUTHENTICATED' });
                }
              })
          })
          .catch((e) => console.log(e));
      }
    });

    return () => isSubscribed = false;

  }, []);

  const checkForToken = async () => {
    var token = await AsyncStorage.getToken();
    if (token) {
      return token
    } else {
      return null;
    }
  };

  const Stack = createStackNavigator();
  const Drawer = createDrawerNavigator();

  function SplashScreen() {
    return (
      <View style={[styles.mainContainer, { justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.brandGreen }]}>
        {/* <Spinner color={'#FFFFFF'} type={'WanderingCubes'} /> */}
        <Image source={require('./img/m3logo.png')} />
      </View>
    )
  }

  function AuthStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: styles.navBar,
          headerTitleStyle: styles.navBarTitle,
          headerBackTitle: null,
          headerTruncatedBackTitle: null,
          headerBackImage: () => <Image source={require('./img/btn-back.png')} style={styles.backButton} />
        }}
      >
        <Stack.Screen name="LoginVideo" component={LoginVideo} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerTitle: "ENTER A MOBILE NUMBER" }} />
        <Stack.Screen name="ChooseCountry" component={ChooseCountry} options={{ headerTitle: "CHOOSE A COUNTRY" }} />
        <Stack.Screen name="LoginEnterCode" component={LoginEnterCode} options={{ headerTitle: "ENTER VERIFICATION CODE" }} />
        <Stack.Screen name="LoginChooseDetails" component={LoginChooseDetails} options={{ headerLeft: null, headerTitle: "ENTER DETAILS" }} />
        <Stack.Screen name="Terms" component={Terms} options={{ headerTitle: "TERMS &amp; CONDITIONS" }} />
        <Stack.Screen name="LoginImageUploading" component={ProfileImageUploading} options={{ headerLeft: null, headerTitle: "UPLOADING PROFILE IMAGE" }} />
      </Stack.Navigator>
    )
  }

  function DashboardStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: styles.navBar,
          headerTitleStyle: styles.navBarTitle,
          headerBackTitle: null,
          headerTruncatedBackTitle: null,
          headerBackImage: () => <Image source={require('./img/btn-back.png')} style={styles.backButton} />
        }}
      >
        <Stack.Screen name="Dashboard" component={Dashboard} options={{ headerTitle: "PLAY" }} />
        <Stack.Screen name="ViewVideoMixDash" component={ViewVideoMix} options={{ headerTitle: "PLAY" }} />
        <Stack.Screen name="Rate" component={Rate} options={{ headerShown: false }} />
      </Stack.Navigator>
    )
  };

  function ProfileStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: styles.navBar,
          headerTitleStyle: styles.navBarTitle,
          headerBackTitle: null,
          headerTruncatedBackTitle: null,
          headerBackImage: () => <Image source={require('./img/btn-back.png')} style={styles.backButton} />
        }}
      >
        <Stack.Screen name="Profile" component={Profile} options={{ headerTitle: "TRACK" }} />
        <Stack.Screen name="ProfileFollowers" component={ProfileFollowers} options={{ headerTitle: "TRACK ME" }} />
        <Stack.Screen name="ProfileFollowing" component={ProfileFollowers} options={{ headerTitle: "I TRACK" }} />
        <Stack.Screen name="ProfileOtherUser" component={ProfileOtherUser} options={{ headerTitle: "VIEW USER" }} />
        <Stack.Screen name="ProfileEditProfile" component={ProfileEditProfile} options={{ headerTitle: "EDIT PROFILE" }} />
        <Stack.Screen name="ProfileEditDescription" component={ProfileEditDescription} options={{ headerTitle: "EDIT MY DESCRIPTION" }} />
        <Stack.Screen name="ProfileImageUploading" component={ProfileImageUploading} options={{ headerLeft: null, headerTitle: "UPLOADING PROFILE IMAGE" }} />
        <Stack.Screen name="ViewVideoMixProfile" component={ViewVideoMix} options={{ headerTitle: "PLAY" }} />

      </Stack.Navigator>
    )
  };

  function MixingDeskStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: styles.navBar,
          headerTitleStyle: styles.navBarTitle,
          headerBackTitle: null,
          headerTruncatedBackTitle: null,
          headerBackImage: () => <Image source={require('./img/btn-back.png')} style={styles.backButton} />
        }}
      >
        <Stack.Screen name="MixingDesk" component={MixingDesk} options={{ headerTitle: "MIX" }} />
        <Stack.Screen name="NewMix" component={NewMix} options={{ headerTitle: "NEW MIX" }} />
        <Stack.Screen name="MixDescription" component={MixDescription} options={{ headerTitle: "MIX DESCRIPTION" }} />
        <Stack.Screen name="NewMixChooseVid" component={NewMixChooseVid} options={{ headerTitle: "CHOOSE A VIDEO CLIP" }} />
        <Stack.Screen name="NewMixAddClip" component={NewMixAddClip} options={{ headerTitle: "ADD A VIDEO CLIP" }} />
        <Stack.Screen name="Categories" component={Categories} options={{ headerTitle: "CATEGORIES" }} />
        <Stack.Screen name="MixTags" component={MixTags} options={{ headerTitle: "TAG MIX" }} />
        <Stack.Screen name="NewMixUploading" component={NewMixUploading} options={{ headerLeft: null, headerTitle: "UPLOADING VIDEO CLIP" }} />
        <Stack.Screen name="EditMix" component={EditMix} options={{ headerTitle: "EDIT MIX" }} />
        <Stack.Screen name="MixOrderClips" component={MixOrderClips} options={{ headerTitle: "CHANGE VIDEO ORDER" }} />
      </Stack.Navigator>
    )
  };

  function ChallengesStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: styles.navBar,
          headerTitleStyle: styles.navBarTitle,
          headerBackTitle: null,
          headerTruncatedBackTitle: null,
          headerBackImage: () => <Image source={require('./img/btn-back.png')} style={styles.backButton} />
        }}
      >
        <Stack.Screen name="Challenges" component={Challenges} options={{ headerTitle: "COMPETE" }} />
        <Stack.Screen name="Challenge" component={Challenge} options={{ headerTitle: "COMPETE" }} />
        <Stack.Screen name="ChallengeActivities" component={ChallengeActivities} options={{ headerTitle: "FILTER" }} />
      </Stack.Navigator>
    )
  };

  if (state.isLoading) {
    return <SplashScreen />
  }

  return (
    <NavigationContainer>
      <AuthContext.Provider value={authContext}>
        {state.isAuthenticated ?
          <Drawer.Navigator
            initialRouteName="DashboardStack"
            drawerType='slide'
            drawerStyle={styles.drawerContainer}
            drawerContentOptions={{
              activeTintColor: '#fff',
              labelStyle: styles.drawerLink
            }}
          >
            <Drawer.Screen name='DashboardStack'
              component={DashboardStack}
              options={{
                drawerIcon: () => <Image source={require('./img/icon-nav-dashboard.png')} />,
                drawerLabel: 'PLAY',
                headerShown: false
              }} />
            <Drawer.Screen name='ProfileStack'
              component={ProfileStack}
              options={{
                drawerIcon: () => <Image source={require('./img/icon-nav-profile.png')} />,
                drawerLabel: 'TRACK',
                headerShown: false
              }} />
            <Drawer.Screen name='MixingDeskStack'
              component={MixingDeskStack}
              options={{
                drawerIcon: () => <Image source={require('./img/icon-nav-mixingdesk.png')} />,
                drawerLabel: 'MIX',
                headerShown: false
              }} />
            <Drawer.Screen name='ChallengesStack'
              component={ChallengesStack}
              options={{
                drawerIcon: () => <Image source={require('./img/icon-nav-challenges.png')} />,
                drawerLabel: 'COMPETE',
                headerShown: false
              }} />
          </Drawer.Navigator>
          :
          <Stack.Navigator>
            <Stack.Screen name='Auth' component={AuthStack} options={{ headerShown: false }} />
          </Stack.Navigator>
        }
      </AuthContext.Provider>
    </NavigationContainer>
  );
}

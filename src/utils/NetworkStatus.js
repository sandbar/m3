import NetInfo from "@react-native-community/netinfo";

var IsOnline = false;

function startListening(onComplete: ?() => boolean) {
  NetInfo.addEventListener(state => _handleConnectivityChange(state.isConnected));
  NetInfo.fetch().then(
      (state) => {
        IsOnline = state.isConnected;
        onComplete && onComplete(IsOnline);
      }
  );
};

function stopListening() {
  NetInfo.removeEventListener();
};

function _handleConnectivityChange (isConnected) {
  IsOnline = isConnected;
};

function isOnline () {
  return IsOnline;
};

exports.startListening = startListening;
exports.stopListening = stopListening;
exports.isOnline = isOnline;

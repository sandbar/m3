var TimeFormat = require('hh-mm-ss');

function formatSecondsWatched(secondsWatched) {
  //console.log(secondsWatched)
  var t = _getTime(secondsWatched);
  //console.log(t)
  if (t.length <= 5) {
    // mins/secs only
    var c = t.indexOf(':');
    // var s = t.slice(c);
    var m = t.slice(0,c);
    return '0h ' + m + 'm';
  } else {
    // we got hours
    var firstC = t.indexOf(':');
    var lastC = t.lastIndexOf(':');
    var h = t.slice(0,firstC);
    var m = t.slice(firstC + 1, lastC);
    return h + 'h ' + m + 'm';
  }

};

function fromStoM(secondsWatched) {
  var t = _getTime(secondsWatched);
  if (t.length <= 5) {
    var c = t.indexOf(':');
    var m = t.slice(0,c);
    return m + 'm';
  }
};

function fromStoH(secondsWatched) {
  var t = _getTime(secondsWatched);
  if (t.length <= 5) {
    return '0h';
  } else {
    var firstC = t.indexOf(':');
    var h = t.slice(0,firstC);
    return h + 'h';
  }
};

function _getTime(secondsWatched) {
  var t = TimeFormat.fromS(parseInt(secondsWatched));
  return t;
}

exports.formatSecondsWatched = formatSecondsWatched;
exports.fromStoM = fromStoM;
exports.fromStoH = fromStoH;

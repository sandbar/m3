var AsyncStorage = require('../communication/AsyncStorage');
//import RNFetchBlob from 'react-native-fetch-blob';
import RNFetchBlob from 'rn-fetch-blob';
let dirs = RNFetchBlob.fs.dirs;

const MY_MIXES_KEY = '@MyMixes';
var MY_MIXES_DATA;
var LOAD_QUEUE = [];
var REMOVE_QUEUE = [];

module.exports = {

  async getStoredMixes() {
    if(MY_MIXES_DATA == null) {
      MY_MIXES_DATA = await this.loadStoredMixes();
    }

    return MY_MIXES_DATA;
  },

  async loadStoredMixes() {
    var storedMixes = await AsyncStorage.getObject(MY_MIXES_KEY);
    if(storedMixes != null) {
      return storedMixes;
    } else {
      return [];
    }
  },

  async storeMixData(mixDetails, mixBreakdown, mixDuration) {
    console.log(mixDuration)
    // TMP MERGE those variables
    mixBreakdown.Ratings = mixDetails.Ratings;
    mixBreakdown.tags = mixDetails.tags;
    mixBreakdown.author = mixDetails.author;
    mixBreakdown.duration = mixDuration;

    for (var i = 0; i < mixDetails.videoClips.length; i++) {
      var videoID = mixDetails.videoClips[i].id;

      for (var j = 0; j < mixBreakdown.videoClips.length; j++) {
        if(mixBreakdown.videoClips[j].id === videoID) {
          mixBreakdown.videoClips[j].category = mixDetails.videoClips[i].category;
          break;
        }
      }
    }


    if(MY_MIXES_DATA == null) {
      MY_MIXES_DATA = await this.loadStoredMixes();
    }

    // Check if not added already
    var alreadyAdded = MY_MIXES_DATA.some(function (el) {
      return el.id === mixBreakdown.id;
    });

    if(alreadyAdded) {
      console.log("UH OH! Already stored. Overwrite?");
    } else {
      MY_MIXES_DATA.push(mixBreakdown);
    }

    // Store data
    await AsyncStorage.storeObject(MY_MIXES_KEY, MY_MIXES_DATA);


    // What about saving videos now?
    LOAD_QUEUE = mixBreakdown.videoClips.slice();
    this.checkQueue();
  },

  async checkQueue() {
    if(LOAD_QUEUE.length > 0) {
      this.downloadVideos(LOAD_QUEUE.shift());
    } else {
      console.log("ALL LOADED");
      // Update stored data
      await AsyncStorage.storeObject(MY_MIXES_KEY, MY_MIXES_DATA);
    }
  },

  downloadVideos(videoClip) {
    var fullFilePath = dirs.DocumentDir + '/Videos/' + videoClip.fileUrl;

    RNFetchBlob.fs.exists(fullFilePath)
      .then((exist) => {
        if(!exist) {

          console.log("Download:", fullFilePath);
          RNFetchBlob
            .config({
              path : fullFilePath
            })
            .fetch('GET', 'http://stream.maxmyminutes.com/live/' + videoClip.fileUrl, {
            })
            .then((res) => {
              // SUCCESS!
              videoClip.fileUrl = res.path();
              this.checkQueue();
            })

        } else {

          // ALREADY DOWNLOADED
          console.log('Already downloaded. Moving on...');
          videoClip.fileUrl = fullFilePath;
          this.checkQueue();

        }
      })
      .catch(() => {
        console.log("What am I doing here?");
      })
  },

  async removeMixData(mixBreakdown) {
    var mixes = await this.getStoredMixes();
    for (var i = 0; i < mixes.length; i++) {
      if (mixes[i].id == mixBreakdown.id) {
        mixes.splice(i,1);
      }
    }

    await AsyncStorage.storeObject(MY_MIXES_KEY, mixes);

    // remove videos
    REMOVE_QUEUE = mixBreakdown.videoClips.slice();
    this.checkRemoveQueue();
  },

  checkRemoveQueue() {
    if(REMOVE_QUEUE.length > 0) {
      this.removeVideos(REMOVE_QUEUE.shift());
    } else {
      console.log("ALL REMOVED");
    }
  },

  removeVideos(videoClip) {
    var fullFilePath = dirs.DocumentDir + '/Videos/' + videoClip.fileUrl;

    RNFetchBlob.fs.unlink(fullFilePath).then(() => {
      console.log('REMOVED:', fullFilePath);
      this.checkRemoveQueue();
    })
  },

  async checkIsSynced(mixID:number) {
    var mixes = await this.getStoredMixes();
    if(mixes.length == 0) {
      //Sry, no mixes synced
      //console.log("no mixes");
      return false;
    } else {
      for (var i = 0; i < mixes.length; i++) {
        if (mixes[i].id == mixID) {
          //console.log("MATCH");
          return true;
        } else {
          //console.log("NO MATCH");
          return false;
        }
      }
      //console.log("WHAT AM I DOING HERE?!?!");
    }
  },

}

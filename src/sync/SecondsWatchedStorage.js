var AsyncStorage = require('../communication/AsyncStorage');

const MY_SECONDS_WATCHED_KEY = '@MySecondsWatched';
var MY_SECONDS_WATCHED_DATA;

module.exports = {

  async getStoredMixes() {
    if(MY_SECONDS_WATCHED_DATA == null) {
      MY_SECONDS_WATCHED_DATA = await this.loadStoredMixes();
    }
    return MY_SECONDS_WATCHED_DATA;
  },

  async loadStoredMixes() {
    var storedMixes = await AsyncStorage.getObject(MY_SECONDS_WATCHED_KEY);
    if(storedMixes != null) {
      return storedMixes;
    } else {
      return [];
    }
  },

  async storeMixData(mixID, secondsWatched) {

    var mixData = {};

    mixData.id = mixID;
    mixData.secondsWatched = secondsWatched;

    if(MY_SECONDS_WATCHED_DATA == null) {
      MY_SECONDS_WATCHED_DATA = await this.loadStoredMixes();
    }

    console.log("MY_SECONDS_WATCHED_DATA", MY_SECONDS_WATCHED_DATA);

    // Check if not added already
    var alreadyAdded = MY_SECONDS_WATCHED_DATA.some(function (el) {
      return el.id === mixData.id;
    });

    if(alreadyAdded) {
      console.log("UH OH! Mix already stored. Adding secondsWatched...");
      for (var i = 0; i <  MY_SECONDS_WATCHED_DATA.length; i++ ) {
        if (mixData.id == MY_SECONDS_WATCHED_DATA[i].id) {
          var tmpInt = parseInt(MY_SECONDS_WATCHED_DATA[i].secondsWatched += mixData.secondsWatched);
          MY_SECONDS_WATCHED_DATA[i].secondsWatched = tmpInt;
        }
      }
    } else {
      MY_SECONDS_WATCHED_DATA.push(mixData);
    }

    // Store data
    console.log('storing:', MY_SECONDS_WATCHED_DATA);
    await AsyncStorage.storeObject(MY_SECONDS_WATCHED_KEY, MY_SECONDS_WATCHED_DATA);
  },

  async deleteStoredMix(returnedID) {
    if (MY_SECONDS_WATCHED_DATA == null) {
      MY_SECONDS_WATCHED_DATA = await this.loadStoredMixes();
    }
    for (var i = 0; i <  MY_SECONDS_WATCHED_DATA.length; i++) {
      if (returnedID == MY_SECONDS_WATCHED_DATA[i].id) {
        MY_SECONDS_WATCHED_DATA.splice(i,1);
      }
    }
    // Store data
    console.log('deleting & storing:', MY_SECONDS_WATCHED_DATA);
    await AsyncStorage.storeObject(MY_SECONDS_WATCHED_KEY, MY_SECONDS_WATCHED_DATA);
  }

}

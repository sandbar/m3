var React = require('react');
var ReactNative = require('react-native');

import {
  Actions,
  Scene,
  Router
} from 'react-native-router-flux';

var styles = require('./common/styles');

import Menu from './scenes/Menu';
import ImagePickerView from './scenes/ImagePickerView';
import VideoPlayerView from './scenes/VideoPlayer';
import Subscribe from './scenes/Subscribe/';

// LOGIN
import LoginStart from './scenes/login/LoginStart';
import LoginVideo from './scenes/login/LoginVideo';
import Login from './scenes/login/Login';
import ChooseCountry from './scenes/login/ChooseCountry';
import LoginEnterCode from './scenes/login/LoginEnterCode';
import LoginChooseDetails from './scenes/login/LoginChooseDetails';
import LoginDOB from './scenes/login/LoginDOB';
import Terms from './scenes/login/Terms';

// DRAWER
import NavigationDrawer from './scenes/Drawer/NavigationDrawer';

// PROFILE
import Profile from './scenes/profile/Profile';
import ProfileEditGoals from './scenes/profile/ProfileEditGoals';
import ProfileEditProfile from './scenes/profile/ProfileEditProfile';
import ProfileFollowers from './scenes/profile/ProfileFollowers';
import FitCheck from './scenes/profile/FitCheck';
import ProfileEditDescription from './scenes/profile/ProfileEditDescription';
import ProfileOtherUser from './scenes/profile/ProfileOtherUser';
import ProfileImageUploading from './scenes/profile/ProfileImageUploading';

// DASHBOARD
import Dashboard from './scenes/dashboard/Dashboard';
import ViewVideoMix from './scenes/dashboard/ViewVideoMix';
import Rate from './scenes/dashboard/components/Rate';
import Search from './scenes/dashboard/Search';
import SearchResults from './scenes/dashboard/SearchResults';

// MIXING DESK
import MixingDeskHint from './scenes/mixingdesk/MixingDeskHint';
import MixingDesk from './scenes/mixingdesk/MixingDesk';
import EditMix from './scenes/mixingdesk/EditMix';
import NewMix from './scenes/mixingdesk/NewMix';
import NewMixChooseVid from './scenes/mixingdesk/NewMixChooseVid';
import NewMixTag from './scenes/mixingdesk/NewMixTag';
import NewMixAddClip from './scenes/mixingdesk/NewMixAddClip';
import NewMixUploading from './scenes/mixingdesk/NewMixUploading';
import Categories from './scenes/mixingdesk/Categories';
import MixDescription from './scenes/mixingdesk/MixDescription';
import MixTags from './scenes/mixingdesk/MixTags';
import MixOrderClips from './scenes/mixingdesk/MixOrderClips';

// CHALLENGES
import Challenges from './scenes/challenges/Challenges';
import Challenge from './scenes/challenges/Challenge';
import ChallengeActivities from './scenes/challenges/ChallengeActivities';

// TMP
import CurrentUser from './scenes/tmp/CurrentUser';

var ROUTES = {
  loginStart: LoginStart,
  loginVideo: LoginVideo,
  login: Login,
  chooseCountry: ChooseCountry,
  loginEnterCode: LoginEnterCode,
  loginChooseDetails: LoginChooseDetails,
  loginDOB: LoginDOB,
  terms: Terms,

  profile: Profile,
  profileEditProfile: ProfileEditProfile,
  profileEditGoals: ProfileEditGoals,
  profileFollowers: ProfileFollowers,
  fitCheck: FitCheck,
  profileEditDescription: ProfileEditDescription,
  profileOtherUser: ProfileOtherUser,
  profileImageUploading: ProfileImageUploading,

  dashboard: Dashboard,
  subscribe: Subscribe,
  viewVideoMix: ViewVideoMix,
  rate: Rate,
  search: Search,
  searchResults: SearchResults,

  mixingDeskHint: MixingDeskHint,
  mixingDesk: MixingDesk,
  editMix: EditMix,
  newMix: NewMix,
  newMixChooseVid: NewMixChooseVid,
  newMixTag: NewMixTag,
  newMixAddClip: NewMixAddClip,
  newMixUploading: NewMixUploading,
  categories: Categories,
  mixDescription: MixDescription,
  mixTags: MixTags,
  mixOrderClips: MixOrderClips,

  challenges: Challenges,
  challenge: Challenge,
  challengeActivities: ChallengeActivities,

  currentUser: CurrentUser,
  menu: Menu,
  pickerScene: ImagePickerView,
  videoPlayerScene: VideoPlayerView
}


var Main = React.createClass({
  render: function() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="LOGIN_PART" initial
            navigationBarStyle={styles.navBar}
            titleStyle={styles.navBarTitle}
            leftButtonIconStyle={styles.leftButtonIconStyle}
            panHandlers={null}>
            <Scene key="loginStart" component={LoginStart} panHandlers={null} hideNavBar />
            <Scene key="loginVideo" component={LoginVideo} panHandlers={null}  />
            <Scene key="login" component={Login} panHandlers={null} hideNavBar={false} title="ENTER A MOBILE NUMBER" />
            <Scene key="chooseCountry" component={ChooseCountry} panHandlers={null}  title="CHOOSE A COUNTRY" />
            <Scene key="loginEnterCode" component={LoginEnterCode} panHandlers={null}  title="ENTER VERIFICATION CODE" />
            <Scene  key="loginChooseDetails"
                    component={LoginChooseDetails}
                    leftButtonIconStyle={{top:-100}}
                    onBack={() => console.log("Don't press this!")}
                    panHandlers={null}
                    title="ENTER DETAILS" />
            <Scene  key="loginImageUploading"
                    component={ProfileImageUploading}
                    title="UPLOADING PROFILE IMAGE"
                    leftButtonIconStyle={{top:-100}}
                    onBack={() => console.log("Don't press this!")}
                    panHandlers={null}
            />
            <Scene key="loginDOB" component={LoginDOB} panhandlers={null} title="ADD INFORMATION" />
            <Scene key="terms" component={Terms} panHandlers={null} title="TERMS & CONDITIONS" />
          </Scene>

          <Scene key="OTHER_PART" component={NavigationDrawer} panHandlers={null}>
            <Scene key="main">

              <Scene key="DASHBOARD" initial
                navigationBarStyle={styles.navBar}
                titleStyle={styles.navBarTitle}
                leftButtonIconStyle={styles.barButtonIconStyle}
                drawerImage={require('../img/btn-hamburger.png')}
                panHandlers={null}
              >
                <Scene  key="dashboard" component={Dashboard}
                  title="PLAY"
                  rightButtonImage={require('../img/btn-search.png')}
                  rightButtonIconStyle={styles.rightButtonIconStyle}
                  onRight={() => Actions.search()}
                  panHandlers={null}
                />
                <Scene key="search" component={Search} title="SEARCH"
                  leftButtonIconStyle={{top:-100}}
                  panHandlers={null}
                />
                <Scene key="searchResults" component={SearchResults} title="SEARCH RESULTS"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  onBack={() => {
                    Actions.pop({ popNum:2 })
                    setTimeout(() => {
                      Actions.refresh({ search:true });
                    }, 10);
                  }}
                  panHandlers={null}
                />
                <Scene key="subscribeDash" component={Subscribe} title="SUBSCRIBE TO PLUS"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  direction={'vertical'}
                  panHandlers={null}
                  hideNavBar
                />
                <Scene key="viewVideoMixDash" component={ViewVideoMix} title="PLAY"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                  hideNavBar={false}
                 />
                <Scene key="rate" component={Rate} title="RATE"
                  leftButtonIconStyle={{top:-100}}
                  panHandlers={null}
                 />
              </Scene>

              <Scene key="PROFILE"
                navigationBarStyle={styles.navBar}
                titleStyle={styles.navBarTitle}
                leftButtonIconStyle={styles.barButtonIconStyle}
                drawerImage={require('../img/btn-hamburger.png')}
                panHandlers={null}
              >
                <Scene  key="profileMain"
                        component={Profile}
                        title="TRACK"
                        rightButtonImage={require('../img/btn-edit.png')}
                        rightButtonIconStyle={styles.rightButtonIconStyle}
                        onRight={() => Actions.profileEditProfile()}
                        panHandlers={null}
                />
                <Scene  key="profileEditProfile" component={ProfileEditProfile} title="EDIT PROFILE"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  onBack={() => Actions.refresh({popMe:true})}
                  panHandlers={null}
                 />
                <Scene  key="profileEditGoals" component={ProfileEditGoals} title="EDIT GOALS"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                 />
                <Scene  key="profileFollowing" component={ProfileFollowers} title="I TRACK"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                 />
                <Scene  key="profileFollowers" component={ProfileFollowers} title="TRACK ME"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                 />
                <Scene  key="fitCheck" component={FitCheck} title="FITCHECK"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                />
                <Scene  key="profileEditDescription" component={ProfileEditDescription} title="EDIT MY DESCRIPTION"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                />
                <Scene  key="profileOtherUser" component={ProfileOtherUser} title="VIEW USER"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                />
                <Scene  key="profileImageUploading"
                        component={ProfileImageUploading}
                        title="UPLOADING PROFILE IMAGE"
                        leftButtonIconStyle={{top:-100}}
                        onBack={() => console.log("Don't press this!")}
                        panHandlers={null}
                />


                <Scene key="viewVideoMix" component={ViewVideoMix} title="PLAY"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                 />
              </Scene>

              <Scene key="MIXINGDESK"
                navigationBarStyle={styles.navBar}
                titleStyle={styles.navBarTitle}
                leftButtonIconStyle={styles.barButtonIconStyle}
                drawerImage={require('../img/btn-hamburger.png')}
                panHandlers={null}
              >
                <Scene  key="mixingDesk"
                        component={MixingDesk}
                        title="MIX"
                        rightButtonImage={require('../img/btn-add.png')}
                        rightButtonIconStyle={styles.rightButtonIconStyle}
                        onRight={() => Actions.newMix()}
                        panHandlers={null}
                />
                <Scene  key="editMix" component={EditMix} title="EDIT MIX"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                />
                <Scene  key="newMix" component={NewMix} title="NEW MIX"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                />
                <Scene  key="newMixChooseVid"
                        component={NewMixChooseVid}
                        title="CHOOSE A VIDEO CLIP"
                        leftButtonIconStyle={styles.leftButtonIconStyle}
                        rightButtonImage={require('../img/btn-camera.png')}
                        rightButtonIconStyle={styles.rightButtonIconStyle}
                        onRight={() => (Actions.refresh({pickVideo: true}))}
                        panHandlers={null}
                />
                <Scene  key="newMixAddClip"
                        component={NewMixAddClip}
                        title="ADD A VIDEO CLIP"
                        leftButtonIconStyle={styles.leftButtonIconStyle}
                        panHandlers={null}
                />
                <Scene  key="categories"
                        component={Categories}
                        title="CATEGORIES"
                        leftButtonIconStyle={styles.leftButtonIconStyle}
                        panHandlers={null}
                />
                <Scene  key="mixDescription"
                        component={MixDescription}
                        title="MIX DESCRIPTION"
                        leftButtonIconStyle={styles.leftButtonIconStyle}
                        panHandlers={null}
                />
                <Scene  key="mixTags"
                        component={MixTags}
                        title="TAG MIX"
                        leftButtonIconStyle={styles.leftButtonIconStyle}
                        panHandlers={null}
                />
                <Scene  key="newMixUploading"
                        component={NewMixUploading}
                        title="UPLOADING VIDEO CLIP"
                        leftButtonIconStyle={{top:-100}}
                        panHandlers={null}
                />
                <Scene  key="mixOrderClips"
                        component={MixOrderClips}
                        title="CHANGE VIDEO ORDER"
                        leftButtonIconStyle={styles.leftButtonIconStyle}
                        panHandlers={null}
                />
              </Scene>

              <Scene key="CHALLENGES"
                navigationBarStyle={styles.navBar}
                titleStyle={styles.navBarTitle}
                leftButtonIconStyle={styles.barButtonIconStyle}
                drawerImage={require('../img/btn-hamburger.png')}
                panHandlers={null}
              >
                <Scene  key="challenges" component={Challenges} title="COMPETE" panHandlers={null} />
                <Scene  key="challenge" component={Challenge} title="COMPETE"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                />
                <Scene  key="challengeActivities" component={ChallengeActivities} title="FILTER"
                  leftButtonIconStyle={styles.leftButtonIconStyle}
                  panHandlers={null}
                />
              </Scene>

            </Scene>
          </Scene>
        </Scene>
      </Router>
    )
  }
});

module.exports = Main;

var userData = {};
var following = [];
var followers = [];

module.exports = {
  storeUserData: function(user) {
    userData = user;
  },

  getUsername: function() {
    return userData.username;
  },

  getEmail: function() {
    return userData.email;
  },

  getId: function() {
    return userData.id;
  },

  getGender: function() {
    return userData.gender;
  },

  getDOB: function() {
    return userData.dateOfBirth;
  },

  getFollowing: function() {
    return userData.followed;
  },

  getFollowers: function() {
    return userData.followers;
  },

  getDescription: function() {
    return userData.description;
  },

  getImage: function() {
    return userData.image;
  },

  getIsPremium() {
    return userData.isPremium;
  },

  getPhoneNumber() {
    return userData.phone;
  },

  storeFollowingList: function(users) {
    following = users;
  },

  storeFollowersList: function(users) {
    followers = users;
  },

  getFollowingList: function() {
    return following;
  },

  getFollowersList: function() {
    return followers;
  },

  storeDescription: function(desc) {
    userData.description = desc;
  },

  storeImage: function(fileName) {
    userData.image = fileName;
  },

  storeIsSubscribed(isSubscribed) {
    userData.isSubscribed = isSubscribed;
  },

  getIsSubscribed() {
    return userData.isSubscribed;
  },

  getData() {
    return userData
  }
}

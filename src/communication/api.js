import RNFetchBlob from 'rn-fetch-blob';

var rootUrl = 'https://api.maxmyminutes.com:1337'
var DataProxy = require('../data/DataProxy');

var getJsonHeaders = function () {
  var token = DataProxy.getToken();
  console.log(token)
  return {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': token
  }
};

var getMultiPartHeaders = function () {
  var token = DataProxy.getToken();
  return {
    'Accept': '*/*',
    'Content-Type': 'multipart/form-data',
    'Authorization': token
  }
};

var getUnAuthenticatedJsonHeaders = function () {
  return {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
};

module.exports = {
  register: function (number) {
    var url = `${rootUrl}/register`;

    return fetch(url, {
      method: 'POST',
      headers: getUnAuthenticatedJsonHeaders(),
      body: JSON.stringify({
        phone: number
      })
    })
      .then((response) => response.json());
  },

  userUpdateSubscription: function (isSubscribed) {
    var url = `${rootUrl}/user/update`;
    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({ isSubscribed: isSubscribed })
    })
      .then((response) => response.json());
  },

  userUpdateDetails: function (username, email) {
    var url = `${rootUrl}/user/update`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({
        username: username,
        email: email
      })
    })
      .then((response) => response.json());
  },

  userUpdateDOB: function (gender, dob) {
    var url = `${rootUrl}/user/update`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({
        gender: gender,
        dateOfBirth: dob
      })
    })
      .then((response) => response.json());
  },

  userUpdateDescription: async function (description) {
    var url = `${rootUrl}/user/update`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({
        description: description
      })
    })
      .then((response) => response.json());
  },

  login: function (identifier, pin) {
    var url = `${rootUrl}/login`;

    return fetch(url, {
      method: 'POST',
      headers: getUnAuthenticatedJsonHeaders(),
      body: JSON.stringify({
        identifier: identifier,
        pin: pin
      })
    })
      .then((response) => response.json());
  },

  fbLogin: function (token, username) {
    var url = `${rootUrl}/auth/facebook/token?username=${username}&access_token=${token}`;

    return fetch(url, {
      method: 'POST',
      headers: getUnAuthenticatedJsonHeaders()
    })
      .then((response) => response.json());
  },

  logout: function () {
    var url = `${rootUrl}/logout`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  isAuthenticated() {
    var url = `${rootUrl}/isAuthenticated`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  forgotPassword(email) {
    var url = `${rootUrl}/forgotPassword?email=${email}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  user: function () {
    var url = `${rootUrl}/user`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  profile: function (id) {
    var url = `${rootUrl}/user/profile?id=${id}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  follow: function (id) {
    var url = `${rootUrl}/follow`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({
        user: id
      })
    })
      .then((response) => response.json());
  },

  unfollow: function (id) {
    var url = `${rootUrl}/unfollow`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({
        user: id
      })
    })
      .then((response) => response.json());
  },

  followings: function (id) {
    var url = `${rootUrl}/followings?id=${id}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  followers: function (id) {
    var url = `${rootUrl}/followers?id=${id}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  favourites: function () {
    var url = `${rootUrl}/favourites`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  goalsWeek: function () {
    var url = `${rootUrl}/goal?type=week`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  goalsMonth: function () {
    var url = `${rootUrl}/goal?type=month`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  /*** DASHBOARD ***/

  dashboard: function (offSet) {

    //var url = `${rootUrl}/videoMix?offSet=${offSet}&limit=10${param}`
    url = `${rootUrl}/videoMix?offset=${offSet}&limit=10&orderByViews=1`

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoMixDetails: function (id) {
    var url = `${rootUrl}/videoMix/details?id=${id}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoMixBreakdown: function (id) {
    var url = `${rootUrl}/videoMix/breakdown?id=${id}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  rate: function (mixID, rate, comm) {
    var url = `${rootUrl}/rating/create`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({
        videoMix: mixID,
        rating: rate,
        comment: comm,
      })
    })
      .then((response) => response.json());
  },

  videoMixFavourite: function (id) {
    var url = `${rootUrl}/videoMix/addToFavourite?id=${id}`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoMixUnFavourite: function (id) {
    var url = `${rootUrl}/videoMix/unfavorite?id=${id}`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoMixReport: function (id) {
    var url = `${rootUrl}/videoMix/complain?id=${id}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  sendSecondsWatched: function (secondsWatched, mixID, withView, id, responseHandler) {
    var url = `${rootUrl}/videoView/add`;
    var token = DataProxy.getToken();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.setRequestHeader("Authorization", token);
    xhr.timeout = 4000;
    xhr.ontimeout = () => {
      responseHandler("error: 1,");
    }
    xhr.onload = () => {
      console.log('onLoad ', xhr.status);
      if (xhr.status !== 200) {
        var errorMsg;
        if (!xhr.responseText) {
          errorMsg = 'Expected HTTP 200 OK response, got ' + xhr.status;
        } else {
          errorMsg = xhr.responseText;
        }
        console.log(
          'Add secondsWatched failed',
          errorMsg
        );
        return;
      }

      if (!xhr.responseText) {
        console.log(
          'Add secondsWatched failed',
          'No response payload.'
        );
        return;
      }

      if (xhr.responseText) {
        responseHandler(xhr.responseText);
        //console.log(xhr.responseText);
        return;
      }
    };

    // Prepare FormData to send
    var formData = new FormData();
    formData.append('videoMix', mixID);
    formData.append('secondsWatched', secondsWatched);
    if (withView && withView == true) {
      console.log('API sending updated play flag!!!')
      formData.append('withView', true);
    }
    if (id != null || id != undefined) {
      formData.append('id', id);
    }

    xhr.send(formData);
  },

  /*** MIXING DESK ***/

  videoMixes: function () {
    var url = `${rootUrl}/user/videoMixes`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoMixDelete: function (mixID) {
    //console.log("DELETE:", mixID)
    var url = `${rootUrl}/videoMix/delete?id=${mixID}`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoClipCreate: async function (videoPath, title, fileName, contentType, category, checkSum, progressHandler, responseHandler) {
    const url = `${rootUrl}/videoClip/create?title=${title}&categoryId=${category}&checksum=${checkSum}`;
    const token = DataProxy.getToken();

    const headers = {
      'Authorization': token,
      'Content-Type': 'multipart/form-data',
    };

    const body = [{
      name: 'video',
      type: contentType,
      filename: fileName,
      categoryId: category,
      checksum: checkSum,
      data: RNFetchBlob.wrap(videoPath)
    }];

    RNFetchBlob.config({
      count: 10,
      interval: 1
    })
      .fetch(
        'POST',
        encodeURI(url),
        headers,
        body
      )
      .uploadProgress((written, total) => {
        progressHandler(written / total);
      })
      .then((res) => {
        let status = res.info().status;
        console.log('status', status);
        if (status == 200) {
          responseHandler("");
        } else {
          const { error, msg } = res.json();
          responseHandler("error: 1," + msg);
        }
      })
      .catch((err) => {
        // error handling ..
        console.log("err", err)
      })
  },

  videoClips: function () {
    var url = `${rootUrl}/videoClip`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoClipDelete: function (clipID) {
    var url = `${rootUrl}/videoClip/delete?id=${clipID}`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  categories: function () {
    var url = `${rootUrl}/category`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  tag: function () {
    var url = `${rootUrl}/tag`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  videoMixCreate: function (title, description, tags, clips, thumb, duration, isPremium, responseHandler) {
    var url = `${rootUrl}/videoMix/create?title=${title}`;
    var token = DataProxy.getToken();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.setRequestHeader("Authorization", token);
    xhr.timeout = 4000;
    xhr.ontimeout = () => {
      responseHandler("error: 1,");
    }
    xhr.onload = () => {
      //console.log('onLoad ', xhr.status);
      if (xhr.status !== 200) {
        var errorMsg;
        if (!xhr.responseText) {
          errorMsg = 'Expected HTTP 200 OK response, got ' + xhr.status;
        } else {
          errorMsg = xhr.responseText;
        }
        console.log(
          'New Mix failed',
          errorMsg
        );
        return;
      }

      if (!xhr.responseText) {
        console.log(
          'New Mix failed',
          'No response payload.'
        );
        return;
      }

      if (xhr.responseText) {
        responseHandler(xhr.responseText);
        //console.log(xhr.responseText);
        return;
      }
    };

    // Prepare FormData to send
    var formData = new FormData();
    formData.append('title', title);
    formData.append('description', description);
    formData.append('thumb', thumb);
    formData.append('duration', duration);
    formData.append('isPremium', isPremium);
    for (i = 0; i < clips.length; i++) {
      formData.append('videoClip', clips[i]);
    }
    for (ii = 0; ii < tags.length; ii++) {
      formData.append('tag', tags[ii]);
    }

    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        //console.log('uploadProgress: ' + event.loaded / event.total);
      }
    };

    xhr.send(formData);
  },

  videoMixUpdate: function (title, description, tags, clips, thumb, duration, mixId, isPremium, responseHandler) {
    var url = `${rootUrl}/videoMix/update?title=${title}`;
    var token = DataProxy.getToken();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.setRequestHeader("Authorization", token);
    xhr.timeout = 10000;
    xhr.ontimeout = () => {
      responseHandler("error: 1,");
    }
    xhr.onload = () => {
      if (xhr.status !== 200) {
        var errorMsg;
        if (!xhr.responseText) {
          errorMsg = 'Expected HTTP 200 OK response, got ' + xhr.status;
        } else {
          errorMsg = xhr.responseText;
        }
        console.log(
          'New Mix failed',
          errorMsg
        );
        return;
      }

      if (!xhr.responseText) {
        console.log(
          'New Mix failed',
          'No response payload.'
        );
        return;
      }

      if (xhr.responseText) {
        responseHandler(xhr.responseText);
        return;
      }
    };

    // Prepare FormData to send
    var formData = new FormData();
    formData.append('id', mixId);
    formData.append('title', title);
    formData.append('description', description);
    formData.append('thumb', thumb);
    formData.append('duration', duration);
    formData.append('isPremium', isPremium);
    for (i = 0; i < clips.length; i++) {
      formData.append('videoClip', clips[i]);
      formData.append('order', i);
    }
    if (tags) {
      for (ii = 0; ii < tags.length; ii++) {
        formData.append('tag', tags[ii]);
      }
    }
    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        //console.log('uploadProgress: ' + event.loaded / event.total);
      }
    };

    xhr.send(formData);
  },

  profileImageCreate: function (imageData, fileName, contentType, progressHandler, responseHandler) {
    var url = `${rootUrl}/user/addImage`;
    var token = DataProxy.getToken();

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.setRequestHeader("Authorization", token);
    xhr.timeout = 10000;
    xhr.ontimeout = () => {
      responseHandler("error: 1,");
    }
    xhr.onload = () => {
      console.log('onLoad ', xhr.status);
      if (xhr.status !== 200) {
        var errorMsg;
        if (!xhr.responseText) {
          errorMsg = 'Expected HTTP 200 OK response, got ' + xhr.status;
        } else {
          errorMsg = xhr.responseText;
        }
        console.log(
          'Upload failed',
          errorMsg
        );
        return;
      }

      if (!xhr.responseText) {
        console.log(
          'Upload failed',
          'No response payload.'
        );
        return;
      }

      if (xhr.responseText) {
        responseHandler(xhr.responseText);
        return;
      }
    };

    // Prepare FormData to send
    var formdata = new FormData();
    formdata.append('image', { type: contentType, name: fileName, ...imageData });

    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        console.log("event", event.loaded, event.total)
        progressHandler(event.loaded / event.total);
      }
    };

    xhr.send(formdata);
  },

  /*** CHALLENGES ***/

  challenges: function () {
    var url = `${rootUrl}/challenge`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  challenge: function (id) {
    var url = `${rootUrl}/challenge/top?id=${id}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

  challengeJoin: function (id) {
    var url = `${rootUrl}/challenge/join`;

    return fetch(url, {
      method: 'POST',
      headers: getJsonHeaders(),
      body: JSON.stringify({
        id: id
      })
    })
      .then((response) => response.json());
  },

  /*** SEARCH AND RELATED VIDEOS ***/

  search: function (tagIDs, duration) {
    var qString = '';
    for (var tag in tagIDs) {
      qString += "&tag=" + tagIDs[tag];
    }
    var url = `${rootUrl}/videoMix/getByTagId?duration=${duration}&${qString}`;

    return fetch(url, {
      method: 'GET',
      headers: getJsonHeaders()
    })
      .then((response) => response.json());
  },

};

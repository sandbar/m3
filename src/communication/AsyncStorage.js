import AsyncStorage from '@react-native-community/async-storage';

module.exports = {

  async getToken() {
    return await this.getString('@MySuperStore:token');
  },

  async storeToken(token) {
    return await this.storeString('@MySuperStore:token', token);
  },


  async getObject(key) {
    const str = await this.getString(key);
    if(str != null) {
      return JSON.parse(str);
    } else {
      return null;
    }
  },

  async storeObject(key, value) {
    return await this.storeString(key, JSON.stringify(value));
  },


  async getString(key) {
    try {
      return await AsyncStorage.getItem(key);
    } catch (error) {
      console.warn('Error while reading:');
      console.warn(error);
      return null;
    }
  },

  async storeString(key, value) {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      console.warn('Error while saving:');
      console.warn(error);
    }
  },

  async removeToken() {
    try {
      return await AsyncStorage.removeItem('@MySuperStore:token')
    } catch (e) {
      console.log('ERROR DELETING', e);
      return null;
    }
  }

}

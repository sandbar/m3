import React from 'react';
import {
  Text,
  View
} from 'react-native';

import Button from '../../common/button';
var styles = require('../../common/styles');
var API = require('../../communication/api');
var UserProxy = require('../../communication/UserProxy');

module.exports = React.createClass({
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={{justifyContent: 'center', marginLeft: 24, marginTop: 24}}>
          <View>
            <Text style={{fontWeight: 'bold'}}>Email:</Text>
            <Text>{UserProxy.getEmail()}</Text>
            <Text></Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold'}}>Username:</Text>
            <Text>{UserProxy.getUsername()}</Text>
            <Text></Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold'}}>ID:</Text>
            <Text>{UserProxy.getId()}</Text>
            <Text></Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold'}}>Gender:</Text>
            <Text>{UserProxy.getGender()}</Text>
            <Text></Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold'}}>Date of Birth:</Text>
            <Text>{UserProxy.getDOB()}</Text>
            <Text></Text>
          </View>
        </View>
        <View style={{marginBottom: 250}}>
        </View>
        <Button
          text={'LOGOUT'}
          onPress={this.onLogoutPress}
        />
      </View>
  )},

  onLogoutPress() {
    API.logout(UserProxy.getEmail())
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          // console.log(responseJson);
          UserProxy.storeUserData({});
          this.props.navigator.popToTop(0);
        } else {
          // Unknown Registration error
          console.log('UNKNOWN ERROR:');
          console.warn(responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:');
        console.warn(error);
        this.setState({loading: false});
      });
  }
});

import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList
} from 'react-native';

import API from '../../communication/api';
import styles from '../../common/styles';

const ChallengeActitivies = ({navigation, route}) => {

  const [activityChosen, setActivityChosen] = useState(0);
  const [filterID, setFilterID] = useState(0);
  const [allTags, setAllTags] = useState([]);

  useEffect(() => {
    API.tag()
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          console.log(responseJson);
          setAllTags(responseJson.data)
          //this.parseResponse(responseJson.data);
        } else {
          // Unknown Registration error
          console.log('TAGS: UNKNOWN ERROR:');
          console.warn(responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('TAGS: ERROR CATCH:');
        console.warn(error);
      });
  },[]);

  useEffect(() => {
    if (route.params?.filterID) {
      setFilterID(route.params?.filterID);
    }
  },[route.params?.filterID])

  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>
        <TouchableOpacity
          onPress={() => pressRow({id:-1})}
          style={[styles.listElement, {height:60}]}
          >
          <View style={styles.listElementInnerLeft}>
            <Text style={styles.mediumText}>All activities</Text>
          </View>
          <View style={[styles.listElementRight, {opacity: getTickOpacity(-1)}]}>
            <Image source={require('../../../img/icon-tick.png')} style={styles.listArrow} />
          </View>
        </TouchableOpacity>

        { drawList() }

      </View>
    </View>
  )

  function drawList() {
    if (allTags.length > 0) {
      return (
        <FlatList
          data={allTags}
          style = {styles.list}
          keyExtractor={(item, index) => index.toString()}
          renderItem = {({item, index}) => (
            <TouchableOpacity
              onPress={() => pressRow(item)}
            >
              <View  style={[styles.listElement, { height: 60 }]}>
                <View style={styles.listElementInnerLeft}>
                  <Text style={styles.mediumText}>{item.name}</Text>
                </View>
                <View style={[styles.listElementRight, {opacity: getTickOpacity(item.id)}]}>
                  <Image source={require('../../../img/icon-tick.png')} style={styles.listArrow} />
                </View>
              </View>
            </TouchableOpacity>                
          )}
        />
      )
    } else {
      return (
        <View style={{flex:1, justifyContent:'center', alignItems: 'center'}}>
          <Text style={styles.defaultText}>No activities to show</Text>
        </View>
      )
    }  
  };

  function pressRow(activityID) {
    setActivityChosen(activityID);
    navigation.navigate('Challenges', { filter: activityID });
  };

  function getTickOpacity(categoryID) {
    if (filterID) {
      if (filterID == categoryID) {
        return 1;
      }
    }
    return 0;
  };  

}
export default ChallengeActitivies;


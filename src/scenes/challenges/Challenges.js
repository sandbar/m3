import React, {useState, useEffect, useLayoutEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator
} from 'react-native';

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';
import NetworkStatus from '../../utils/NetworkStatus';

const Challenges = ({navigation, route}) => {

  const [challenges, setChallenges] = useState(null);
  const [loading, setLoading] = useState(false);
  const [filter, setFilter] = useState('All activities');
  const [filterID, setFilterID] = useState(-1); 

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          style={{left:20}}
          onPress={() => navigation.toggleDrawer()}
        >
          <Image source={require('../../../img/btn-hamburger.png')} />
        </TouchableOpacity>
      ),
    });    
  },[navigation]);  

  useEffect(() => {
    let isSubscribed = true;
    if (NetworkStatus.isOnline()) {
      API.challenges()
      .then((responseJson) => {
        if (isSubscribed) {
          if(responseJson.error == 0) {
            // SUCCESS
            //console.log(responseJson.data);
            setLoading(false);
            setChallenges(responseJson.data);
          } else {
            // Unknown Registration error
            console.log('CHALLENGES: UNKNOWN ERROR:');
            console.warn(responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('CHALLENGES: ERROR CATCH:');
        console.warn(error);
      });      
    }
    return () => isSubscribed = false;
  },[]);

  useEffect(() => {
    let isSubscribed = true;
    if (route.params?.filter) {
      if (route.params?.filter.id == -1) {
        setFilter('All activities');
        setFilterID(-1);
        //this.sendRequest();
        API.challenges()
        .then((responseJson) => {
          if (isSubscribed) {
            if(responseJson.error == 0) {
              // SUCCESS
              //console.log(responseJson.data);
              setLoading(false);
              isSubscribed && setChallenges(responseJson.data);
            } else {
              // Unknown Registration error
              console.log('CHALLENGES: UNKNOWN ERROR:');
              console.warn(responseJson);
            }
          }
        })
        .catch((error) => {
          // Request sending/parsing error
          console.log('CHALLENGES: ERROR CATCH:');
          console.warn(error);
        }); 
      } else {
        setFilter(route.params?.filter.name);
        setFilterID(route.params?.filter.id);
        ///this.filterChallenges(nextProps.filter.id);
        let filteredChallenges = [];
        for (var i = 0; i < challenges.length; i++) {
          if (route.params?.filter.id == challenges[i].id) {
            filteredChallenges.push(challenges[i]);
          }
        }
        isSubscribed && setChallenges(filteredChallenges);   
      }
    }
    return () => isSubscribed = false;
  },[route.params?.filter]);

  const pressRow = (challengeID, tagName, title, description, thumb) => {
    navigation.navigate('Challenge', {
      challengeID: challengeID, 
      tagName: tagName, 
      title: title, 
      description: description, 
      thumb: thumb
    })
  };

  const getThumb = (thumb) => {
    return require('../../../img/icon-challenge-default.png')
    /*if (thumb == null) {
      return (
        require('../../../img/icon-challenge-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb + ".png"}
      )
    }*/
  };

  const showFilters = () => {
    navigation.navigate('ChallengeActivities', {filterID:filterID})
  };  

  if (NetworkStatus.isOnline()) {
    if (loading) {
      return (
        <View style={styles.mainContainerUnderHeader}>
          <View style={styles.paddedContainer}>
            <View style={styles.loadingContainer}>
              <ActivityIndicator size='small' color={Colors.brandGreen}/>
            </View>
          </View>
        </View>        
      )
    }
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.paddedContainer}>
          <TouchableOpacity style={styles.newMixElement} onPress={() => showFilters()}>
            <View style={styles.listElementLeft}>
            <Text numberOfLines={1} style={styles.mediumText}>{filter}</Text>
            </View>
            <View style={styles.listElementRight}>
              <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
            </View>
          </TouchableOpacity>

          {challenges ?
            <FlatList
              data={challenges}
              style = {styles.challengeList}
              keyExtractor={(index) => index.toString()}
              renderItem={({item, index}) => (
                <TouchableOpacity onPress={() => pressRow(item.id, item.Tag.name, item.title, item.description, item.thumb)}>
                  <View style={styles.challengeContainer}>
                    <Image
                      source={getThumb(item.thumb)}
                      style={styles.challengeThumb}
                    />
                    <View>
                      <Text style={styles.challengeActivity}>{item.Tag.name}</Text>
                      <Text style={styles.challengeTitle}>{item.title}</Text>
                      <Text style={styles.smallText}>{item.description}</Text>
                    </View>
                  </View>
                </TouchableOpacity>    
              )}
            />
            :
            <View style={[styles.loadingContainer, {marginTop: 20}]}>
              <Text style={styles.smallText}>There are no challenges for this activity</Text>
            </View>
          }

        </View>
      </View>
    )
  } else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.loadingContainer}>
          <View style={{flexDirection:'row', marginTop:10}}>
            <Image source={require('../../../img/icon-offline.png')} />
            <Text style={styles.offlineText}>Not available offline</Text>
          </View>
        </View>
      </View>
    )
  }

};
export default Challenges;

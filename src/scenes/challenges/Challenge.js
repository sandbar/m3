import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Animated,
  ActivityIndicator
} from 'react-native';

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';
import FormatData from '../../utils/FormatData';

const Challenge = ({route}) => {

  const [loading, setLoading] = useState(true);
  const [challengeData, setChallengeData] = useState([]);
  const [challengeList, setChallengeList] = useState([]);
  const [refreshData, setRefreshData] = useState(false);

  const challengeDescriptionAnim = new Animated.Value();
  const challengeDescriptionHeight = 100;
  const arrowRotation = '90deg';
  const {challengeID} = route.params;

  useEffect(() => {
    let isSubscribed = true;
    API.challenge(this.state.challengeID)
    .then((responseJson) => {
      if (isSubscribed) {
        if (responseJson.error == 0) {
          // SUCCESS
          // var tmpData = [];
          // for (var i = 0; i < responseJson.data.topUsers.length; i++) {
          //   tmpData.push(responseJson.data.topUsers[i]);
          // }          
          setChallengeData(challengeData); 
          setChallengeList(responseJson.data.topUsers);        
          setLoading(false);
          //setChallengeTotalUsers(challengeData.totalCount);
          //isMember: challengeData.isMember,
        } else {
          // Unknown Registration error
          console.log('CHALLENGE: UNKNOWN ERROR:', responseJson);
        }
      }
    })
    .catch((error) => {
      // Request sending/parsing error
      console.log('CHALLENGE: ERROR CATCH:', error);
    });
    return () => isSubscribed = false;
  },[refreshData]);

  const handleJoin = () => {
    API.challengeJoin(challengeID)
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          //console.log(responseJson);
          setRefreshData(true);
        } else {
          // Unknown Registration error
          console.log('CHALLENGE JOIN: UNKNOWN ERROR:', responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('CHALLENGE JOIN: ERROR CATCH:', error);
      });
  };  

  if (loading) {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.paddedContainer}>
          <View style={styles.loadingContainer}>
            <ActivityIndicator size='small' color={Colors.brandGreen}/>
          </View>
        </View>
      </View>
    )
  }
  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.challengeHeader}>
        <View style={{flexDirection:'row', alignItems: 'center'}}>
          <Image
            source={require('../../../img/icon-challenge-default.png')}
            style={styles.challengeThumb}
          />
          <View>
            <Text style={styles.challengeActivity}>{this.props.tagName}</Text>
            <Text style={styles.challengeTitle}>{this.props.title}</Text>
            <View style={{flexDirection:'row', alignItems:'center'}}>
              <Image source={require('../../../img/icon-challenge-members.png')} />
              <Text style={[styles.smallText, {marginLeft:4}]}>{challengeData.totalCount}</Text>
            </View>
          </View>
        </View>
        <Animated.View style={[styles.challengeDescription, {height:challengeDescriptionAnim}]}>
          {/*
          <TouchableOpacity
            onPress={() => this.onArrowPress()}
            style={[styles.challengeDescriptionArrow, {transform: [{rotate: this.state.arrowRotation}]}]}>
            <Image source={require('../../../img/icon-arrow.png')} />
          </TouchableOpacity>
          */}
          <Text style={styles.challengeDescriptionText}>{this.props.description}</Text>
        </Animated.View>
        {challengeData.isMember ?
          <View style={styles.challengeJoinedButton} >
            <Text style={styles.challengeJoinedButtonText}>JOINED</Text>
          </View>
          :
          <TouchableOpacity style={styles.challengeJoinButton} onPress={() => handleJoin()}>
            <Text style={styles.challengeJoinButtonText}>JOIN</Text>
          </TouchableOpacity>
        }
      </View>
      <View style={[styles.paddedContainer, {paddingTop:0}]}>
        {challengeList.length > 0 ?
          <FlatList
            data={challengeList}
            renderItem={(item, index) => {
              <TouchableOpacity style={styles.challengeElement} >
                <View style={styles.challengeNumCircle}>
                  <Text style={styles.challengeNum} numberOfLines={1}>{parseInt(index) + 1}</Text>
                </View>
                <Image
                  source={this.getAvatar(item.image)}
                  style={styles.challengeAvatar}
                />
                <View>
                  <Text style={styles.challenger}>{item.username}</Text>
                  <View style={styles.challengeInfoContainer}>
                    <Text style={styles.challengeInfo}>{FormatData.formatSecondsWatched(item.time)}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            }}
          />
          :
          <View style={styles.loadingContainer}>
            <Text style={styles.defaultText}>No leaderboard yet! Play now to get a head start!</Text>
          </View>
        }
      </View>
    </View>
  );

  // const getAvatar = (thumb) => {
  //   if (thumb == null) {
  //     return (
  //       require('../../../img/avatar-default.png')
  //     )
  //   } else {
  //     return (
  //       {uri: "http://stream.maxmyminutes.com/users-images/" + thumb}
  //     )
  //   }
  // };


  // onArrowPress() {
  //     /*let initialValue    = 30,
  //         finalValue      = 100;

  //     this.state.challengeDescriptionAnim.setValue(initialValue);
  //     Animated.spring(
  //         this.state.challengeDescriptionAnim,
  //         {
  //             toValue: finalValue
  //         }
  //     ).start();*/
  // },

};

export default Challenge;

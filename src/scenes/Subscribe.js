import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
  Alert,
  NativeModules,
  Linking
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

const { InAppUtils } = NativeModules
//import iapReceiptValidator from 'iap-receipt-validator';
const InAppBilling = require("react-native-billing");

import ButtonTransparent from '../common/ButtonTransparent';
import UserProxy from '../communication/UserProxy';
import styles from '../common/styles';
import API from '../communication/api';

const Subscribe = (props) => {

  const {requestClose, mixID, navigation} = props;
  const [loading, setLoading] = useState(false);
  const [description, setDescription] = useState('Loading...');
  const [priceText, setPriceText] = useState('');
  let [hasSubscribed, setHasSubscribed] = useState(UserProxy.getIsSubscribed());
  const termsURL = 'https://maxmyminutes.com/terms/';
  const privacyURL = 'https://maxmyminutes.com/privacy/';

  useEffect(() => {
    let isSubscribed = true;
    if (Platform.OS === 'ios') {
      InAppUtils.canMakePayments((canMakePayments) => {
         if (!canMakePayments) {
            Alert.alert('Not Allowed', 'This device is not allowed to make purchases. Please check restrictions on device.');
         } else {
           var products = [
              'com.maxmyminutes.subscription.1m',
           ];
           InAppUtils.loadProducts(products, (error, products) => {
             if (isSubscribed) {
              //update store here.
              console.log(error, products);
              setPriceText(products[0].priceString);
              setDescription(products[0].description);
             }
           });
         }
      })
    } else {
      InAppBilling.open()
        .then(() => InAppBilling.getSubscriptionDetails("subscription.1m"))
        .then((details) => {
          if (isSubscribed) {
            console.log(details);
            setPriceText(details.priceText);
            setDescription(details.description);
            return InAppBilling.close();
          }
        }, (error) => {
          InAppBilling.close();
          console.log(error)
        });
    }
    return () => isSubscribed = false;
  },[]);

  const onSubscribePress = () => {
    setDescription('Loading...');
    setLoading(true);
    if (Platform.OS === 'ios') {
      const productIdentifier = 'com.maxmyminutes.subscription.1m';
      InAppUtils.purchaseProduct(productIdentifier, (error, response) => {
         if (response && response.productIdentifier) {
            // Alert.alert('Purchase Successful', 'Your Transaction ID is ' + response.transactionIdentifier);
            console.log('Purchase Successful', 'Your Transaction ID is ' + response.transactionIdentifier);
            handleSubscriptionSuccess();
         } else {
           console.log(error)
         }
      });
    } else {
      InAppBilling.open()
        .then(() => InAppBilling.subscribe('subscription.1m'))
        .then((details) => {
          console.log(details);
          handleSubscriptionSuccess();
          return InAppBilling.close();
        }, (error) => {
          console.log(error)
          InAppBilling.close();
          //Alert.alert(error);
        });
    }
  };

  const handleSubscriptionSuccess = () => {
    UserProxy.storeIsSubscribed(true);
    sendSubscriptionStatus(true);
    setHasSubscribed(true);
    setPriceText(null);
    setDescription('You have now subscribed to M:3 Plus');
    setLoading(false);
  };

  const onContinuePress = () => {
    requestClose();
    // if we're in the Dashboard, show a video
    mixID && navigation.navigate('ViewVideoMixDash', {mixID: mixID});
  };

  const restorePurchaseiOS = () => {
    // console.log('restore purchases')
    setLoading(true);
    InAppUtils.restorePurchases((error, response) => {
      if (error) {
        Alert.alert('iTunes Error', 'Could not connect to iTunes store.');
        setLoading(false);
      } else {
        if (response.length === 0) {
          Alert.alert('No Purchases', "We didn't find any purchases to restore.");
          setLoading(false);
          return;
        }
        response.forEach((purchase, i) => {
          if (purchase.productIdentifier === 'com.maxmyminutes.subscription.1m') {
            //console.log('purchase found', i, response.length);
            //var finalPurchase;
            //i === response.length - 1 ? finalPurchase = true : finalPurchase = false
            if (UserProxy.getIsSubscribed()) {
              console.log('Subscribed')
              Alert.alert('Restore Successful', 'Successfully restores all your purchases.');
              handleSubscriptionSuccess();
            } else {
              Alert.alert('Restore Unsuccessful', 'Looks like your subscription has ended!');
              setLoading(false);
              return;
            }
            return;
          }
        });
       }
    });
  };

  const sendSubscriptionStatus = async (status) => {
    return API.userUpdateSubscription(status)
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.error == 0) { return true } else { return false }
      })
      .catch((error) => { return false } )
  };
  
  return (
    <View style={styles.mainContainer}>
      <LinearGradient
        colors={['#22908f','#5bd1aa']}
        start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 1 }}
        style={{flex:1}}>
        <View style={[styles.paddedContainer, {alignItems: 'center'}]}>
          <TouchableOpacity
            onPress={() => requestClose()}
            hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}
            style={styles.modalClose}>
            <Image
              source={require('../../img/btn-close.png')}
              style={{width: 10, height: 10}}/>
          </TouchableOpacity>
          <Image source={require('../../img/subscribe-splash.png')} style={{marginTop: 180}} />
          <Text
            style={[styles.header1,
              { opacity: hasSubscribed ? 1 : 0,
                marginTop: hasSubscribed ? 70 : 0,
                fontSize: 18,
                backgroundColor: 'transparent',
                color: '#ffffff' }]}>
            Subscription success!
          </Text>
          <Text
            style={[styles.mediumText,
              { fontSize: 18,
                textAlign: 'center',
                lineHeight: 30,
                marginTop: hasSubscribed ? 10 : 50,
                backgroundColor: 'transparent',
                color: '#ffffff' }]}>
            {description}
          </Text>
        </View>
        <View style={styles.subscribeBottomContainer}>
          <Text
            style={[styles.header1,
              { marginBottom: 10,
                backgroundColor: 'transparent',
                color: '#ffffff',
                opacity: priceText ? 1 : 0 }]}>
            {priceText} per month
          </Text>
          <ButtonTransparent
            text={hasSubscribed ? 'CONTINUE' : 'SUBSCRIBE TO PLUS'}
            onPress={hasSubscribed ? () => onContinuePress() : () => onSubscribePress()}
            opacity={loading ? 0.5 : 1}
            disabled={loading ? true : false}
            stretch={'stretch'}
            />
          <TouchableOpacity
            onPress={() => restorePurchaseiOS()}
            disabled={loading ? true : false}
            >
            <Text
              style={[styles.defaultText,
                { backgroundColor: 'transparent',
                  opacity:Platform.OS === 'ios' && !hasSubscribed && !loading ? 1 : 0,
                  color: '#fff',
                  marginTop:20,
                  marginBottom: 20 }]}>
              Restore purchases
            </Text>
          </TouchableOpacity>
          <Text style={[styles.defaultText, {marginTop: 15, fontSize: 12, color: '#fff'}]}>
            Subscriptions will be charged to your credit card through your iTunes account. Your subscription will automatically renew unless cancelled at least 24 hours before the end of the current period. You will not be able to cancel the subscription once activated. Manage your subscriptions in Account Settings after purchase.
          </Text>
          <TouchableOpacity
            style={{height: 30}}
            onPress={() => handleShowURL(termsURL)}>
              <Text style={[styles.defaultText, {marginTop: 15, fontSize: 12, flex: 1, textAlign: 'center', color: '#fff'}]}>
                Terms of Service
              </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{height: 30}}
            onPress={() => handleShowURL(privacyURL)}>
              <Text style={[styles.defaultText, {marginTop: 15, fontSize: 12, flex: 1, textAlign: 'center', color: '#fff'}]}>
                Privacy Policy
              </Text>
          </TouchableOpacity>         
        </View>
      </LinearGradient>
    </View>
  );

  async function handleShowURL(url) {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    }
  }  

};

export default Subscribe;
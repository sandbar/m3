import React, {useEffect, useState, useContext} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  Keyboard,
  LayoutAnimation,
  SafeAreaView,
  Platform,
  Linking
} from 'react-native';

import ImagePicker from 'react-native-image-picker';
import {AuthContext} from '../../../App';

import Colors from '../../common/colors';
import Button from '../../common/button';
import LabelButton from '../../common/labelbutton';
import UserProxy from '../../communication/UserProxy';
import DataProxy from '../../data/DataProxy';
import AsyncStorage from '../../communication/AsyncStorage';
import styles from '../../common/styles';
import API from '../../communication/api';

const LoginChooseDetails = ({navigation, route}) => {

  const { signIn } = useContext(AuthContext);
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [usernameError, setUsernameError] = useState('');
  const [keyboardOffset, setKeyboardOffset] = useState(0);
  const [imageData, setImageData] = useState(require('../../../img/avatar-default.png'));
  const [loading, setLoading] = useState(false);
  const termsURL = 'https://maxmyminutes.com/terms/';

  let androidOffset = Platform.OS === 'ios' ? 0 : 220;

  useEffect(() => {
    let isSubscribed = true
    if (isSubscribed && route.params?.refreshView) {
      setImageData({imageData: UserProxy.getImage() == null ? require('../../../img/avatar-default.png') : {uri: "http://stream.maxmyminutes.com/users-images/" + UserProxy.getImage()}});
    }      
    return () => isSubscribed = false;
  }, [route.params?.refreshView]);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => {
      setKeyboardOffset(e.endCoordinates.height - androidOffset);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);      
    });
    return () => keyboardDidShowListener.remove();
  }, [Keyboard]);

  useEffect(() => {
    const keyboardHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardOffset(0);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);      
    });
    return () => keyboardHideListener.remove();
  }, [Keyboard]);  


  const getErrorContainer = (errorMsg) => {
    if(errorMsg) {
      return (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{errorMsg}</Text>
          <Image source={require('../../../img/icon-error.png')} />
        </View>
      );
    }
    else {
      return null;
    }
  };

  const getErrorStyle = (errorMsg) => {
    return errorMsg ? styles.textInputError : styles.textInput;
  };

  const getErrorInputStyle = (errorMsg) => {
    return errorMsg ? styles.inputFieldError : styles.inputField;
  };

  const onNextPress = () => {
    var areFieldsValid = validateFields();
    if (areFieldsValid) {
      setLoading(false);
      updateDetails();
    }
  };

  const updateDetails = () => {
    API.userUpdateDetails(username, email)
    .then((responseJson) => {
      console.log(responseJson);
      if(responseJson.error == 0) {
        AsyncStorage.storeToken(DataProxy.getToken())
        .then(() => signIn())
        .catch(e => console.log(e));
      } else {
        setEmailError('Email address invalid');
        setUsernameError('Username invalid');
      }
    })
    .catch((error) => {
      // Request sending/parsing error
      console.log('ERROR CATCH:');
      //console.warn(error);
    })
  };

  const validateFields = () => {
    var usernameValid = isUsernameValid();
    var emailValid = isEmailValid();
    // Show error messages (if any)
    setEmailError(emailValid ? null : 'Please enter a valid email address');
    setUsernameError(usernameValid ? null : 'Please enter a minimum of 3 characters');
    return emailValid && usernameValid;
  };

  const isEmailValid = () => {
    if (
      email.length > 4 &&
      email.indexOf('@') > -1 &&
      email.lastIndexOf('.') > email.indexOf('@')
    ) {
      return true;
    }
    return false;
  };

  const isUsernameValid = () => {
    if (username.length >= 1) {
      return true;
    }
    return false;
  };

  /** IMAGE HANDLING **/

  const selectPhotoTapped = () => {
    const options = {
      title: 'Photo Picker',
      takePhotoButtonTitle: 'Take Photo...',
      chooseFromLibraryButtonTitle: 'Choose from Library...',
      quality: 0.5,
      maxWidth: 300,
      maxHeight: 300,
      allowsEditing: false,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source;

        // You can display the image using either:
        //source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

        // Or:
        if (Platform.OS === 'android') {
          source = {uri: response.uri, isStatic: true};
        } else {
          source = {uri: response.uri.replace('file://', ''), isStatic: true};
        }

        // Upload image
        uploadImageToServer(response);
      }
    });
  };

  const uploadImageToServer = (response) => {
    //var fileName = response.uri.replace(/^.*[\\\/]/, '');
    var contentType = 'image/jpg';
    navigation.navigate('LoginImageUploading', {imageData: response, contentType: contentType, returnView: 'LoginChooseDetails'})
  };

  return (
    <SafeAreaView style={styles.mainContainerUnderHeader}>

      {/* <View style={[styles.profileHeader, {height: 130}]}>
        <View style={styles.paddedContainer}>
          <TouchableOpacity onPress={() => selectPhotoTapped()} style={{flex: 1, flexDirection: 'row', alignItems: 'center',}}>
            <Image style={styles.profileAvatarImage} source={imageData} />
            <Text style={[styles.mediumText, {marginLeft:10,marginRight:10}]}>Add an image</Text>
            <Image source={require('../../../img/icon-arrow.png')} />
          </TouchableOpacity>
        </View>
      </View> */}

      <View style={styles.paddedContainer}>

        <View style={getErrorStyle(usernameError)}>
          <TextInput
            style={getErrorInputStyle(usernameError)}
            placeholder={'Choose username'}
            placeholderTextColor={Colors.defaultInputText}
            value={username}
            onChangeText={(text) => {setUsername(text); setUsernameError(null); setLoading(false)}}
            returnKeyType='next'
            //onSubmitEditing={() => this.onSignInPress()}
            underlineColorAndroid='transparent'
            autoFocus
          />
        </View>
        {getErrorContainer(usernameError)}

        <View style={{height:20}} />

        <View style={getErrorStyle(emailError)}>
          <TextInput
            style={getErrorInputStyle(emailError)}
            placeholder={'Enter email address'}
            placeholderTextColor={Colors.defaultInputText}
            value={email}
            onChangeText={(text) => {setEmail(text); setEmailError(null); setLoading(false)}}
            returnKeyType='next'
            keyboardType={'email-address'}
            //onSubmitEditing={() => this.onSignInPress()}
            underlineColorAndroid='transparent'
          />
        </View>
        {getErrorContainer(emailError)}

        <View style={[styles.loginBottomContainer, {bottom: keyboardOffset}]}>
          <View style={styles.tsandcsTextContainer}>
            <Text style={[styles.mediumText, {marginBottom:5}]}>By continuing, I agree to the</Text>
            <LabelButton 
              text={'Terms & Conditions'} 
              onPress={() => handleShowURL(termsURL)} 
              btnStyle={styles.buttonText} 
            />
          </View>
          <Button
            text={'NEXT'}
            onPress={onNextPress}
            opacity={loading ? 0.5 : 1}
            disabled={loading ? true : false}
            stretch={'stretch'}
          />
        </View>

      </View>

    </SafeAreaView>
  )

  async function handleShowURL(url) {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    }
  }   

}

export default LoginChooseDetails;

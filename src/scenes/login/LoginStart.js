import React from 'react';
import {
  View,
  Text
} from 'react-native';

import {Actions} from 'react-native-router-flux';
import Colors from '../../common/colors';
var Orientation = require('react-native-orientation');
var Spinner = require('react-native-spinkit');
var styles = require('../../common/styles');
var UserDataProvider = require('../../data/provider/UserDataProvider');

module.exports = React.createClass({

  componentDidMount() {
    Orientation.lockToPortrait();
    setTimeout(this.getUser, 1000);
  },

  async getUser() {
    UserDataProvider.getUser()
      .then((userData) => {
        if(userData) {
          Actions.OTHER_PART();
        } else {
          Actions.loginVideo();
        }
      })
  },

  render() {
    return (
      <View style={[styles.mainContainer, {justifyContent:'center', alignItems:'center', backgroundColor:Colors.brandGreen}]}>
        <Spinner color={'#FFFFFF'} type={'WanderingCubes'} />
      </View>
    )
  }

});

import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  Keyboard,
  LayoutAnimation,
  SafeAreaView,
  Platform
} from 'react-native';

import {AuthContext} from '../../../App';

import Button from '../../common/button';
import styles  from '../../common/styles';
import API from '../../communication/api';
import UserProxy from '../../communication/UserProxy';
import DataProxy from '../../data/DataProxy';
import AsyncStorage from '../../communication/AsyncStorage';

const LoginEnterCode = ({navigation, route}) => {

  const {data, countryCode, mobileNumber} = route.params;
  const { signIn } = useContext(AuthContext);
  const [pinRequired, setPinRequired] = useState(data.pin);
  const [keyboardOffset, setKeyboardOffset] = useState(0);
  const [pinError, setPinError] = useState(false);
  const [code1, setCode1] = useState('');
  const [code2, setCode2] = useState('');
  const [code3, setCode3] = useState('');
  const [code4, setCode4] = useState('');
  const [code5, setCode5] = useState('');
  const [code6, setCode6] = useState('');
  const [loading, setLoading] = useState(false);
  const input1 = useRef(null);
  const input2 = useRef(null);
  const input3 = useRef(null);
  const input4 = useRef(null);
  const input5 = useRef(null);
  const input6 = useRef(null);

  let androidOffset = Platform.OS === 'ios' ? 0 : 220;

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => {
      setKeyboardOffset(e.endCoordinates.height - androidOffset);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);      
    });
    return () => keyboardDidShowListener.remove();
  }, [Keyboard]);

  useEffect(() => {
    const keyboardHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardOffset(0);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);      
    });
    return () => keyboardHideListener.remove();
  }, [Keyboard]);


  const getErrorContainer = (errorMsg) => {
    if(errorMsg) {
      return (
        <View style={[styles.errorContainer, {marginBottom:20}]}>
          <Text style={styles.errorText}>{errorMsg}</Text>
          <Image source={require('../../../img/icon-error.png')} />
        </View>
      );
    }
    else {
      return null;
    }
  };

  const getErrorStyle = (errorMsg) => {
    return errorMsg ? styles.textInputError : styles.textInput;
  };

  const getErrorInputStyle = (errorMsg) => {
    return errorMsg ? styles.inputFieldError : styles.inputField;
  };

  const focusNextField = (nextField) => {
    switch (nextField) {
      case '1' : input1.current.focus(); break;
      case '2' : input2.current.focus(); break;
      case '3' : input3.current.focus(); break;
      case '4' : input4.current.focus(); break;
      case '5' : input5.current.focus(); break;
      case '6' : input6.current.focus(); break;
    }
  };

  const checkCodes = (finalDigit) => {

    //debug
    //navigation.navigate('LoginChooseDetails');

    const pinEntered = code1 + code2 + code3 + code4 + code5 + finalDigit;

    console.log("checking", pinEntered, pinRequired);

    if ( pinEntered == pinRequired ) {
      setLoading(true);
      sendLoginRequest();
    } else {
      // show pin match error
      setPinError('Incorrect PIN, please try again!');
    }
  };

  const onResendCodePress = () => {
    //console.log('resend code');
    setLoading(true);
    API.register(countryCode + mobileNumber)
    .then((responseJson) => {
      console.log(responseJson);
      if (responseJson.error == 0) {
        focusNextField('1');
        setCode1('');
        setCode2('');
        setCode3('');
        setCode4('');
        setCode5('');
        setCode6('');
        setPinError(null);
        setPinRequired(responseJson.data.pin);
        setLoading(false);
      } 
    })
    .catch((error) => {
      // Request sending/parsing error
      setLoading(false);
      console.log(error);
      //console.warn(error);
    })
  };

  const sendLoginRequest = () => {
    setLoading(true);
    console.log(countryCode + mobileNumber, pinRequired)
    API.login( countryCode + mobileNumber, pinRequired ) //data.user.phone
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          console.log(responseJson);
          if (responseJson.data.user.username == null) {
            UserProxy.storeUserData(responseJson.data.user);
            DataProxy.storeToken(responseJson.data.m3token);
            navigation.navigate('LoginChooseDetails')
          } else {
            UserProxy.storeUserData(responseJson.data.user);
            DataProxy.storeToken(responseJson.data.m3token);
            AsyncStorage.storeToken(responseJson.data.m3token)
            .then(() => signIn())
            .catch(e => console.log(e));
          }
        } else if(responseJson.error == 1) {
          // Login error
          setLoading(false);
          focusNextField('1');
          setCode1('');
          setCode2('');
          setCode3('');
          setCode4('');
          setCode5('');
          setCode6('');          
          console.log(responseJson);
          setPinError(responseJson.msg);
        } else {
          // Unknown Registration error
          setLoading(false);
          focusNextField('1');
          setCode1('');
          setCode2('');
          setCode3('');
          setCode4('');
          setCode5('');
          setCode6('');          
          setPinError('Incorrect PIN, please try again!');
          console.log(responseJson);
        }
        setLoading(false);
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log(error);
      });
  }  


  return (
    <SafeAreaView style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>
        <Text style={[styles.mediumText, {textAlign: 'center', lineHeight: 22}]}>
          An SMS containing a 6 digit verification code has been sent to +{countryCode} {mobileNumber}. Please enter this below to confirm your number:
        </Text>
        <View style={styles.loginInputCode}>
          <View style={getErrorStyle(pinError)}>
            <TextInput
              style={[getErrorInputStyle(pinError), {width: 50, textAlign: 'center'}]}
              value={code1}
              onChangeText={(text) => { setCode1(text); setPinError(null); focusNextField('2'); }}
              maxLength={1}
              keyboardType='numeric'
              autoFocus
              ref={input1}
              blurOnSubmit={false}
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={getErrorStyle(pinError)}>
            <TextInput
              style={[getErrorInputStyle(pinError), {width: 50, textAlign: 'center'}]}
              value={code2}
              onChangeText={(text) => { setCode2(text); setPinError(null); focusNextField('3'); }}
              maxLength={1}
              keyboardType='numeric'
              ref={input2}
              blurOnSubmit={false}
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={getErrorStyle(pinError)}>
            <TextInput
              style={[getErrorInputStyle(pinError), {width: 50, textAlign: 'center'}]}
              value={code3}
              onChangeText={(text) => { setCode3(text); setPinError(null); focusNextField('4'); }}
              maxLength={1}
              keyboardType='numeric'
              ref={input3}
              blurOnSubmit={false}
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={getErrorStyle(pinError)}>
            <TextInput
              style={[getErrorInputStyle(pinError), {width: 50, textAlign: 'center'}]}
              value={code4}
              onChangeText={(text) => { setCode4(text); setPinError(null); focusNextField('5'); }}
              maxLength={1}
              keyboardType='numeric'
              ref={input4}
              blurOnSubmit={false}
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={getErrorStyle(pinError)}>
            <TextInput
              style={[getErrorInputStyle(pinError), {width: 50, textAlign: 'center'}]}
              value={code5}
              onChangeText={(text) => { setCode5(text); setPinError(null); focusNextField('6'); }}
              maxLength={1}
              keyboardType='numeric'
              ref={input5}
              blurOnSubmit={false}
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={getErrorStyle(pinError)}>
            <TextInput
              style={[getErrorInputStyle(pinError), {width: 50, textAlign: 'center'}]}
              value={code6}
              onChangeText={(text) => {
                //setCode6(text); 
                setPinError(null);
                //setTimeout(checkCodes, 1000);
                checkCodes(text);
              }}
              maxLength={1}
              keyboardType='numeric'
              ref={input6}
              blurOnSubmit={false}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {getErrorContainer(pinError)}

        <View style={[styles.loginBottomContainer, {bottom: keyboardOffset}]}>
          <Button
            text={'RESEND CODE'}
            onPress={onResendCodePress}
            opacity={loading ? 0.5 : 1}
            disabled={loading ? true : false}
            stretch={'stretch'}
          />
        </View>

      </View>
    </SafeAreaView>
  )

};

export default LoginEnterCode;



import React from 'react';
import {
  Text,
  View,
  ScrollView
} from 'react-native';

import styles from '../../common/styles';

const Terms = () => {
  return (
    <View style={styles.mainContainerUnderHeader}>
      <ScrollView style={styles.termsContainer}>
        <Text style={[styles.mediumText, {lineHeight: 24}]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie tellus a molestie efficitur. Phasellus molestie urna nec tempus luctus. Pellentesque efficitur maximus turpis, non tristique nisl scelerisque et. Sed pulvinar mollis vulputate. Suspendisse quis nibh id augue tempor gravida. Pellentesque convallis libero arcu, non lacinia leo bibendum vitae. Nulla id justo venenatis ex rutrum lobortis. Nam at tincidunt risus. Duis pulvinar velit eu magna ornare tristique. Sed imperdiet volutpat lorem, in blandit est laoreet et.
        {'\n\n'}
Praesent nunc lorem, gravida non lacinia eget, eleifend at eros. Nulla ac erat auctor, placerat ex vitae, interdum lacus. Phasellus elementum eget augue ut venenatis. Mauris varius porta lectus sit amet eleifend. Praesent sagittis, erat feugiat efficitur eleifend, orci justo tempor eros, quis mattis magna dolor et dolor. Etiam euismod cursus odio, eget interdum velit convallis vitae. Donec fermentum dui vitae accumsan sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas viverra, ante a cursus commodo, urna ex rhoncus orci, quis interdum justo nunc sit amet urna. Duis blandit erat in libero volutpat accumsan.</Text>
      </ScrollView>
    </View>
  )
};

export default Terms;






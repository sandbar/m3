import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
  LayoutAnimation,
  Alert,
  SafeAreaView,
  Platform
} from 'react-native';

import Colors from '../../common/colors';
import Button from '../../common/button';
import styles from '../../common/styles';
import API from '../../communication/api';
import DataConstants from '../../data/DataConstants';

const Login = ({navigation, route}) => {

  const {updatedCountryData} = route.params ? route.params : '';
  const countryData = DataConstants.getCountries();

  const [loading, setLoading] = useState(false);
  const [currentCountry, setCurrentCountry] = useState(0);
  const [countryCode, setCountryCode] = useState(countryData[0].phoneCode);
  const [keyboardOffset, setKeyboardOffset] = useState(0);
  const [mobileNumber, setMobileNumber] = useState('');
  const [mobileNumberError, setMobileNumberError] = useState('');

  const countryFlag = countryData[currentCountry].uri;
  let androidOffset = Platform.OS === 'ios' ? 0 : 220;

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (updatedCountryData) {
        setCountryCode(updatedCountryData.phoneCode);
        setCurrentCountry(updatedCountryData.id);
      }      
    });
    return unsubscribe;
  }, [navigation, route]);   

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => {
      //console.log(e)
      //setKeyboardOffset(e.endCoordinates.height);
      setKeyboardOffset(Math.floor(e.endCoordinates.height - androidOffset));
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);      
    });
    return () => keyboardDidShowListener.remove();
  }, [Keyboard]);

  useEffect(() => {
    const keyboardHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardOffset(0);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);      
    });
    return () => keyboardHideListener.remove();
  }, [Keyboard]);

  const getErrorContainer = (errorMsg) => {
    if(errorMsg) {
      return (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{errorMsg}</Text>
          <Image source={require('../../../img/icon-error.png')} />
        </View>
      );
    }
    else {
      return null;
    }
  };

  const getErrorStyle = (errorMsg) => {
    return errorMsg ? styles.textInputError : styles.textInput;
  };

  const getErrorInputStyle = (errorMsg) => {
    return errorMsg ? styles.inputFieldError : styles.inputField;
  };

  const onSignInPress = () => {
    var areFieldsValid = validateFields();
    if (areFieldsValid) {
      // debug
      //navigation.navigate('LoginEnterCode', {countryCode:countryCode, mobileNumber:mobileNumber, data: {pin: '123456'}})

      setLoading(true);
      sendLoginRequest();
    }
  };

  const validateFields = () => {

    // Check if email has min 1 char
    var mobileNumberValid = isMobileNumberValid();

    // Show error messages (if any)
    setMobileNumberError(mobileNumberValid ? null : 'Please enter a valid mobile number');

    return mobileNumberValid;
  };

  const isMobileNumberValid = () => {
    if (mobileNumber) {
      console.log(mobileNumber.length)
      return mobileNumber.length > 7 ? true : false;
    }
    return false;
  };

  const sendLoginRequest = () => {
    // check for initial zero and remove
    let mobileNum;
    if (mobileNumber.charAt(0) == '0') {
      mobileNum = mobileNumber.substr(1);
    } else {
      mobileNum = mobileNumber;
    }

    console.log(countryCode + mobileNum)

    API.register(countryCode + mobileNum)
    .then((responseJson) => {
      console.log(responseJson);
      if(responseJson.error == 0) {
        if(responseJson.data.errorSending) {
          // show the mobile number had a problem, try again
          setMobileNumberError(responseJson.data.errorSending);
          setLoading(false);
        } else {
          setLoading(false);
          navigation.navigate('LoginEnterCode', {
            countryCode: countryCode, 
            mobileNumber: mobileNum, 
            data: responseJson.data
          })
        }
      } else {
        setLoading(false);
        Alert.alert('Error sending code', responseJson.msg);
      }
    })
    .catch((error) => {
      // Request sending/parsing error
      setLoading(false);
      console.log('ERROR CATCH:', error);
      //console.warn(error);
    })
  };

  // const onLogoutPress = () => {
  //   API.logout()
  //   .then((responseJson) => {
  //     if(responseJson.error == 0) {
  //       console.log(responseJson);
  //     }
  //   })
  //   .catch((error) => {
  //     // Request sending/parsing error
  //     console.log('ERROR CATCH:');
  //     //console.warn(error);
  //   })
  // }; 

  return (
    <SafeAreaView style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>

        <View style={styles.countryChoiceContainer}>
          <View style={[styles.textInput, {marginRight: 10}]}>
            <TextInput
              style={[styles.inputField, {width: 70, textAlign: 'right'}]}
              value={countryCode}
              onChangeText={(text) => setCountryCode(text)}
              editable={currentCountry == 10 ? true : false}
              maxLength={3}
              keyboardType='numeric'
              underlineColorAndroid='transparent'
            />
            <Text style={styles.phoneCodePlus}>+</Text>
          </View>
          <TouchableOpacity 
            style={{flex:1}} 
            onPress={() => navigation.navigate('ChooseCountry', {countryID: currentCountry})}
          >
            <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
              <Image source={countryFlag} style={{marginRight: 10}} />
              <Text style={[styles.mediumText, {flex:1}]}>{countryData[currentCountry].name}</Text>
              <Image source={require('../../../img/icon-arrow.png')} />
            </View>
          </TouchableOpacity>
        </View>

        <View style={getErrorStyle(mobileNumberError)}>
          <TextInput
            style={getErrorInputStyle(mobileNumberError)}
            placeholder={'Mobile number'}
            placeholderTextColor={Colors.defaultInputText}
            value={mobileNumber}
            onChangeText={(text) => {
              setMobileNumber(text);
              setMobileNumberError(null);
            }}
            returnKeyType='next'
            keyboardType='numeric'
            onSubmitEditing={() => onSignInPress()}
            underlineColorAndroid='transparent'
            autoFocus
          />
        </View>
        {getErrorContainer(mobileNumberError)}

        <View style={[styles.loginBottomContainer, {bottom: keyboardOffset}]}>
          <Button
            text={'NEXT'}
            onPress={onSignInPress}
            opacity={loading ? 0.5 : 1}
            disabled={loading ? true : false}
            stretch={'stretch'}
          />
        </View>

      </View>
    </SafeAreaView>  
  );

};

export default Login;
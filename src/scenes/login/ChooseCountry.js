import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList
} from 'react-native';

import Colors from '../../common/colors';
import styles from '../../common/styles';
import DataConstants from '../../data/DataConstants';

const ChooseCountry = ({navigation, route}) => {

  const {countryID} = route.params;
  const countryData = DataConstants.getCountries();

  const chooseCountry = (rowData) => {
    navigation.navigate('Login', {updatedCountryData:rowData});
  }  

  return (
    <FlatList
      data={countryData}
      style={styles.list}
      renderItem={({item, index}) => (
        <TouchableOpacity onPress={() => chooseCountry(item)} underlayColor={Colors.underlayColor}>
          <View style={styles.countryRowContainer}>
            <Image source={item.uri} style={{marginRight:10}}/>
            <Text style={[styles.mediumText, {flex:1}]}>{item.name}</Text>
            <Image source={require('../../../img/icon-tick.png')} style={{opacity: countryID == index ? 1 : 0}} />
          </View>
        </TouchableOpacity>        
      )}
      keyExtractor={(item, index) => index.toString()}
      renderSeparator={() => <View style={styles.listSeparator} />}
    />    
  )
};

export default ChooseCountry;
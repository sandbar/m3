import React from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  Picker,
  Alert
} from 'react-native';

import {Actions} from 'react-native-router-flux';

import Colors from '../../common/colors';
import Button from '../../common/button';
import LabelButton from '../../common/labelbutton';
var styles = require('../../common/styles');
var API = require('../../communication/api');
var UserProxy = require('../../communication/UserProxy');
const Item = Picker.Item;

module.exports = React.createClass({
  getInitialState() {
    return {
      loading: false,
      selectedDay: '01',
      selectedMonth: '01',
      selectedYear: '2003',
      gender: null,
      male: false,
      female: false,
    };
  },

  render() {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <ScrollView style={{flex:1}}>
          <View style={{alignSelf:'stretch', alignItems: 'center', marginTop: 15, marginBottom: 5}}>
            <Text style={[styles.header1, {marginBottom:10}]}>DATE OF BIRTH</Text>
          </View>
          <View style={styles.pickerBG}>
            <View style={styles.view320}>
              <Picker style={styles.picker}
                      selectedValue={this.state.selectedDay}
                      onValueChange={this.onValueChange.bind(this, 'selectedDay')}>
                <Item label="01" value="01" />
                <Item label="02" value="02" />
                <Item label="03" value="03" />
                <Item label="04" value="04" />
                <Item label="05" value="05" />
                <Item label="06" value="06" />
                <Item label="07" value="07" />
                <Item label="08" value="08" />
                <Item label="09" value="09" />
                <Item label="10" value="10" />
                <Item label="11" value="11" />
                <Item label="12" value="12" />
                <Item label="13" value="13" />
                <Item label="14" value="14" />
                <Item label="15" value="15" />
                <Item label="16" value="16" />
                <Item label="17" value="17" />
                <Item label="18" value="18" />
                <Item label="19" value="19" />
                <Item label="20" value="20" />
                <Item label="21" value="21" />
                <Item label="22" value="22" />
                <Item label="23" value="23" />
                <Item label="24" value="24" />
                <Item label="25" value="25" />
                <Item label="26" value="26" />
                <Item label="27" value="27" />
                <Item label="28" value="28" />
                <Item label="29" value="29" />
                <Item label="30" value="30" />
                <Item label="31" value="31" />
              </Picker>

              <Picker style={styles.picker}
                      selectedValue={this.state.selectedMonth}
                      onValueChange={this.onValueChange.bind(this, 'selectedMonth')}>
                <Item label="01" value="01" />
                <Item label="02" value="02" />
                <Item label="03" value="03" />
                <Item label="04" value="04" />
                <Item label="05" value="05" />
                <Item label="06" value="06" />
                <Item label="07" value="07" />
                <Item label="08" value="08" />
                <Item label="09" value="09" />
                <Item label="10" value="10" />
                <Item label="11" value="11" />
                <Item label="12" value="12" />
              </Picker>

              <Picker style={styles.picker}
                      selectedValue={this.state.selectedYear}
                      onValueChange={this.onValueChange.bind(this, 'selectedYear')}>
                <Item label="2003" value="2003" />
                <Item label="2002" value="2002" />
                <Item label="2001" value="2001" />
                <Item label="2000" value="2000" />
                <Item label="1999" value="1999" />
                <Item label="1998" value="1998" />
                <Item label="1997" value="1997" />
                <Item label="1996" value="1996" />
                <Item label="1995" value="1995" />
                <Item label="1994" value="1994" />
                <Item label="1993" value="1993" />
                <Item label="1992" value="1992" />
                <Item label="1991" value="1991" />
                <Item label="1990" value="1990" />
                <Item label="1989" value="1989" />
                <Item label="1988" value="1988" />
                <Item label="1987" value="1987" />
                <Item label="1986" value="1986" />
                <Item label="1985" value="1985" />
                <Item label="1984" value="1984" />
                <Item label="1983" value="1983" />
                <Item label="1982" value="1982" />
                <Item label="1981" value="1981" />
                <Item label="1980" value="1980" />
                <Item label="1979" value="1979" />
                <Item label="1978" value="1978" />
                <Item label="1977" value="1977" />
                <Item label="1976" value="1976" />
                <Item label="1975" value="1975" />
                <Item label="1974" value="1974" />
                <Item label="1973" value="1973" />
                <Item label="1972" value="1972" />
                <Item label="1971" value="1971" />
                <Item label="1970" value="1970" />
                <Item label="1969" value="1969" />
                <Item label="1968" value="1968" />
                <Item label="1967" value="1967" />
                <Item label="1966" value="1966" />
                <Item label="1965" value="1965" />
                <Item label="1964" value="1964" />
                <Item label="1963" value="1963" />
                <Item label="1962" value="1962" />
                <Item label="1961" value="1961" />
                <Item label="1960" value="1960" />
                <Item label="1959" value="1959" />
                <Item label="1958" value="1958" />
                <Item label="1957" value="1957" />
                <Item label="1956" value="1956" />
                <Item label="1955" value="1955" />
                <Item label="1954" value="1954" />
                <Item label="1953" value="1953" />
                <Item label="1952" value="1952" />
                <Item label="1951" value="1951" />
                <Item label="1950" value="1950" />
                <Item label="1949" value="1949" />
                <Item label="1948" value="1948" />
                <Item label="1947" value="1947" />
                <Item label="1946" value="1946" />
                <Item label="1945" value="1945" />
                <Item label="1944" value="1944" />
                <Item label="1943" value="1943" />
                <Item label="1942" value="1942" />
                <Item label="1941" value="1941" />
                <Item label="1940" value="1940" />
                <Item label="1939" value="1939" />
                <Item label="1938" value="1938" />
                <Item label="1937" value="1937" />
                <Item label="1936" value="1936" />
                <Item label="1935" value="1935" />
                <Item label="1934" value="1934" />
                <Item label="1933" value="1933" />
                <Item label="1932" value="1932" />
                <Item label="1931" value="1931" />
                <Item label="1930" value="1930" />
                <Item label="1929" value="1929" />
                <Item label="1928" value="1928" />
                <Item label="1927" value="1927" />
                <Item label="1926" value="1926" />
                <Item label="1925" value="1925" />
                <Item label="1924" value="1924" />
                <Item label="1923" value="1923" />
                <Item label="1922" value="1922" />
                <Item label="1921" value="1921" />
                <Item label="1920" value="1920" />
              </Picker>
            </View>
          </View>

          <View style={{alignSelf:'stretch', alignItems: 'center'}}>
            <View style={styles.form320}>
              <TouchableHighlight
                style={[styles.genderButton, this.state.gender == 'male' ? {borderColor:Colors.brandGreen} : {borderColor: Colors.disabledGrey}]}
                underlayColor={Colors.brandGreen}
                onPress={() => { this.selectGender('male') }}
                >
                <View style={{flexDirection: 'row'}}>
                  <Image style={styles.genderIcon} source={require('../../../img/icon-male.png')} />
                  <Text style={[styles.genderButtonText, this.state.gender == 'male' ? {color: Colors.brandGreen} : {color: Colors.disabledGreyText}]}>MALE</Text>
                </View>
              </TouchableHighlight>
              <TouchableHighlight
                style={[styles.genderButton, this.state.gender == 'female' ? {borderColor:Colors.brandGreen} : {borderColor: Colors.disabledGrey}]}
                underlayColor={Colors.brandGreen}
                onPress={() => { this.selectGender('female') }}
                >
                <View style={{flexDirection: 'row'}}>
                  <Image style={styles.genderIcon} source={require('../../../img/icon-female.png')} />
                  <Text style={[styles.genderButtonText, this.state.gender == 'female' ? {color: Colors.brandGreen} : {color: Colors.disabledGreyText}]}>FEMALE</Text>
                </View>
              </TouchableHighlight>
            </View>
          </View>

        </ScrollView>

        <View style={styles.loginBottomContainer}>
          <View style={styles.tsandcsTextContainer}>
            <Text style={[styles.mediumText, {marginBottom:5}]}>By continuing, I agree to the</Text>
            <LabelButton text={'Terms & Conditions'} onPress={this.showTsandCsPopup} btnStyle={styles.buttonText} />
          </View>
          <Button
          disabled={this.state.loading ? true : false}
          text={'NEXT'}
          onPress={this.onSignUpPress}
          opacity={this.state.loading ? 0.5 : 1}
          ref='_signupButton'
          stretch={'stretch'}
          />
        </View>

      </View>
  )},

  onValueChange: function(key: string, value: string) {
    const newState = {};
    newState[key] = value;
    this.setState(newState);
  },

  selectGender(gender) {
    this.setState({gender: gender});
  },

  onSignUpPress() {
    this.setState({loading: true});
    API.userUpdateDOB(this.state.gender, (this.state.selectedYear + '-' + this.state.selectedMonth + '-' + this.state.selectedDay))
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          // console.log(responseJson);
          //UserProxy.storeUserData(responseJson.data[0]);
          Actions.OTHER_PART();
        } else {
          // Unknown User Update error
          console.log('UNKNOWN ERROR:');
          console.warn(responseJson);
        }
        this.setState({loading: false});
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:');
        console.warn(error);
        this.setState({loading: false});
      });
  },
  showTsandCsPopup() {
    Actions.terms();
  },
  onClosePress() {
    Actions.pop();
  }
});

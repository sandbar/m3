import React from 'react';
import {
  Text,
  Image,
  View,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import styles from '../../common/styles';
import ButtonTransparent from '../../common/ButtonTransparent';

const LoginVideo = ({ navigation }) => {

  const onSignUpPress = () => {
    navigation.navigate('Login');
  }

  return (
    <View style={styles.mainContainer}>
      <View style={styles.paddedContainer}>
        <LinearGradient
          colors={['#22908f', '#5bd1aa']}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          style={styles.loginVideoContainer}>

          <View style={styles.splashIconsContainer}>
            <View style={{ marginBottom: 50 }}>
              <Image source={require('../../../img/m3logo.png')} />
            </View>
            <View style={styles.splashIconRow}>
              <View style={styles.splashIcon}>
                <Image source={require('../../../img/login-icon-upload.png')} />
              </View>
              <Text style={styles.splashIconsText}>Upload.</Text>
            </View>
            <View style={styles.splashIconRow}>
              <View style={styles.splashIcon}>
                <Image source={require('../../../img/login-icon-play.png')} />
              </View>
              <Text style={styles.splashIconsText}>Play.</Text>
            </View>
            <View style={styles.splashIconRow}>
              <View style={styles.splashIcon}>
                <Image source={require('../../../img/login-icon-track.png')} />
              </View>
              <Text style={styles.splashIconsText}>Track.</Text>
            </View>
            <View style={styles.splashIconRow}>
              <View style={styles.splashIcon}>
                <Image source={require('../../../img/login-icon-compete.png')} />
              </View>
              <Text style={styles.splashIconsText}>Compete.</Text>
            </View>
          </View>

          <View style={styles.splashButtonContainer}>
            <ButtonTransparent
              onPress={onSignUpPress}
              stretch={'stretch'}
              text={'GET STARTED'}
            />
          </View>

          {/*
          <View style={styles.loginLogo}>
            <Image source={require('../../../img/m3logo.png')} />
          </View>
          */}

        </LinearGradient>
      </View>
    </View>
  );
};

module.exports = LoginVideo;

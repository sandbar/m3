import React from 'react';
import {
  Text,
  View
} from 'react-native';

import styles from '../../common/styles';
import FormatData from '../../utils/FormatData';

const PlayItem = (props) => {

  var title = props.title ? props.title : '...';
  var progress = props.progress;
  var bgColour = 'rgba(242,101,34,0.3)';
  if(progress <= 0.25) {
    bgColour = 'rgba(0,191,243,0.3)';
  } else if(progress <= 0.5) {
    bgColour = 'rgba(131,147,202,0.3)';
  } else if(progress <= 0.75) {
    bgColour = 'rgba(247,148,29,0.3)';
  }

  return (
    <View style={[styles.profilePlayItem, {backgroundColor:bgColour}]}>
      <View style={[styles.profilePlayCircle, {backgroundColor:bgColour}]}>
        <Text style={styles.playItemHoursText}>{FormatData.fromStoH(props.total)}</Text>
        <Text style={styles.smallText}>{FormatData.fromStoM(props.total)}</Text>
      </View>
      <Text style={styles.smallText} numberOfLines={1}>{props.title}</Text>
    </View>
  )

};

export default PlayItem;

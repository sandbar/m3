import React from 'react';
import {
  Text,
  View
} from 'react-native';

import {Actions} from 'react-native-router-flux';
var styles = require('../../common/styles');
import Button from '../../common/button';

var QUESTIONS = [
  {id: 0, title: 'Please read the following questions carefully', body:'IMPORTANT: If the answer to any of the following questions is "yes" please consult your doctor before attempting any physical activity.'},
  {id: 1, title: 'Heart Condition', body: 'Has your doctor ever said that you have a heart condition and that you should only do physical activity recommended by a doctor?'},
  {id: 2, title: 'Chest Pain', body: 'Do you feel pain in your chest when you do physical activity?'},
  {id: 3, title: 'Chest Pain - last month', body: 'In the past month, have you had chest pain when you were not doing physical activity?'},
  {id: 4, title: 'Asthma', body: 'Has a doctor ever told you that you have asthma?'},
  {id: 5, title: 'Diabetes', body: 'Has a doctor ever told you that you have diabetes?'},
  {id: 6, title: 'Dizziness', body: 'Do you lose your balance because of dizziness or do you lose consciousness?'},
  {id: 7, title: 'Joints', body: 'Do you have a bone or joint problem that could be made worse by a change in your physical activity?'},
  {id: 8, title: 'Blood Pressure', body: 'Is your doctor currently prescribing drugs (for example, water pills) for your blood pressure or heart condition?'},
  {id: 9, title: 'Any other reasons', body: 'Do you know of ANY other reason why you should not do physical activity?'},
]

var currentQuestion = 0;

module.exports = React.createClass({

  getInitialState() {
    return {
      currentQuestion: currentQuestion+1,
      totalQuestions: 10,
      currentQuestionTitle: QUESTIONS[currentQuestion].title,
      currentQuestionBody: QUESTIONS[currentQuestion].body,
      loading: false,
      buttonText: 'NEXT',
    };
  },

  render() {
    return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={[styles.profileHeader, {height:130}]}>
        <View style={styles.profileHeaderMainRow}>
          <View style={styles.fitCheckQuestionNum}>
            <Text style={styles.currentQuestionNum}>{this.state.currentQuestion}</Text>
            <Text style={styles.totalQuestionNum}>/{this.state.totalQuestions}</Text>
          </View>
        </View>
      </View>
      <View style={styles.paddedContainer}>
        <Text style={styles.fitCheckTitle}>{this.state.currentQuestionTitle}</Text>
        <Text style={styles.fitCheckBody}>
          {this.state.currentQuestionBody}
        </Text>
      </View>
      <View style={styles.fitCheckBottomContainer}>
        <Button
          text={this.state.buttonText}
          stretch={'stretch'}
          onPress={this.onNextPress}
          opacity={this.state.loading ? 0.5 : 1}
          disabled={this.state.loading ? true : false}
        />
      </View>

    </View>
  )},

  onNextPress() {
    if (this.state.currentQuestion < this.state.totalQuestions) {
      currentQuestion++;
      this.state.currentQuestion == this.state.totalQuestions-1 ? this.setState({buttonText:'DONE'}) : this.setState({buttonText:'NEXT'})
    } else {
      currentQuestion = 0;
      Actions.pop();
    }
    this.setState({
      currentQuestion: currentQuestion+1,
      currentQuestionTitle: QUESTIONS[currentQuestion].title,
      currentQuestionBody: QUESTIONS[currentQuestion].body,
    })
  }

});

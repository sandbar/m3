import React, {useState, useEffect, useLayoutEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Animated,
} from 'react-native';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import ProfilePlays from './ProfilePlays';
import ProfileFavourites from './ProfileFavourites';
import ProfileSynced from './ProfileSynced';

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';
import UserProxy from '../../communication/UserProxy';
import NetworkStatus from '../../utils/NetworkStatus';

const Profile = ({navigation, route}) => {

  const [following, setFollowing] = useState('');
  const [followers, setFollowers] = useState('');
  const [imageData, setImageData] = useState(UserProxy.getImage() == null ? require('../../../img/avatar-default.png') : {uri: "http://stream.maxmyminutes.com/users-images/" + UserProxy.getImage()});
  const [addGroupBtnPos, setAddGroupBtnPos] = useState(0);
  let imageOpacity = new Animated.Value(86);
  const Tab = createMaterialTopTabNavigator();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          style={{left:20}}
          onPress={() => navigation.toggleDrawer()}
        >
          <Image source={require('../../../img/btn-hamburger.png')} />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={{right:20}}
          onPress={() => navigation.navigate('ProfileEditProfile')}
        >
          <Image source={require('../../../img/btn-edit.png')} />
        </TouchableOpacity>
      ),    
    });  
  },[navigation]);    

  useEffect(() => {
    let isSubscribed = true;
    if (NetworkStatus.isOnline()) {
      API.user()
        .then((responseJson) => {
          if (isSubscribed) {
            if(responseJson.error == 0) {
              // SUCCESS
              console.log("getting profile =>", responseJson.data)
              UserProxy.storeUserData(responseJson.data);
              // set number of followers for this view
              setFollowing(UserProxy.getFollowing());
              setFollowers(UserProxy.getFollowers());
            } else {
              // Unknown Registration error
              console.log('PROFILE: UNKNOWN ERROR:', responseJson);
            }
          }
        })
        .catch((error) => {
          // Request sending/parsing error
          console.log('GET PROFILE: ERROR CATCH:', error);
        });    
    }
    return () => isSubscribed = false;
  },[]);

  useEffect(() => {
    let isSubscribed = true;
    if (route.params?.refreshView) {
      if (isSubscribed) {
        setImageData(UserProxy.getImage() == null ? require('../../../img/avatar-default.png') : {uri: "http://stream.maxmyminutes.com/users-images/" + UserProxy.getImage()});
      }
    }
    return () => isSubscribed = false;
  },[route.params?.refreshView]);

  const onImageLoaded = () => {
    Animated.timing(
       imageOpacity,
       {
         toValue: 1,
         duration: 500,
         useNativeDriver: true
       }
     ).start();
  };

  const showFollowing = () => {
    following > 0 &&
      navigation.navigate('ProfileFollowing', {type: 'following'}); //UserProxy.getFollowingList()
  };

  const showFollowers = () => {
    followers > 0 &&
      navigation.navigate('ProfileFollowers', {type: 'followers'}); //UserProxy.getFollowersList()}
  };

  // is this needed?
  // const getFollowing = () => {
  //   API.followings(UserProxy.getId())
  //     .then((responseJson) => {
  //       if(responseJson.error == 0) {
  //         // SUCCESS
  //         console.log(responseJson);
  //         UserProxy.storeFollowingList(responseJson.data);
  //         //this.setState({following: responseJson.data.length});
  //       } else {
  //         // Unknown Registration error
  //         console.log('UNKNOWN ERROR:');
  //         console.warn(responseJson);
  //       }
  //     })
  //     .catch((error) => {
  //       // Request sending/parsing error
  //       console.log('ERROR CATCH:');
  //       console.warn(error);
  //     });
  // },

  // is this needed?
  // getFollowers() {
  //   API.followers(UserProxy.getId())
  //     .then((responseJson) => {
  //       if(responseJson.error == 0) {
  //         // SUCCESS
  //         //console.log(responseJson);
  //         UserProxy.storeFollowersList(responseJson.data);
  //         //this.setState({followers: responseJson.data.length});
  //       } else {
  //         // Unknown Registration error
  //         console.log('UNKNOWN ERROR:');
  //         console.warn(responseJson);
  //       }
  //     })
  //     .catch((error) => {
  //       // Request sending/parsing error
  //       console.log('ERROR CATCH:');
  //       console.warn(error);
  //     });
  // },  


  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.profileHeader}>
        <View style={styles.profileHeaderMainRow}>
          <TouchableOpacity onPress={() => showFollowing()}>
            <View style={styles.followerButton}>
              <Text style={styles.followerAmount}>{following}</Text>
              <Text style={styles.followerType}>I track</Text>
            </View>
          </TouchableOpacity>          
          <View style={styles.profileAvatar}>
            <Animated.Image
              style={[styles.profileAvatarImage, {opacity:imageOpacity}]}
              source={imageData}
              onLoad={() => onImageLoaded()}
            />
          </View>
          <TouchableOpacity onPress={() => showFollowers()}>
            <View style={styles.followerButton}>
              <Text style={styles.followerAmount}>{followers}</Text>
              <Text style={styles.followerType}>track me</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Text style={styles.profileUsername}>{UserProxy.getUsername()}</Text>
      </View>


      <Tab.Navigator
        initialRouteName="ProfilePlays"
        tabBarOptions={{
          activeTintColor: '#fff',
          inactiveTintColor: Colors.brandLightGreen,
          indicatorStyle: styles.tabBarUnderlineStyle,
          labelStyle: styles.tabBarTextStyle,
          style: styles.tabBar,
        }}
      >
        <Tab.Screen
          name="ProfilePlays"
          component={ProfilePlays}
          options={{ tabBarLabel: 'PLAYS' }}
        />
        <Tab.Screen
          name="ProfileFavourites"
          component={ProfileFavourites}
          options={{ tabBarLabel: 'FAVOURITES' }}
        />
        <Tab.Screen
          name="ProfileSynced"
          component={ProfileSynced}
          options={{ tabBarLabel: 'SYNCED' }}
        />                

      </Tab.Navigator>

    </View>
  )

};

export default Profile;

import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ListView,
  Picker,
  Animated
} from 'react-native';

import EditGoalsList from './EditGoalsList';
import Colors from '../../common/colors';
var styles = require('../../common/styles');

const {width, height} = require('Dimensions').get('window');
const Item = Picker.Item;

module.exports = React.createClass({

  getInitialState() {
    return {
      weeklyGoals: this.props.weeklyGoals,
      monthlyGoals: this.props.monthlyGoals,
      pickerOpen: false,
      pickerCoverHeight: 0,
      pickerCoverOpacity: 0,
      pickerCoverAnim: new Animated.Value(),
      pickerBottom: 0,
      pickerAnim: new Animated.Value(),
    };
  },

  render() {
    return (
      <View style={{flex:1}}>
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.paddedContainer}>

          <Text style={[styles.header1, {marginTop:10}]}>This week</Text>
          {this.weeklyList()}
          <Text style={[styles.header1, {marginTop:16}]}>This month</Text>
          {this.monthlyList()}

        </View>
      </View>

      {/*<TouchableOpacity onPress={() => this.togglePicker()}><Text>PICKER</Text></TouchableOpacity>*/}

      <Animated.View style={[styles.pickerCover, {opacity: this.state.pickerCoverAnim, width:width, height:this.state.pickerCoverHeight}]}>
      <TouchableOpacity
        onPress={() => this.togglePicker()}
        style={{flex:1}}
      />
      </Animated.View>

      <Animated.View style={[styles.pickerContainer, {bottom: this.state.pickerAnim}]}>
        <View style={styles.view320}>
          <Picker style={styles.pickerBottom}
            selectedValue={this.state.selectedTime}
            onValueChange={this.onValueChange.bind(this, 'selectedTime')}>
            <Item label="0.5h" value="0.5" />
            <Item label="1h" value="1" />
            <Item label="1.5h" value="1.5" />
            <Item label="2h" value="2" />
            <Item label="2.5h" value="2.5" />
            <Item label="3h" value="3" />
            <Item label="3.5h" value="3.5" />
            <Item label="4h" value="4" />
            <Item label="4.5h" value="4.5" />
            <Item label="5h" value="5" />
            <Item label="5.5h" value="5.5" />
            <Item label="6h" value="6" />
          </Picker>
        </View>
      </Animated.View>

      </View>
    )
  },

  weeklyList() {
    if (this.state.weeklyGoals.length > 0) {
      return(
        <EditGoalsList goals={this.state.weeklyGoals} handler={this.togglePicker} />
      )
    } else {
      return (
        <View style={{alignSelf:'stretch'}}>
          <Text style={[styles.smallText, {marginTop: 12}]}>You have no weekly goals set up. Watch Mixes to generate goals!</Text>
        </View>
      )
    }
  },

  monthlyList() {
    if (this.state.monthlyGoals.length > 0) {
      return(
        <EditGoalsList goals={this.state.monthlyGoals} handler={this.togglePicker} />
      )
    } else {
      return (
        <View style={{alignSelf:'stretch'}}>
          <Text style={[styles.smallText, {justifyContent:'center', marginTop: 12}]}>You have no monthly goals set up. Watch Mixes to generate goals!</Text>
        </View>
      )
    }
  },

  togglePicker() {

    let initialCoverValue = this.state.pickerOpen ? 1 : 0,
        finalCoverValue = this.state.pickerOpen ? 0 : 1;
    let initialPickerValue = this.state.pickerOpen ? 0 : -216,
        finalPickerValue = this.state.pickerOpen ? -216 : 0;

    this.state.pickerCoverAnim.setValue(initialCoverValue);
    Animated.spring(
      this.state.pickerCoverAnim,
      {
        toValue: finalCoverValue
      }
    ).start();

    this.state.pickerAnim.setValue(initialPickerValue);
    Animated.spring(
      this.state.pickerAnim,
      {
        toValue: finalPickerValue
      }
    ).start();

    this.setState({
      pickerCoverHeight: this.state.pickerOpen ? 0 : height,
      pickerOpen: !this.state.pickerOpen,
    });

  },

  onValueChange: function(key: string, value: string) {
    const newState = {};
    newState[key] = value;
    this.setState(newState);
  },


});

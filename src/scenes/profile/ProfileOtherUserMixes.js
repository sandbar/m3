import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  FlatList
} from 'react-native';

var TimeFormat = require('hh-mm-ss');
import styles from '../../common/styles';

const ProfileOtherUserMixes = ({navigation, route}) => {

  const {mixes, username} = route.params;

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  const getDuration = (duration) => {
    if (duration == null) {
      return (
        '0:00'
      )
    } else {
      var durInt = parseInt(duration)
      var dur = TimeFormat.fromS(durInt);
      return (
        dur
      )
    }
  };

  return (
    <View style={styles.containerGeneric}>
      <View style={styles.profileTabContentVideo}>
        {mixes.length > 0 ? 
          <FlatList
            data={mixes}
            contentContainerStyle = {styles.videoList}
            keyExtractor = {(item, index) => index.toString()}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity onPress={() => navigation.navigate('ViewVideoMixProfile', {mixID: item.id})}>
                <View style={styles.videoContainer}>
                  <ImageBackground
                    source={getThumb(item.thumb)}
                    style={styles.videoThumb} >
                    <View style={styles.videoLength}>
                      <Text style={styles.videoLengthText}>
                        {getDuration(item.mixDuration)}
                      </Text>
                    </View>
                  </ImageBackground>
                  <Text style={styles.videoTitle}>{item.title}</Text>
                  <Text style={styles.videoInfo}>{username}</Text>
                  <Text style={styles.videoInfo}>{item.views == null ? 'no' : item.views} plays</Text>
                </View>
              </TouchableOpacity>  
              )            
            }}
          />
          :
          <View style={styles.profileTabContent}>
            <Text style={styles.smallText}>{'No mixes'}</Text>
          </View>          
        }
      </View>
    </View>
  );

};

export default ProfileOtherUserMixes;

import React, {useState, useEffect} from 'react';
import {
  Alert,
  Text,
  View
} from 'react-native';

import styles from '../../common/styles';
import API from '../../communication/api';
import UserProxy from '../../communication/UserProxy';

const ProfileImageUploading = ({navigation, route}) => {

  const {imageData, fileName, contentType, returnView} = route.params;
  const [loadingBarWidth, setLoadingBarWidth] = useState(0);
  const [percentLoaded, setPercentLoaded] = useState(0);
  const [percentIcon, setPercentIcon] = useState('%');

  useEffect(() => {
    let isSubscribed = true;
    API.profileImageCreate(imageData, fileName, contentType, progressHandler, responseHandler);
    return () => isSubscribed = false;      
  },[]);  

  const progressHandler = (progress) => { 
    var percent = Math.floor(progress * 100);
    setLoadingBarWidth(progress);
    setPercentLoaded(percent);
    if (progress == 1) {
      //console.log("SUCCESS!!")
      setPercentLoaded('PROCESSING');
      setPercentIcon('');
    }
  };

  const responseHandler = (response) => {
    //console.log(response);
    if (response.indexOf('"error": 1') > -1) {
      var alertMessage = response.msg;
      Alert.alert(
        'Upload error',
        alertMessage,
        [
          {text: 'OK', onPress: () => navigation.goBack()},
        ]
      )
    } else {
      // save image string to proxy
      var sliceStart = response.indexOf('"image": "');
      var sliceEnd = response.indexOf('.jpg');
      //console.log(response.slice(sliceStart + 10, sliceEnd + 4));
      UserProxy.storeImage(response.slice(sliceStart + 10, sliceEnd + 4));
      navigation.navigate(returnView, {refreshView:true});
    }
  };

  
  return (
    <View style={styles.containerGeneric}>
      <View style={[styles.paddedContainer, {justifyContent: 'center'}]}>
        <View style={styles.loadingBar}>
          <View style={[styles.loadingBarTop, {flex: loadingBarWidth}]}>
            <Text style={styles.loadingBarText} numberOfLines={1}>{percentLoaded}{percentIcon}</Text>
          </View>
          <View style={[styles.loadingBarBot, {flex: 1 - loadingBarWidth}]} />
        </View>
      </View>
    </View>
  );

};

export default ProfileImageUploading;

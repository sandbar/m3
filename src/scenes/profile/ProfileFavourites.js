import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator
} from 'react-native';

var TimeFormat = require('hh-mm-ss')

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';
import NetworkStatus from '../../utils/NetworkStatus';

const ProfileFavourites = ({navigation}) => {

  const [loading, setLoading] = useState(true);
  const [favourites, setFavourites] = useState([]);

  useEffect(() => {
    let isSubscribed = true;
    if (NetworkStatus.isOnline()) {
      API.favourites()
      .then((responseJson) => {
        if (isSubscribed) {
          if(responseJson.error == 0) {
            // SUCCESS
            //console.log(responseJson);
            setFavourites(responseJson.data.favourites);
            setLoading(false);
          } else {
            // Unknown Registration error
            console.log('FAVOURITES: UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('FAVOURITES: ERROR CATCH:', error);
      });      
    }
    return () => isSubscribed = false;
  },[]);

  const pressRow = (mixID) => {
    navigation.navigate('ViewVideoMixProfile', {mixID:mixID});
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  const getDuration = (duration) => {
    if (duration == null) {
      return '0:00'
    } else {
      var dur = TimeFormat.fromS(duration);
      return dur
    }
  };

  if (loading) {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.loadingContainer}>
          <ActivityIndicator size='small' color={Colors.brandGreen}/>
        </View>
    </View>        
    )
  }  

  if (NetworkStatus.isOnline()) {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.profileTabContentVideo}>

          {favourites.length > 0 ?
            <FlatList
              data={favourites}
              contentContainerStyle = {styles.videoList}
              renderItem = {({item, index}) => {
                return (
                  <TouchableOpacity onPress={() => pressRow(item.id)}>
                    <View style={styles.videoContainer}>
                      <Image
                        source={getThumb(item.thumb)}
                        style={styles.videoThumb} >
                        <View style={styles.videoLength}>
                          <Text style={styles.videoLengthText}>
                            {getDuration(parseInt(item.mixDuration))}
                          </Text>
                        </View>
                      </Image>
                      <Text style={styles.videoTitle} numberOfLines={1}>{item.title}</Text>
                      <Text style={styles.videoInfo}>{item.author.username}</Text>
                      <Text style={styles.videoInfo}>{item.views != null ? item.views : '0'} plays</Text>
                    </View>
                  </TouchableOpacity>
                )
              }}
            />
            :
            <View style={[styles.profileTabContent, {marginTop: 10}]}>
              <Text style={styles.defaultText}>You dont have any favourites yet</Text>
            </View>
          }

        </View>
      </View>
    )
  } else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.profileTabContent}>
          <View style={{flexDirection:'row', marginTop:10}}>
            <Image source={require('../../../img/icon-offline.png')} />
            <Text style={styles.offlineText}>Not available offline</Text>
          </View>
        </View>
      </View>
    )
  }

};

export default ProfileFavourites;

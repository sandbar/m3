import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Picker,
} from 'react-native';

import ProfileBar from './ProfileBar';
import Colors from '../../common/colors';
var TimeFormat = require('hh-mm-ss');
var styles = require('../../common/styles');

var goalTotalTime = 3.5 * 60 * 60;

module.exports = React.createClass({

  render() {
    return (
      <View>
      {
        this.props.goals.map(function(goal, i) {
          var tmpTime = TimeFormat.fromS(Math.ceil(goalTotalTime));
          return (
            <TouchableOpacity
              onPress={() => this.pressRow(i)}
              underlayColor={Colors.underlayColor}
              style={styles.listElement}
              key={i}
            >
              <View style={styles.listElementInner}>
                <View style={styles.listElementLeft}>
                  <Text style={styles.defaultText}>
                    {goal.name}
                  </Text>
                </View>
                <View style={styles.listElementRight}>
                  <Text style={styles.defaultText}>
                    {tmpTime.slice(0,-3)}
                  </Text>
                  <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
                </View>
              </View>
            </TouchableOpacity>
          )
        }.bind(this))
      }
      </View>

    );
  },

  pressRow(id) {
    //console.log(id, " pressed");
    this.props.handler(id)
  },

});

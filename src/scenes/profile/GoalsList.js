import React from 'react';
import {
  View
} from 'react-native';

import ProfileBar from './ProfileBar';

module.exports = React.createClass({

  render() {
    return (
      <View style={{alignSelf: 'stretch'}}>
      {
        this.props.goals.map(function(goal, i) {
          return (
            <ProfileBar
              key={i}
              title={goal.name}
              progress={goal.progress}
              total={goal.watched}
            />
          )
        })
      }
      </View>
    );
  }

});

import React, {useState} from 'react';
import {
  TextInput,
  View,
} from 'react-native';

import Button from '../../common/button';
import styles from '../../common/styles';
import API from '../../communication/api';
import UserProxy from '../../communication/UserProxy';

const ProfileEditDescription = ({navigation}) => {

  const [description, setDescription] = useState(UserProxy.getDescription());
  const [loading, setLoading] = useState(false);

  const onSavePress = () => {
    setLoading(true);
    API.userUpdateDescription(description)
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          //console.log(responseJson);
          UserProxy.storeDescription(description);
          navigation.navigate('ProfileEditProfile', {updatedDescription:description})
        } else {
          // Unknown User Update error
          console.log('UNKNOWN ERROR:', responseJson);
        }
        setLoading(false);
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:', error);
        setLoading(false);
      });
  };  

  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>
        <View style={styles.textInput}>
          <TextInput
            style={[styles.inputFieldMultiline, {marginBottom:10}]}
            multiline={true}
            numberOfLines={4}
            onChangeText={(text) => setDescription(text)}
            value={description}
            placeholder={'Add a description'}
          />
        </View>

        <Button
          text={'SAVE'}
          onPress={onSavePress}
          opacity={loading ? 0.3 : 1}
          disabled={loading ? true : false}
        />
      </View>
    </View>
  );

};

export default ProfileEditDescription;

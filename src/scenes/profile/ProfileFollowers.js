import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList
} from 'react-native';

import Colors from '../../common/colors';
import styles from '../../common/styles';
import UserProxy from '../../communication/UserProxy';
import API from '../../communication/api';

const ProfileFollowers = ({navigation, route}) => {

  const {type} = route.params;
  const [loading, setLoading] = useState(true);
  const [userList, setUserList] = useState([]);

  useEffect(() => {
    let isSubscribed = true;
    type == 'following' ?
    API.followings(UserProxy.getId())
      .then((responseJson) => {
        if (isSubscribed) {
          if(responseJson.error == 0) {
            // SUCCESS
            console.log(responseJson);
            UserProxy.storeFollowingList(responseJson.data);
            setUserList(responseJson.data);
            setLoading(false);
            //this.setState({following: responseJson.data.length});
          } else {
            // Unknown Registration error
            console.log('UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:', error);
      })   
    :
    API.followers(UserProxy.getId())
      .then((responseJson) => {
        if (isSubscribed) {
          if(responseJson.error == 0) {
            // SUCCESS
            //console.log(responseJson);
            UserProxy.storeFollowersList(responseJson.data);
            setUserList(responseJson.data);
            setLoading(false);
            //this.setState({followers: responseJson.data.length});
          } else {
            // Unknown Registration error
            console.log('UNKNOWN ERROR:', responseJson);
          }          
        }        
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:', error);
      })    
    return () => isSubscribed = false;
  },[type]);

  const getAvatar = (img) => {
    if (img == null) {
      return (
        require('../../../img/avatar-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/users-images/" + img}
      )
    }
  };

  if (loading) {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.paddedContainer}>
          {/* loading content here */}
        </View>
      </View>      
    )
  }

  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>
        {userList.length > 0 ?
          <FlatList
            data={userList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  onPress={() => navigation.navigate('ProfileOtherUser', {userID:item.id})}
                  style={[styles.listElement, {height:70}]}
                  >
                  <View style={styles.listElementInnerLeft}>
                    <Image
                      source={getAvatar(item.image)}
                      style={styles.followerAvatar}
                    />
                    <View style={styles.followerInfo}>
                      <Text style={styles.defaultText}>{item.username}</Text>
                      <Text style={[styles.smallText, {marginTop:5, color:Colors.greyText}]}>
                        {item.videoMixes} Mixes {'   '}
                        {item.followers} track me
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            }}
          />
          :
          <View style={{flex:1, justifyContent:'center', alignItems: 'center'}}>
            <Text>No users to show</Text>
          </View>
        }        
      </View>
    </View>
  );

};

export default ProfileFollowers;

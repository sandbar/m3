import React from 'react';
import {
  Text,
  View
} from 'react-native';

var TimeFormat = require('hh-mm-ss');
var styles = require('../../common/styles');

module.exports = React.createClass({

  render() {
    var title = this.props.title ? this.props.title : 'Title';
    var progress = this.props.progress;
    var bgColour = '#f26522';
    if(progress <= 0.25) {
      bgColour = '#00bff3';
    } else if(progress <= 0.5) {
      bgColour = '#8393ca';
    } else if(progress <= 0.75) {
      bgColour = '#f7941d';
    }

    return (
      <View style={styles.containerGeneric}>
        <View style={styles.profileRow}>
          <View style={styles.profileRowLeft}>
            <Text style={styles.smallText}>{this.props.title}</Text>
          </View>
          <View style={styles.profileRowRight}>
            <Text style={styles.smallText}>{TimeFormat.fromS(Math.ceil(this.props.total))}</Text>
          </View>
        </View>
        <View style={styles.profileBar}>
          <View style={[styles.profileBarTop, {flex:progress, backgroundColor:bgColour}]} />
          <View style={{flex:1 - progress}} />
        </View>
      </View>
    )
  }

});

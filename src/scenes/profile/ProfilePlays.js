import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  ScrollView,
  Image,
  ActivityIndicator
} from 'react-native';

import PlayList from './PlayList';

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';
import NetworkStatus from '../../utils/NetworkStatus';


const ProfilePlays = () => {

  const [weeklyGoals, setWeeklyGoals] = useState([]);
  const [monthlyGoals, setMonthlyGoals] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let isSubscribed = true;
    if (NetworkStatus.isOnline()) {
      API.goalsMonth()
        .then((responseJson) => {
          if (isSubscribed) {
            if(responseJson.error == 0) {
              // SUCCESS
              //console.log(responseJson.data);
              prepareData(responseJson.data, true);
            } else {
              // Unknown Registration error
              console.log('GOAL: UNKNOWN ERROR:', responseJson);
            }
          }
        })
        .catch((error) => {
          // Request sending/parsing error
          console.log('GOAL: ERROR CATCH:', error);
        });

      API.goalsWeek()
        .then((responseJson) => {
          if (isSubscribed) {
            if(responseJson.error == 0) {
              // SUCCESS
              //console.log("WEEKLY", responseJson.data);
              prepareData(responseJson.data, false);
            } else {
              // Unknown Registration error
              console.log('GOAL: UNKNOWN ERROR:');
              console.warn(responseJson);
            }
          }
        })
        .catch((error) => {
          // Request sending/parsing error
          console.log('GOAL: ERROR CATCH:');
          console.warn(error);
        });
    }
    return () => isSubscribed = false;
  },[]);

  const prepareData = (goals, monthly) => {

    var WEEKLY_GOALS = [];
    var MONTHLY_GOALS = [];

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;

    goals.forEach(myFunction)

    function myFunction(item, index) {
      var startDate = new Date(item.dateStart);
      var endDate = new Date(item.dateEnd);
      var diff_ms = Math.abs(endDate - startDate);
      var diff_s = diff_ms / 1000;
      //console.log("Days => " + diff_ms/ONE_DAY);
      //console.log("Seconds => " + diff_s);

      var goalTotalTime = 3.5 * 60 * 60;

      var goal = {
        name: item.name,
        //totalTime: diff_s,
        watched: item.secondsWatched,
        progress: item.secondsWatched / goalTotalTime
      };

      if (monthly) {
        // It's a Monthly goal
        MONTHLY_GOALS.push(goal);
        // console.log(goal)
      } else {
        // It's a Weekly goal
        WEEKLY_GOALS.push(goal);
      }
    }

    if (monthly) {
      setMonthlyGoals(MONTHLY_GOALS);
    } else {
      setWeeklyGoals(WEEKLY_GOALS);
    }

    setLoading(false);

  }; 

  const weeklyList = () => {
    if (weeklyGoals.length > 0) {
      return(
        <PlayList plays={weeklyGoals}/>
      )
    } else {
      return (
        <View style={{alignSelf:'stretch', alignItems:'center'}}>
          <Text style={styles.smallText}>You have no weekly plays yet. Watch Mixes to generate plays!</Text>
        </View>
      )
    }
  };

  const monthlyList = () => {
    if (monthlyGoals.length > 0) {
      return(
        <PlayList plays={monthlyGoals}/>
      )
    } else {
      return (
        <View style={{alignSelf:'stretch', alignItems:'center'}}>
          <Text style={styles.smallText}>You have no monthly plays set up. Watch Mixes to generate plays!</Text>
        </View>
      )
    }
  };

  if (loading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size='small' color={Colors.brandGreen}/>
      </View>
    )
  }

  
  if (NetworkStatus.isOnline()) {
    return (
      <ScrollView style={styles.containerGeneric}>
        <View style={[styles.profileTabContent, {paddingLeft:0,paddingRight:0,paddingBottom:0}]}>
          <View style={[styles.profileRow, {marginTop:6, marginLeft: 10, alignSelf: 'stretch'}]}>
            <Text style={[styles.header1, {justifyContent:'flex-start', alignSelf: 'stretch', marginBottom:16}]}>This week</Text>
          </View>
          {weeklyList()}
          <View style={[styles.profileRow, {marginTop:16, marginLeft: 10, alignSelf: 'stretch'}]}>
            <Text style={[styles.header1, {justifyContent:'flex-start', alignSelf: 'stretch', marginBottom:16}]}>This month</Text>
          </View>
          {monthlyList()}
        </View>
      </ScrollView>
    )
  }
  else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.profileTabContent}>
          <View style={{flexDirection:'row', marginTop:10}}>
            <Image source={require('../../../img/icon-offline.png')} />
            <Text style={styles.offlineText}>Not available offline</Text>
          </View>
        </View>
      </View>
    )
  }

};

export default ProfilePlays;

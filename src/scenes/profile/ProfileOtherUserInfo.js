import React from 'react';
import {
  Text,
  View,
} from 'react-native';

import styles from '../../common/styles';

const ProfileOtherUserInfo = ({route}) => {

  const {data} = route.params;

  return (
    <View style={[styles.containerGeneric, {backgroundColor: '#fff'}]}>
      <View style={styles.profileTabContent}>
        <Text style={styles.defaultText}>
          { data == null || data == '' ? 'No profile information' : data }
        </Text>
      </View>
    </View>
  );

};

export default ProfileOtherUserInfo;
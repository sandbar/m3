import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  Image,
  ImageBackground,
  FlatList
} from 'react-native';

var TimeFormat = require('hh-mm-ss');

import MixesStorage from '../../sync/MixesStorage';
import Colors from '../../common/colors';
import styles from'../../common/styles';

const ProfileSynced = ({navigation}) => {

  //const {navigation} = props;
  const [loading, setLoading] = useState(true);
  const [mixes, setMixes] = useState([]);
  const [textMsg, setTextMsg] = useState('Loading...');

  useEffect(() => {
    let isSubscribed = true;
    MixesStorage.getStoredMixes()
      .then((mixes) => {
        if (isSubscribed) {
          if(mixes.length == 0) {
            setTextMsg("You don't have any synced mixes");
          } else {
            setMixes(mixes);
            setLoading(false);
          }
        }
      })  
    return () => isSubscribed = false;  
  },[]);

  const pressRow = (rowData) => {
    for (var i = 0; i < mixes.length; i++) {
      if (mixes[i].id === rowData.id) {
        navigation.navigate('ViewVideoMixProfile', {syncedData: mixes[i]});
        return;
      }
    }
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  const getDuration = (duration) => {
    if (duration == null) {
      return '0:00'
    } else {
      var dur = TimeFormat.fromS(duration);
      return dur
    }
  };

  if (loading) {
    return (
      <View style={styles.containerGeneric}>
        <View style={[styles.profileTabContent, {marginTop: 10}]}>
          <Text style={styles.defaultText}>{textMsg}</Text>
        </View>
      </View>
    )
  } else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.profileTabContentVideo}>
          <FlatList
            data={mixes}
            contentContainerStyle = {styles.videoList}
            keyExtractor = {(item, index) => index.toString()}
            renderItem = {({item, index}) => {
              return (
                <TouchableHighlight onPress={() => pressRow(item)} underlayColor={Colors.underlayColor}>
                <View style={styles.videoContainer}>
                  <ImageBackground
                    source={getThumb(item.thumb)}
                    style={styles.videoThumb} >
                    <View style={styles.videoLength}>
                      <Text style={styles.videoLengthText}>
                        {getDuration(item.duration)}
                      </Text>
                    </View>
                    <Image
                      source={require('../../../img/icon-plus.png')}
                      style={[styles.videoPlus, {opacity: item.isPremium ? 1 : 0}]}
                      />            
                  </ImageBackground>
                  <Text style={styles.videoTitle}>{item.title}</Text>
                  <Text style={styles.videoInfo}>{item.author.username}</Text>
                  <Text style={styles.videoInfo}>{/*rowData.views*/}</Text>
                </View>
              </TouchableHighlight>
              )
            }}
          />
        </View>
      </View>
    )
  }

};

export default ProfileSynced;

import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Colors from '../../common/colors';
import ProfileOtherUserMixes from './ProfileOtherUserMixes';
import ProfileOtherUserInfo from './ProfileOtherUserInfo';
import styles from '../../common/styles';
import API from '../../communication/api';

const ProfileOtherUser = ({navigation, route}) => {

  const {userID} = route.params;
  const [following, setFollowing] = useState('');
  const [followers, setFollowers] = useState('');
  const [followingList, setFollowingList] = useState([]);
  const [followersList, setFollowersList] = useState([]);
  const [username, setUsername] = useState('');
  const [infoData, setInfoData] = useState('');
  const [mixesData, setMixesData] = useState([]);
  const [imageData, setImageData] = useState(null);
  const [loading, setLoading] = useState(true);
  const Tab = createMaterialTopTabNavigator();

  useEffect(() => {
    let isSubscribed = true;

    API.profile(userID)
      .then((responseJson) => {
        if (isSubscribed) {
          if(responseJson.error == 0) {
            // SUCCESS
            console.log(responseJson.data)
            setFollowing(responseJson.data.followed);
            setFollowers(responseJson.data.followers);
            setUsername(responseJson.data.username);
            setMixesData(responseJson.data.videoMixes);
            setInfoData(responseJson.data.description);
            setImageData(responseJson.data.image == null ? require('../../../img/avatar-default.png') : {uri: "http://stream.maxmyminutes.com/users-images/" + responseJson.data.image});
            setLoading(false);
          } else {
            // Unknown Registration error
            console.log('PROFILE: UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('GET PROFILE: ERROR CATCH:', error);
      });
      
    API.followings(userID)
      .then((responseJson) => {
        if (isSubscribed) {
          if(responseJson.error == 0) {
            // SUCCESS
            // console.log(responseJson);
            setFollowing(responseJson.data.length);
            setFollowingList(responseJson.data);
          } else {
            // Unknown Registration error
            console.log('UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:', error);
      });      

      API.followers(userID)
      .then((responseJson) => {
        if (isSubscribed) {
          if(responseJson.error == 0) {
            // SUCCESS
            // console.log(responseJson);
            setFollowers(responseJson.data.length);
            setFollowersList(responseJson.data);
          } else {
            // Unknown Registration error
            console.log('UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:', error);
      });      

    return () => isSubscribed = false;
  },[userID]);

  if (loading) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.defaultText}>LOADING...</Text>
      </View>
    )
  }

  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.profileHeader}>
        <View style={styles.profileHeaderMainRow}>
          <TouchableOpacity onPress={() => navigation.navigate('ProfileFollowing', {userList:followingList})}>
            <View style={styles.followerButton}>
              <Text style={styles.followerAmount}>{following}</Text>
              <Text style={styles.followerType}>I track</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.profileAvatar}>
            <Image style={styles.profileAvatarImage} source={imageData} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('ProfileFollowers', {userList:followersList})}>
            <View style={styles.followerButton}>
              <Text style={styles.followerAmount}>{followers}</Text>
              <Text style={styles.followerType}>track me</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Text style={styles.profileUsername}>{username}</Text>
      </View>

      <Tab.Navigator
        initialRouteName="ProfilePlays"
        tabBarOptions={{
          activeTintColor: '#fff',
          inactiveTintColor: Colors.brandLightGreen,
          indicatorStyle: styles.tabBarUnderlineStyle,
          labelStyle: styles.tabBarTextStyle,
          style: styles.tabBar,
        }}
      >
        <Tab.Screen
          name="ProfileOtherUserMixes"
          component={ProfileOtherUserMixes}
          options={{ tabBarLabel: 'MIXES' }}
          initialParams={{mixes: mixesData, username: username}}
        />
        <Tab.Screen
          name="ProfileOtherUserInfo"
          component={ProfileOtherUserInfo}
          options={{ tabBarLabel: 'INFO' }}
          initialParams={{data:infoData}}
        />               

      </Tab.Navigator>
      
    </View>
  );
  
};

export default ProfileOtherUser;
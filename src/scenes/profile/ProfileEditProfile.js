import React, {useState, useEffect, useContext} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform
} from 'react-native';

import ImagePicker from 'react-native-image-picker';

import {AuthContext} from '../../../App';
import API from '../../communication/api';
import AsyncStorage from '../../communication/AsyncStorage';
import Colors from '../../common/colors';
import Button from '../../common/button';
import UserProxy from '../../communication/UserProxy';
import styles from '../../common/styles';
import NetworkStatus from '../../utils/NetworkStatus';

const ProfileEditProfile = ({navigation, route}) => {

  const { signOut } = useContext(AuthContext);
  const [imageData, setImageData] = useState(UserProxy.getImage() == null ? require('../../../img/avatar-default.png') : {uri: "http://stream.maxmyminutes.com/users-images/" + UserProxy.getImage()});
  const [description, setDescription] = useState(UserProxy.getDescription() == null ? 'Say something about yourself..' : UserProxy.getDescription());
  const phoneNumber = UserProxy.getPhoneNumber();
  const isPremiumUser = UserProxy.getIsPremium();
  const isSubscribed = UserProxy.getIsSubscribed();

  navigation.setOptions({
    headerLeft: () => (
      <TouchableOpacity
        style={{left:20}}
        onPress={() => navigation.navigate('Profile', {refreshView:true})}
      >
        <Image source={require('../../../img/btn-back.png')} />
      </TouchableOpacity>
    )
  });  

  useEffect(() => {
    let isSubscribed = true
    if (route.params?.updatedDescription) {
      isSubscribed && setDescription(route.params?.updatedDescription);
    }
    if (route.params?.refreshView) {
      isSubscribed && setImageData({uri: "http://stream.maxmyminutes.com/users-images/" + UserProxy.getImage()});
    }

    return () => isSubscribed = false;
  },[route.params?.updatedDescription, route.params?.refreshView]);

 const selectPhotoTapped = () => {
  const options = {
    title: 'Photo Picker',
    takePhotoButtonTitle: 'Take Photo...',
    chooseFromLibraryButtonTitle: 'Choose from Library...',
    quality: 0.5,
    maxWidth: 300,
    maxHeight: 300,
    allowsEditing: false,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    // console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
      //console.log(response)
      var source;

      // You can display the image using either:
      // source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};
     
      //Or:

      if (Platform.OS === 'android') {
        source = {uri: response.uri, isStatic: true};
      } else {
        source = {uri: response.uri.replace('file://', ''), isStatic: true};
      }

      setImageData(source)
        // Upload image
      var name = response.uri.replace(/^.*[\\\/]/, '');
        //console.log(fileName)
      var contentType = 'image/jpg';
        //navigation.navigate('ProfileImageUploading', {fileName:response.fileName, response:response, name:name, contentType:contentType, returnView:'ProfileEditProfile'})
       navigation.navigate('ProfileImageUploading', {imageData:response, fileName:name, contentType:contentType, returnView:'ProfileEditProfile'})
      }
    });
  };

  if (NetworkStatus.isOnline()) {
    return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={[styles.profileHeader, {height: 130}]}>
        <View style={styles.profileHeaderMainRow}>
          <TouchableOpacity style={styles.profileAvatar} onPress={() => selectPhotoTapped()}>
            <Image style={styles.profileAvatarImage} source={imageData} />
          </TouchableOpacity>
          <Image source={require('../../../img/icon-edit.png')} style={styles.editAvatar} />
        </View>
      </View>
      <View style={[styles.paddedContainer, {paddingTop: 0}]}>

        <TouchableOpacity
          onPress={() => navigation.navigate('ProfileEditDescription')}
          underlayColor={Colors.underlayColor}
          style={styles.listElement}
          >
          <View style={styles.listElementInner}>
            <View style={styles.listElementLeft}>
              <Text style={styles.mediumText} numberOfLines={1}>
              {description}
              </Text>
            </View>
            <View style={styles.listElementRight}>
              <Image source={require('../../../img/icon-edit.png')} />
              <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.listElement}>
          <View style={styles.listElementInner}>
            <View style={[styles.listElementLeft, {flex:1}]}>
              <Text style={styles.mediumText}>
                My number
              </Text>
            </View>
            <View style={styles.listElementRight}>
              <Text style={[styles.mediumText, {color: Colors.disabledGreyText}]}>
                {phoneNumber}
              </Text>
            </View>
          </View>
        </View>
        <View style={[styles.listElement, {marginBottom: 20}]}>
          <View style={styles.listElementInner}>
            <View style={[styles.listElementLeft, {flex:1}]}>
              <Text style={styles.mediumText}>
                Subscription
              </Text>
            </View>
            <View style={styles.listElementRight}>
              <Text style={[styles.mediumText, {color: Colors.disabledGreyText}]}>
                {isSubscribed || isPremiumUser ? 'Plus' : 'Free'}
              </Text>
            </View>
          </View>
        </View>

          {//!isSubscribed || !isPremiumUser && // is this needed?
            // <Button
            //   text={'SUBSCRIBE TO PLUS'}
            //   onPress={onSubscribePress}
            //   opacity={loading ? 0.5 : 1}
            //   disabled={loading ? true : false}
            //   stretch={'stretch'}
            // />
          }
            <Button
              text={'LOGOUT'}
              onPress={onLogoutPress}
              stretch={'stretch'}
             />          
        </View>
      </View>
    )
  } else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.loadingContainer}>
          <View style={{flexDirection:'row', marginTop:10}}>
            <Image source={require('../../../img/icon-offline.png')} />
            <Text style={styles.offlineText}>Not available offline</Text>
          </View>
        </View>
      </View>
    )
  };

  function onLogoutPress() {
    AsyncStorage.removeToken()
    .then(() => {
      API.logout()
      .then (() => signOut())
      .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
  };

};

export default ProfileEditProfile;

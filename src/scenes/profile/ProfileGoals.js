import React from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  Image
} from 'react-native';

import {Actions} from 'react-native-router-flux';
import Colors from '../../common/colors';
import ProfileBar from './ProfileBar';
import GoalsList from './GoalsList';
var styles = require('../../common/styles');
var API = require('../../communication/api');
var NetworkStatus = require('../../utils/NetworkStatus');


module.exports = React.createClass({

  getInitialState() {
    return {
      weeklyGoals: [],
      monthlyGoals: []
    };
  },

  componentWillMount() {
    if(NetworkStatus.isOnline()) {
      this.sendRequest();
    }
  },

  render() {
    if(NetworkStatus.isOnline()) {
      return (
        <View style={styles.containerGeneric}>
          <View style={styles.profileTabContent}>
            <View style={styles.profileRow}>
              <View style={[styles.profileRowLeft, {height: 10, marginBottom:0}]}>
                <Text style={styles.header1}>This week</Text>
              </View>
              <View style={[styles.profileRowRight, {alignItems:'flex-start'}]}>
                <TouchableOpacity onPress={() => Actions.profileEditGoals({weeklyGoals:this.state.weeklyGoals, monthlyGoals:this.state.monthlyGoals})}>
                  <View style={{flexDirection:'row', alignItems: 'center', marginTop:0}}>
                    <Text style={styles.profileEditText}>Edit goals</Text>
                    <Image source={require('../../../img/icon-settings-sm.png')} />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            {this.weeklyList()}
            <View style={[styles.profileRow, {marginTop:16, alignSelf: 'stretch'}]}>
              <Text style={[styles.header1, {justifyContent:'flex-start', alignSelf: 'stretch'}]}>This month</Text>
            </View>
            {this.monthlyList()}
          </View>
        </View>
      )
    }
    else {
      return (
        <View style={styles.containerGeneric}>
          <View style={styles.profileTabContent}>
            <View style={{flexDirection:'row', marginTop:10}}>
              <Image source={require('../../../img/icon-offline.png')} />
              <Text style={styles.offlineText}>Not available offline</Text>
            </View>
          </View>
        </View>
      )
    }
  },

  weeklyList() {
    if (this.state.weeklyGoals.length > 0) {
      return(
        <GoalsList goals={this.state.weeklyGoals}/>
      )
    } else {
      return (
        <View style={{alignSelf:'stretch'}}>
          <Text style={[styles.smallText, {alignItems:'flex-start', marginTop: 16}]}>You have no weekly goals set up. Watch Mixes to generate goals!</Text>
        </View>
      )
    }
  },

  monthlyList() {
    if (this.state.monthlyGoals.length > 0) {
      return(
        <GoalsList goals={this.state.monthlyGoals}/>
      )
    } else {
      return (
        <View style={{alignSelf:'stretch'}}>
          <Text style={[styles.smallText, {alignItems:'flex-start', marginTop: 16}]}>You have no monthly goals set up. Watch Mixes to generate goals!</Text>
        </View>
      )
    }
  },

  sendRequest() {
    API.goalsMonth()
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          //console.log(responseJson.data);
          this.prepareData(responseJson.data, true);
        } else {
          // Unknown Registration error
          console.log('GOAL: UNKNOWN ERROR:');
          console.warn(responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('GOAL: ERROR CATCH:');
        console.warn(error);
      });

    API.goalsWeek()
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          //console.log("WEEKLY", responseJson.data);
          this.prepareData(responseJson.data, false);
        } else {
          // Unknown Registration error
          console.log('GOAL: UNKNOWN ERROR:');
          console.warn(responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('GOAL: ERROR CATCH:');
        console.warn(error);
      });

  },

  prepareData(goals, monthly) {

    var WEEKLY_GOALS = [];
    var MONTHLY_GOALS = [];

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;

    goals.forEach(myFunction)

    function myFunction(item, index) {
      var startDate = new Date(item.dateStart);
      var endDate = new Date(item.dateEnd);
      var diff_ms = Math.abs(endDate - startDate);
      var diff_s = diff_ms / 1000;
      //console.log("Days => " + diff_ms/ONE_DAY);
      //console.log("Seconds => " + diff_s);

      var goalTotalTime = 3.5 * 60 * 60;

      var goal = {
        name: item.name,
        //totalTime: diff_s,
        watched: item.secondsWatched,
        progress: item.secondsWatched / goalTotalTime
      };

      if(monthly) {
        // It's a Monthly goal
        MONTHLY_GOALS.push(goal);
      } else {
        // It's a Weekly goal
        WEEKLY_GOALS.push(goal);
      }
    }

    if (monthly) {
      this.setState({monthlyGoals: MONTHLY_GOALS})
    } else {
      this.setState({weeklyGoals: WEEKLY_GOALS})
    }    

  }

});

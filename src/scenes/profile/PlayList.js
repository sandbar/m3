import React from 'react';
import {
  View
} from 'react-native';

import PlayItem from './PlayItem';
import styles from '../../common/styles';

const PlayList = (props) => {
  return (
    <View
      //style={{alignSelf: 'stretch', backgroundColor: 'red'}}
      //contentContainerStyle={styles.profilePlayList}
      style={styles.profilePlayList}
    >
    {
      props.plays.map(function(play, i) {
        return (
          <PlayItem
            key={i}
            title={play.name}
            progress={play.progress}
            total={play.watched}
          />
        )
      })
    }
    </View>
  );

};

export default PlayList;

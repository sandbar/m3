import React, {useState} from 'react';
import {
  TextInput,
  View,
} from 'react-native';

import Button from '../../common/button';
import styles from '../../common/styles';

const MixDescription = ({navigation, route}) => {

  const {description, returnView} = route.params;
  const [desc, setDescription] = useState(description == 'Enter a description' ? '' : description);
  const [loading, setLoading] = useState(true);

  const onSavePress = () => {
    // TODO check this is correct
    navigation.navigate(returnView, {description: desc});
  };

  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>
        <View style={[styles.textInput, {marginBottom:12}]}>
          <TextInput
            style={styles.inputFieldMultiline}
            multiline={true}
            numberOfLines={4}
            placeholder={'Add a description'}
            onChangeText={(text) => {
              setDescription(text); 
              setLoading(false);
            }}
            value={desc}
          />
        </View>

        <Button
          text={'SAVE'}
          onPress={onSavePress}
          opacity={loading ? 0.3 : 1}
          disabled={loading ? true : false}
        />
      </View>
    </View>
  );

};

export default MixDescription;
import React, {useState, useEffect} from 'react';
import {
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  Keyboard,
  LayoutAnimation,
  Dimensions
} from 'react-native';

import Button from '../../common/button';
import API from '../../communication/api';
import styles from '../../common/styles';

const MixTags = ({navigation, route}) => {

  const {width, height} = Dimensions.get('window');
  const {tags, tagIDs, returnView} = route.params;
  const [currentTags, setCurrentTags] = useState(tags == undefined ? [] : tags);
  const [currentTagsIDs, setCurrentTagsIDs] = useState(tagIDs == undefined ? [] : tagIDs);
  const [allTags, setAllTags] = useState([]);
  const [allTagIDs, setAllTagIDs] = useState([]);
  const [matchingTags, setMatchingTags] = useState([]);
  const [tag, setTag] = useState('');
  const [loading, setLoading] = useState(true);
  const [overlayPosition, setOverlayPosition] = useState(width);
  const [tagHolderHeight, setTagHolderHeight] = useState(height - 60);

  useEffect(() => {
    let isSubscribed = true; 
    API.tag()
      .then((responseJson) => {
        if(responseJson.error == 0) {
          if (isSubscribed) {
            let tmpTags = [];
            let tmpTagIDs = [];
            for (var i=0; i<responseJson.data.length; i++) {
              //console.log("**", data[i].name)
              tmpTags.push(responseJson.data[i].name);
              tmpTagIDs.push(responseJson.data[i].id);
            }
            setAllTags(tmpTags);
            setAllTagIDs(tmpTagIDs);
            setLoading(false);
          }
          // SUCCESS
          //console.log(responseJson);
          //parseResponse();
        } else {
          // Unknown Registration error
          console.log('TAGS: UNKNOWN ERROR:');
          console.warn(responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('TAGS: ERROR CATCH:');
        console.warn(error);
      });
    return () => isSubscribed = false;
  },[]);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => {
      let newSize = height - e.endCoordinates.height;
      let keyboardHeight = height - newSize;
      setTagHolderHeight(height - keyboardHeight - 118);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeNone);   
    });
    return () => keyboardDidShowListener.remove();
  }, [Keyboard]);

  useEffect(() => {
    const keyboardHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setTagHolderHeight(height - 60);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeNone);    
    });
    return () => keyboardHideListener.remove();
  }, [Keyboard]);  

  const compareTags = (text) => {
    setTag(text); 
    setOverlayPosition(width);
    //reset matching tags array
    //this.state.matchingTags.splice(0, this.state.matchingTags.length);
    setMatchingTags([]);
    var tmpText = text.toLowerCase();
    let tmpTagArray = matchingTags;
    for (var i=0; i<allTags.length; i++) {
      //console.log(allTags[i], tmpText);
      var tmpCompareText = allTags[i].toLowerCase();
      //console.log(tmpCompareText.indexOf(tmpText))
      if (tmpCompareText.indexOf(tmpText) > -1) {
        //console.log("match");
        tmpTagArray.push(allTags[i]);
      }
      setMatchingTags(tmpTagArray);
    }
    // if there is a match, show overlay
    if (tmpTagArray.length > 0) {
      setOverlayPosition(0);
    }
  };

  const getCurrentTags = () => {
    return currentTags.map((tag, i) => {
      return (
        <View style={[styles.tagContainer, {alignSelf:'stretch'}]} key={i}>
          <View style={styles.tag}>
            <Text style={styles.smallText}>{tag}</Text>
          </View>
          <TouchableOpacity style={styles.deleteTag} onPress={() => onDeleteTagPress(tag)}>
            <Text style={styles.smallText}>x</Text>
          </TouchableOpacity>
        </View>
      )
    });
  };

  const onTagPress = (tag) => {
    console.log(tag);
    let tmpTagArray = currentTags;
    let tmpTagIDArray = currentTagsIDs;
    if (currentTags.indexOf(tag) == -1) {
      tmpTagArray.push(tag);
      // keep track of the tag ID for creating mix
      tmpTagIDArray.push(allTagIDs[allTags.indexOf(tag)])
      setCurrentTags(tmpTagArray);
      setCurrentTagsIDs(tmpTagIDArray);
      setOverlayPosition(width);
      setLoading(false);
    }
  };

  const onDeleteTagPress = (tag) => {
    setLoading(true);
    var index = currentTags.indexOf(tag);
    console.log(index)
    // let tmpTagArray = currentTags;
    // let tmpTagIDArray = currentTagsIDs;
    if (index !== -1) {
      currentTags.splice(index, 1);
      console.log(currentTags)
      // keep track of the tag ID for creating mix
      currentTagsIDs.splice(index, 1);
      // setCurrentTags(tmpTagArray);
      // setCurrentTagsIDs(tmpTagIDArray);
      setOverlayPosition(width);
      //getCurrentTags();
      setLoading(false);
    } else {
      setLoading(false);
    }
  }; 

  const onSavePress = () => {
    navigation.navigate(returnView, {tags: currentTags, tagIDs: currentTagsIDs });
  };
  
  if (loading) {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.loadingContainer}>
            <Text style={styles.offlineText}></Text>
        </View>
      </View>      
    )
  } else {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.paddedContainer}>
          <View style={[styles.textInput, {marginBottom: 10}]}>
            <TextInput
              style={styles.inputFieldFW}
              multiline={false}
              numberOfLines={1}
              placeholder={'Enter a tag'}
              onChangeText={(text) => compareTags(text)}
              onFocus={() => compareTags('')}
              value={tag}
            />
          </View>

          <View style={{marginTop:0, marginBottom:10}}>
            { 
              getCurrentTags()
            }
          </View>

          <Button
            text={'SAVE'}
            onPress={onSavePress}
            opacity={loading ? 0.3 : 1}
            disabled={loading ? true : false}
          />

          <ScrollView
            style={[styles.tagOverlayContainer, {left:overlayPosition, top: 60, height: tagHolderHeight, paddingBottom:10}]}
            keyboardShouldPersistTaps={'always'}
          >
            {matchingTags &&
              matchingTags.map((tag, i) => {
                return (
                  <TouchableOpacity style={{marginBottom:20}} key={i} onPress={() => onTagPress(tag)}>
                    <Text style={styles.defaultText}>{tag}</Text>
                  </TouchableOpacity>
                )
              })
            }
          </ScrollView>

        </View>
      </View>
    );
  }
  
};

export default MixTags;

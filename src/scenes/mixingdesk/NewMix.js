import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  Alert,
  Switch
} from 'react-native';

var TimeFormat = require('hh-mm-ss')

import Colors from '../../common/colors';
import Button from '../../common/button';
import API from '../../communication/api';
import styles from '../../common/styles';
import NetworkStatus from '../../utils/NetworkStatus';
import UserProxy from '../../communication/UserProxy';

const NewMix = ({navigation, route}) => {

  const isPremiumUser = UserProxy.getIsPremium();

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('Enter a description');
  const [tags, setTags] = useState([]);
  const [tagIDs, setTagIDs] = useState([]);
  const [clips, setClips] = useState([]);
  const [clipsOrder, setClipsOrder] = useState([])
  const [length, setLength] = useState(0);
  const [lengthString, setLengthString] = useState(TimeFormat.fromS(0));
  const [loading, setLoading] = useState(true);
  const [thumbID, setThumbID] = useState(0);
  const [scrollEnabled, setScrollEnabled] = useState(true);
  const [isPlus, setIsPlus] = useState(false);

  useEffect(() => {
    if (route.params?.description) {
      setDescription(route.params.description);
    }
    if (route.params?.clip) {
      let myClip = route.params?.clip;
      if (clips.indexOf(myClip) == -1) {
        clips.push(myClip);
        let tmpLength = myClip.duration == 0 ? 60 : myClip.duration
        setLength(tmpLength);
        setLengthString(TimeFormat.fromS(tmpLength));
        setThumbID(clips.length > 1 ? 1 : 0);
        checkCanPublish();
      }
    }
    if (route.params?.sortedClips) {
      //console.log('setting clips', route.params?.sortedClips)
      setClips(route.params?.sortedClips);
    }    
  },[route.params?.description, route.params?.clip, route.params?.sortedClips]);

  useEffect(() => {
    console.log('CHECKING PUBLISH');
    checkCanPublish();
  },[title, description, tagIDs, clips]);

  const checkCanPublish = () => {
    if(title !== '' && description !== 'Enter a description' && tagIDs !== [] && clips !== []) {
      setTimeout(setToPublish, 1000);
    }
  };

  const setToPublish = () => {
    setLoading(false);
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  const onPublishMixPress = () => {
    //create array of clip IDs
    var clipIDs = [];
    for (var i = 0; i < clips.length; i++) {
      clipIDs.push(clips[i].id);
    }
    API.videoMixCreate(title, description, tagIDs, clipIDs, clips[thumbID].thumb, length, isPlus, responseHandler)
  };

  const responseHandler = (response) => {
    console.log(response);

    if (response.indexOf(': 1,') > -1) {
      var alertMessage = response.msg;
      Alert.alert(
        'Upload error',
        alertMessage,
        [
          {text: 'OK', onPress: () => Actions.pop()},
        ]
      )
    } else {
      // Actions.pop();
      // Actions.refresh({refresh:true});
      navigation.navigate('MixingDesk', {refreshView: true})
    }
  }; 


  if (NetworkStatus.isOnline()) {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <ScrollView
          style={styles.paddedContainer}
          //ref='scrollView'
          scrollEnabled={scrollEnabled}
          >

            <View style={[styles.textInput, {marginBottom:0}]}>
              <TextInput
                style={styles.inputFieldFW}
                placeholder={'Enter a title'}
                placeholderTextColor={Colors.defaultInputText}
                value={title}
                onChangeText={(text) => setTitle(text)}
                underlineColorAndroid='transparent'
              />
            </View>

          <TouchableOpacity 
            style={styles.newMixElement} 
            onPress={() => navigation.navigate('MixDescription', {description: description, returnView: 'NewMix'})}
          >
            <View style={styles.listElementLeft}>
            <Text numberOfLines={1} style={styles.mediumText}>{description}</Text>
            </View>
            <View style={styles.listElementRight}>
              <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity 
            style={styles.newMixElement} 
            onPress={() => navigation.navigate('MixTags', {tags:tags, tagIDs:tagIDs, returnView:'NewMix'})}
          >
            <View style={styles.listElementLeft}>
              <View style={styles.tagContainer}>
                {tags.length == 0 ?
                  <Text style={[styles.mediumText, {marginTop:5}]}>Choose tags</Text>
                :
                  tags.map((tag) => {
                    return (
                      <View style={styles.tag} key={tag}>
                        <Text style={styles.smallText}>{tag}</Text>
                      </View>
                    )
                  })                
                }                  
              </View>
            </View>
            <View style={styles.listElementRight}>
              <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
            </View>
          </TouchableOpacity>

          <View style={styles.newMixElement}>
            <View style={styles.listElementLeft}>
              <Text style={styles.mediumText}>Length</Text>
            </View>
            <View style={styles.listElementRight}>
              <Text style={styles.mediumText}>{lengthString}</Text>
            </View>
          </View>

          {clips.length > 0 &&
            <View style={{marginTop:10}}>
            {clips.map((clip) => {
              return (
                <TouchableOpacity
                  key={clip.id}
                  //onLongPress={() => navigation.navigate('MixOrderClips', {data: clips, order: clipsOrder})}
                  underlayColor={Colors.underlayColor}>
                  <View style={styles.clipsContainer}>
                    <Image
                      source={getThumb(clip.thumb)}
                      style={styles.clipThumb}
                    />
                    <View>
                      <Text style={styles.clipTitle}>{clip.title}</Text>
                    </View>
                  </View>
                </TouchableOpacity>              
              )
            })}
            </View>
          }
          
          {clips.length > 0 &&
          <TouchableOpacity 
            style={styles.newMixElement} 
            onPress={() => navigation.navigate('MixOrderClips', {data: clips, order: clipsOrder, returnView: 'NewMix'})}
          >
            <View style={styles.listElementLeft}>
              <Text style={styles.mediumText}>Order video clips</Text>
            </View>
            <View style={styles.listElementRight}>
              <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
            </View>
          </TouchableOpacity>  
          }          

          <TouchableOpacity style={[styles.newMixElement, {marginBottom: isPremiumUser ? 0 : 16}]} 
            onPress={() => navigation.navigate('NewMixChooseVid', {returnView:'NewMix'})}
          >
            <View style={styles.listElementLeft}>
              <Text style={styles.mediumText}>Add a video clip</Text>
            </View>
            <View style={styles.listElementRight}>
              <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
            </View>
          </TouchableOpacity>

          {isPremiumUser &&
            <View style={[styles.newMixElement, {marginBottom: 16}]}>
              <View style={styles.listElementLeft}>
                <Text style={styles.mediumText}>Plus Mix</Text>
              </View>
              <View style={styles.listElementRight}>
                <Switch
                  value={isPlus}
                  onValueChange={(value) => setIsPlus(value)}/>
              </View>
            </View>
          }

          <Button
            text={'BROADCAST'}
            onPress={onPublishMixPress}
            opacity={loading ? 0.3 : 1}
            disabled={loading ? true : false}
          />

          <Text style={[styles.smallText, {marginTop: 16, marginBottom: 16}]}>*By publishing I acknowledge that my content will be publicly available through the M:3 app service </Text>

        </ScrollView>
      </View>
    )
  } else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.loadingContainer}>
          <View style={{flexDirection:'row', marginTop:10}}>
            <Image source={require('../../../img/icon-offline.png')} />
            <Text style={styles.offlineText}>Not available offline</Text>
          </View>
        </View>
      </View>
    )
  }

};

export default NewMix;

import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList
} from 'react-native';

import styles from '../../common/styles';
import API from '../../communication/api';

const Categories = ({navigation, route}) => {

  const {returnView} = route.params;
  const [filterID, setFilterID] = useState(null);
  const [categoriesData, setCategoriesData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let isSubscribed = true;
    //setLoading(true);
    API.categories()
      .then((responseJson) => {
        if (isSubscribed) {
          if (responseJson.error == 0) {
            // SUCCESS
            //setLoading(false);
            //console.log(responseJson.data);
            setCategoriesData(responseJson.data);
            setLoading(false);
          } else {
            // Unknown Registration error
            console.log('CATEGORIES: UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('CATEGORIES: ERROR CATCH:', error);
      });
    return () => isSubscribed = false;
  },[]);

  useEffect(() => {
    let isSubscribed = true;
    if (route.params?.filterID) {
      if (isSubscribed) {
        setFilterID(route.params?.filterID);
      }
    }
    return () => isSubscribed = false;
  },[route.params?.filterID])

  const pressRow = (category) => {  
    navigation.navigate(returnView, {filter: category});
  };

  const getTickOpacity = (categoryID) => {
    if (filterID) {
      if (filterID == categoryID) {
        return 1;
      }
    }
    return 0;
  };

  if (loading) {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.loadingContainer}>
          <Text style={styles.offlineText}>Loading...</Text>
        </View>
      </View>        
    )
  }
  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>
        {returnView === 'NewMixChooseVid' &&
          <TouchableOpacity
            onPress={() => pressRow({id:-1})}
            style={[styles.listElement, {height:60}]}
            >
            <View style={styles.listElementInnerLeft}>
              <Text style={styles.mediumText}>All categories</Text>
            </View>
            <View style={[styles.listElementRight, {opacity: getTickOpacity(-1)}]}>
              <Image source={require('../../../img/icon-tick.png')} style={styles.listArrow} />
            </View>
          </TouchableOpacity>        
        }
        {categoriesData.length > 0 ?
        <FlatList
          data={categoriesData}
          contentContainerStyle = {styles.list}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => pressRow(item)}
              style={[styles.listElement, {height:60}]}
              >
              <View style={styles.listElementInnerLeft}>
                <Text style={styles.mediumText}>{item.name}</Text>
              </View>
              <View style={[styles.listElementRight, {opacity: getTickOpacity(item.id)}]}>
                <Image source={require('../../../img/icon-tick.png')} style={styles.listArrow} />
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => index.toString()}
        />        
        :
        <View style={{flex:1, justifyContent:'center', alignItems: 'center'}}>
          <Text>No categories to show</Text>
        </View>
        }         
      </View>
    </View>
  );  

};

export default Categories;

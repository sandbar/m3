import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image,
} from 'react-native';

import Colors from '../../common/colors';
import Button from '../../common/button';
var styles = require('../../common/styles');

module.exports = React.createClass({

  render() {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.fsHint}>
          <Image source={require('../../../img/fs-branding.png')} style={styles.fsHint}>
            <Text style={styles.fsHintText}>M:3 NEEDS ACCESS TO YOUR CAMERA</Text>
            <TouchableHighlight
              style={styles.fsHintButton}
              underlayColor={Colors.brandGreen}
              onPress={this.onAccessPress}
              >
              <Text style={styles.buttonText}>ALLOW ACCESS</Text>
            </TouchableHighlight>
          </Image>
        </View>
      </View>
    )
  },

  onAccessPress() {
    //
  }

});

import React, {useState, useEffect, useLayoutEffect} from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  ImageBackground,
  Animated,
  ActivityIndicator
} from 'react-native';

import { SwipeListView } from 'react-native-swipe-list-view';
var TimeFormat = require('hh-mm-ss');

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';
import NetworkStatus from '../../utils/NetworkStatus';
import UserProxy from '../../communication/UserProxy';

const MixingDesk = ({navigation, route}) => {

  const [loading, setLoading] = useState(true);
  const [mixes, setMixes] = useState([]);
  let animatedValues = [];

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => ( 
        <TouchableOpacity
          style={{left:20}}
          hitSlop={{top:10, right:10, bottom:10, left:10}}
          onPress={() => navigation.toggleDrawer()}
        >
          <Image source={require('../../../img/btn-hamburger.png')} />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity 
          style={{marginRight: 20}}
          hitSlop={{top:10, right:10, bottom:10, left:10}}
          onPress={() => navigation.navigate('NewMix')} >
          <Image source={require('../../../img/btn-add.png')} />
        </TouchableOpacity>
      )
    });
  },[navigation]);  
 

  useEffect(() => {
    let isSubscribed = true;
    if (NetworkStatus.isOnline()) {
      API.videoMixes()
      .then((responseJson) => {
        if (isSubscribed) {
          if (responseJson.error == 0) {
            // SUCCESS
            //console.log("videoMixes", responseJson.data)
            for (var i = 0; i < responseJson.length; i++) {
              //create animated values for each mix image
              animatedValues.push(new Animated.Value(0));
            }              
            setMixes(responseJson.data);
            setLoading(false);
          } else {
            // Unknown Registration error
            console.log('MIXING DESK: UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('MIXING DESK: ERROR CATCH:', error);
      });
    }
    return () => isSubscribed = false;
  },[route.params?.refreshView]);

  const pressRow = (rowData) => {
    navigation.navigate('EditMix', {mixData: rowData});
  };

  const pressHiddenRow = (mixID) => {
    //console.log(mixID);
    API.videoMixDelete(mixID)
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          // get data again
        } else {
          console.log('DELETE MIX: UNKNOWN ERROR:', responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('DELETE MIX: ERROR CATCH:', error);
      });    
  };

  const imgLoaded = (rowID) => {
    Animated.timing(
       animatedValues[rowID],
       {
         toValue: 1,
         duration: 500,
         useNativeDriver: true
       }
     ).start();
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  const getDuration = (duration) => {
    if (duration == null || duration == NaN || duration == undefined) {
      return (
        '0:00'
      )
    } else {
      var dur = TimeFormat.fromS(parseInt(duration));
      return (
        dur
      )
    }
  };  

  if (loading) {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.loadingContainer}>
          <ActivityIndicator size='small' color={Colors.brandGreen}/>
        </View>
    </View>        
    )
  }

  if (NetworkStatus.isOnline()) {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <Text style={[styles.mixingDeskTitle, {opacity: mixes.length > 0 ? 1 : 0}]}>MY MIXES</Text>
        {mixes.length > 0 ?
          <SwipeListView
            useFlatList={true}
            data={mixes}
            numColumns={2}
            keyExtractor={(index) => index.toString()}
            contentContainerStyle = {styles.videoList}
            renderItem={ (rowData, rowMap) => (
              <TouchableHighlight onPress={() => pressRow(rowData.item)} underlayColor={'#FFF'}>
                <View style={styles.videoContainer}>
                  <ImageBackground
                    source={getThumb(rowData.item.thumb)}
                    style={[styles.videoThumb]} //{opacity:animatedValues[rowMap]}]}
                    //onLoad={ () => imgLoaded(rowMap) }
                  >
                    <View style={styles.videoLength}>
                      <Text style={styles.videoLengthText}>
                        {getDuration(rowData.item.mixDuration)}
                      </Text>
                    </View>
                    <Image
                      source={require('../../../img/icon-plus.png')}
                      style={[styles.videoPlus, {opacity: rowData.item.isPremium ? 1 : 0}]}
                      />            
                  </ImageBackground>
                  <Text style={styles.videoTitle} numberOfLines={1}>{rowData.item.title}</Text>
                  <Text style={styles.videoInfo}>{UserProxy.getUsername()}</Text>
                  <Text style={styles.videoInfo}>{rowData.item.views == null ? '0' : rowData.item.views} plays</Text>
                </View>
                </TouchableHighlight>
            )}
            renderHiddenRow={(rowData, rowMap) => (
              <View style={styles.videoHiddenRow}>
                <TouchableOpacity
                  onPress={() => pressHiddenRow(rowData.item.id)}
                  style={styles.videoDelete}
                >
                    <Text style={[styles.defaultText, {color:'#FFF'}]}>DELETE</Text>
                </TouchableOpacity>
              </View>
            )}
            disableRightSwipe
            rightOpenValue={-75}
          />     
          :
          <View style={styles.loadingContainer}>
            <Text style={styles.defaultText}>You dont have any mixes yet</Text>
          </View>
        }
      </View>
    )
  } else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.loadingContainer}>
          <View style={{flexDirection:'row', marginTop:10}}>
            <Image source={require('../../../img/icon-offline.png')} />
            <Text style={styles.offlineText}>Not available offline</Text>
          </View>
        </View>
      </View>
    )
  }

  // parseResponse(response_data) {
  //   if(response_data.length > 0) {
  //     var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  //     var animatedValues = [];
  //     for (var i = 0; i < response_data.length; i++) {
  //       //create animated values for each mix image
  //       animatedValues.push(new Animated.Value(0));
  //     }
  //     this.setState({
  //       dataSource: ds.cloneWithRows(this.genRows(response_data)),
  //       animatedValues: animatedValues,
  //     });
  //   } else {
  //     // Sorry, looks like we got empty array in return
  //   }
  // },

};

export default MixingDesk;

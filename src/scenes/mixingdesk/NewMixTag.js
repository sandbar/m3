import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView
} from 'react-native';

import Colors from '../../common/colors';
import styles from '../../common/styles';

const NewMixTag = () => {

  const tag = '';
  const [title, setTitle] = useState('');

  return (
    <View style={styles.containerGeneric}>
      <ScrollView style={styles.paddedContainer}>

        <View style={[styles.textInput, {marginBottom:10}]}>
          <TextInput
            style={styles.inputFieldFW}
            placeholder={'Enter a tag'}
            placeholderTextColor={Colors.defaultInputText}
            value={tag}
            onChangeText={(text) => setTitle(text)}
            underlineColorAndroid='transparent'
          />
        </View>

        <View style={{flexDirection: 'row'}}>
          <View style={styles.listElementLeft}>
            <View style={styles.tagContainer}>
              <View style={styles.tag}>
                <Text style={styles.smallText}>Yoga</Text>
              </View>
            </View>
          </View>
          <View style={styles.listElementRight}>
            <TouchableOpacity>
              <View style={styles.tag}>
                <Text style={styles.smallText}>x</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{flexDirection: 'row'}}>
          <View style={styles.listElementLeft}>
            <View style={styles.tagContainer}>
              <View style={styles.tag}>
                <Text style={styles.smallText}>Yoga</Text>
              </View>
            </View>
          </View>
          <View style={styles.listElementRight}>
            <TouchableOpacity>
              <View style={styles.tag}>
                <Text style={styles.smallText}>x</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

      </ScrollView>
    </View>
  );


};

export default NewMixTag;

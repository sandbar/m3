import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  Alert,
  Switch
} from 'react-native';

var TimeFormat = require('hh-mm-ss')

import Colors from '../../common/colors';
import Button from '../../common/button';
import styles from '../../common/styles';
import API from '../../communication/api';
import UserProxy from '../../communication/UserProxy';

const EditMix = ({navigation, route}) => {

  const {mixData} = route.params;

  const id = mixData.id;
  const isPremiumUser = UserProxy.getIsPremium();

  const [title, setTitle] = useState(mixData.title);
  const [description, setDescription] = useState(mixData.description == null ? 'Enter a description' : mixData.description);
  const [duration, setDuration] = useState(mixData.mixDuration == undefined ? 0 : mixData.mixDuration);
  const [tags, setTags] = useState(null);
  const [tagIDs, setTagIDs] = useState([]);
  const [thumbID, setThumbID] = useState(0);
  const [clips, setClips] = useState(null);
  const [clipsOrder, setClipsOrder] = useState([])
  const [loading, setLoading] = useState(true);
  const [isPlus, setIsPlus] = useState(mixData.isPremium);

  // const [length, setLength] = useState(0);
  // const [lengthString, setLengthString] = useState('0:00');  
  // const [listMsg, setListMsg] = useState('');

  useEffect(() => {
    //console.log('useEffect', mixData)
    let isSubscribed = true;
    API.videoMixBreakdown(mixData.id)
      .then((responseJson) => {
        if (isSubscribed) {
          if (responseJson.error == 0) {
            // SUCCESS
            console.log('responseJson', responseJson.data)
            parseResponse(responseJson.data);
          } else {
            // Unknown Registration error
            console.log('EDIT MIX: UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('EDIT MIX: ERROR CATCH:', error);
      });
    return () => isSubscribed = false;
  },[]);

  useEffect(() => {
    console.log('CHECKING PUBLISH');
    checkCanPublish();
  },[title, description, tagIDs, clips]);

  useEffect(() => {
    if (route.params?.clip) {
      let newClip = route.params?.clip;
      if (clips.indexOf(newClip) == -1) {
        let _clips = clips;
        _clips.push(newClip);
  
        //update length
        let newDuration = newClip.duration == null ? 0 : parseInt(duration) + parseInt(newClip.duration);
        setDuration(newDuration);
        setLengthString(getDuration(parseInt(newDuration)));
        //console.log(_clips)
      }
    }
    if (route.params?.sortedClips) {
      //console.log('setting clips', route.params?.sortedClips)
      setClips(route.params?.sortedClips);
    }
  },[route.params?.clip, route.params?.sortedClips]);

  const parseResponse = (response_data) => {
    // PARSE TAGS:
    var tags_data = response_data.tags;
    if (tags_data.length > 0) {
      var tagsArray = [];
      var tagIDsArray = [];
      for (var i = 0; i < tags_data.length; i++) {
        tagsArray.push(tags_data[i].name);
        tagIDsArray.push(tags_data[i].id);
      }
      setTags(tagsArray);
      setTagIDs(tagIDsArray);
    }
    if (response_data.videoClips.length > 0) {
      // Handle whole array
      var thumbID = response_data.videoClips.length > 1 ? 1 : 0;
      setThumbID(thumbID);
      setClips(response_data.videoClips);
      setClipsOrder(Object.keys(response_data.videoClips));
    }
  }; 

  const getDuration = (duration) => {
    if (duration == null || duration == undefined) {
      return (
        '0:00'
      )
    } else {
      var dur = TimeFormat.fromS(parseInt(duration));
      return (
        dur
      )
    }
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };


  const checkCanPublish = () => {
    if(title !== '' && description !== '' && tags !== [] && clips !== []) {
      setTimeout(setToPublish, 1000);
    }
  };

  const setToPublish = () => {
    if (loading)
      setLoading(false);
  };

  const onPublishMixPress = () => {
    console.log(clips)
    //create array of clip IDs
    var clipIDs = [];
    for (var i = 0; i < clips.length; i++) {
      clipIDs.push(clips[i].id);
    }
    
    API.videoMixUpdate(title, description, tagIDs, clipIDs, clips[thumbID].thumb, duration, id, isPlus, responseHandler)
  };

  const responseHandler = (response) => {

    console.log(response);

    if (response.indexOf(': 1,') > -1) {
      var alertMessage = response.msg;
      Alert.alert(
        'Upload error',
        alertMessage,
        [
          {text: 'OK', onPress: () => navigation.goBack()},
        ]
      )
    } else {
      navigation.navigate('MixingDesk', {refresh: true})
    }
  };

  return (
    <View style={styles.mainContainerUnderHeader}>
      <ScrollView style={styles.paddedContainer}>

          <View style={[styles.textInput, {marginBottom:0}]}>
            <TextInput
              style={styles.inputFieldFW}
              placeholder={'Enter a title'}
              placeholderTextColor={Colors.defaultInputText}
              value={title}
              onChangeText={(text) => setTitle(text)}
              underlineColorAndroid='transparent'
            />
          </View>

        <TouchableOpacity 
          style={styles.newMixElement} 
          onPress={() => navigation.navigate('MixDescription', {description: description})}
        >
          <View style={styles.listElementLeft}>
            <Text numberOfLines={1} style={styles.mediumText}>{description}</Text>
          </View>
          <View style={styles.listElementRight}>
            <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
          </View>
        </TouchableOpacity>

        <TouchableOpacity 
          style={styles.newMixElement} 
          onPress={() => navigation.navigate('MixTags', {tags:tags})}
        >
          <View style={styles.listElementLeft}>
            <View style={styles.tagContainer}>

              { tags ?
                  tags.map(function(tag){
                    //console.log(tag.name);
                    return (
                      <View style={styles.tag} key={tag}>
                        <Text style={styles.smallText}>{tag}</Text>
                      </View>
                    )
                  })
                :
                <Text style={[styles.mediumText, {paddingTop:6}]}>Choose tags</Text>
                }                

            </View>
          </View>
          <View style={styles.listElementRight}>
            <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
          </View>
        </TouchableOpacity>

        <View style={styles.newMixElement}>
          <View style={styles.listElementLeft}>
            <Text style={styles.mediumText}>Length</Text>
          </View>
          <View style={styles.listElementRight}>
            <Text style={styles.mediumText}>{getDuration(duration)}</Text>
          </View>
        </View>

        <View style={{height:10}} />

        {clips &&
          clips.map((clip, index) => {
            return (
              <TouchableOpacity
                key={index.toString()}
                //onLongPress={() => navigation.navigate('MixOrderClips', {data: clips, order: clipsOrder})}
                underlayColor={Colors.underlayColor}>
                <View style={styles.clipsContainer}>
                  <Image
                    source={getThumb(clip.thumb)}
                    style={styles.clipThumb}
                  />
                  <View>
                    <Text style={styles.clipTitle}>{clip.title}</Text>
                  </View>
                </View>
              </TouchableOpacity>              
            )
          })
        }

        <TouchableOpacity 
          style={styles.newMixElement} 
          onPress={() => navigation.navigate('MixOrderClips', {data: clips, order: clipsOrder, returnView: 'EditMix'})}
        >
          <View style={styles.listElementLeft}>
            <Text style={styles.mediumText}>Order video clips</Text>
          </View>
          <View style={styles.listElementRight}>
            <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
          </View>
        </TouchableOpacity>        

        <TouchableOpacity 
          style={styles.newMixElement} 
          onPress={() => navigation.navigate('NewMixChooseVid', {returnView:'EditMix'})}
        >
          <View style={styles.listElementLeft}>
            <Text style={styles.mediumText}>Add a video clip</Text>
          </View>
          <View style={styles.listElementRight}>
            <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
          </View>
        </TouchableOpacity>

        {isPremiumUser &&
          <View style={[styles.newMixElement, {marginBottom: 16}]}>
            <View style={styles.listElementLeft}>
              <Text style={styles.mediumText}>Plus Mix</Text>
            </View>
            <View style={styles.listElementRight}>
              <Switch
                value={isPlus}
                onValueChange={(value) => setIsPlus(value)}/>
            </View>
          </View>
        }

        <View style={{height:10}} />

        <Button
          text={'SAVE CHANGES'}
          onPress={onPublishMixPress}
          opacity={loading ? 0.3 : 1}
          disabled={loading ? true : false}
        />

        <TouchableOpacity 
          style={{padding:20, alignSelf: 'stretch', alignItems: 'center'}}
          onPress={() => deleteMix()}>
          <Text style={[styles.defaultText, {textAlign: 'center', color: Colors.greyText}]}>Delete mix</Text>
        </TouchableOpacity>

        <View style={{height:40, alignSelf: 'stretch'}}></View>

      </ScrollView>
    </View>
  )

  function deleteMix() {
    API.videoMixDelete(id)
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // SUCCESS
          navigation.navigate('MixingDesk', {refreshView:true});
        } else {
          console.log('DELETE MIX: UNKNOWN ERROR:', responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('DELETE MIX: ERROR CATCH:', error);
      });     
  }

};

export default EditMix;

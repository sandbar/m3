import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
  Platform,
  ScrollView
} from 'react-native';

import RNFetchBlob from 'rn-fetch-blob';
import Video from 'react-native-video';

import Colors from '../../common/colors';
import Button from '../../common/button';
import styles from '../../common/styles';

//const VIDEO_URL = 'http://stream.maxmyminutes.com/live/test-compressed.mov';

const NewMixAddClip = ({ navigation, route }) => {

  const { videoData } = route.params;

  const [volume, setVolume] = useState(1);
  const [muted, setMuted] = useState(true);
  const [paused, setPaused] = useState(false);
  const [resizeMode, setResizeMode] = useState('contain');
  const [duration, setDuration] = useState(0.0);
  const [currentTime, setCurrentTime] = useState(0.0);
  const [loading, setLoading] = useState(false);
  const [title, setTitle] = useState('');
  const [categoryID, setCategoryID] = useState(0);
  const [categoryName, setCategoryName] = useState('');
  const [checkSum, setCheckSum] = useState(null);
  const [fileSize, setFileSize] = useState(0);
  const [rate, setRate] = useState(null);

  // var flexCompleted = this.getCurrentTimePercentage() * 100;
  // var flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;

  // componentWillReceiveProps: function(nextProps) {
  //   //console.log("nextProps =", nextProps);
  //   // Waiting for REFRESH with extra params in here
  //   if(nextProps.filter) {
  //     //console.log(nextProps.filter);
  //     this.setState({
  //       categoryID: nextProps.filter.id,
  //       categoryName: nextProps.filter.name
  //     });
  //   }
  //   if(nextProps.checkSum) {
  //     this.setState({checkSum:nextProps.checkSum});
  //   }
  // },  


  useEffect(() => {
    let isSubscribed = true;
    let path = (Platform.OS === 'ios') ? videoData.uri.slice(7) : videoData.uri;
    // get file size
    RNFetchBlob.fs.stat(path)
      .then((stat) => {
        if (isSubscribed) {
          setFileSize(stat.size);
          console.log('stat', stat)
        }
      })
      .catch((err) => { console.log(err) });
    return () => isSubscribed = false;
  }, []);

  useEffect(() => {
    if (route.params?.filter) {
      setCategoryID(route.params.filter.id);
      setCategoryName(route.params.filter.name);
      console.log(categoryID)
    }
  }, [route.params?.filter]);


  /** VIDEO HANDLING **/

  const onLoad = (data) => {

    // check if duration is less than 5 seconds or more than 20 minutes
    if (data.duration <= 5) {
      Alert.alert(
        'Error',
        'Please choose a video longer than 5 seconds',
        [
          { text: 'OK', onPress: () => navigation.goBack() },
        ]
      )
    } else if (data.duration > 1200) {
      Alert.alert(
        'Error',
        'Please choose a video shorter than 20 minutes',
        [
          { text: 'OK', onPress: () => navigation.goBack() },
        ]
      )
    } else {
      setDuration(data.duration);
    }
  };

  const onProgress = (data) => {
    //console.log(data)
    setCurrentTime(data.currentTime);
  };

  // const getCurrentTimePercentage = () => {
  //   if (currentTime > 0) {
  //     return parseFloat(currentTime) / parseFloat(duration);
  //   } else {
  //     return 0;
  //   }
  // };

  // const renderRateControl = (rate) => {
  //   var isSelected = (rate ? true : false);
  //   return (
  //     <TouchableOpacity onPress={() => { setRate(rate) }}>
  //       <Text style={[styles.videoControlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
  //         {rate}x
  //       </Text>
  //     </TouchableOpacity>
  //   )
  // };

  // const renderResizeModeControl = (resizeMode) => {
  //   var isSelected = (resizeMode ? true : false);
  //   return (
  //     <TouchableOpacity onPress={() => { setResizeMode(resizeMode) }}>
  //       <Text style={[styles.videoControlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
  //         {resizeMode}
  //       </Text>
  //     </TouchableOpacity>
  //   )
  // };

  // const renderVolumeControl = (volume) => {
  //   var isSelected = (volume ? true : false);
  //   return (
  //     <TouchableOpacity onPress={() => { setVolume(volume) }}>
  //       <Text style={[styles.videoControlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
  //         {volume * 100}%
  //       </Text>
  //     </TouchableOpacity>
  //   )
  // };

  const isButtonAvailable = () => {
    if (title.trim() != '' && categoryID != 0 && loading == false) {
      return true;
    }
    return false;
  };

  const onAddVideoPress = () => {
    setPaused(true);
    navigation.navigate('NewMixUploading', {
      videoData: videoData,
      videoTitle: title,
      category: categoryID,
      fileSize: fileSize
    });
  };

  return (
    <ScrollView style={styles.mainContainerUnderHeader}>
      <TouchableOpacity style={styles.videoPlayer} onPress={() => { setPaused(!paused) }}>
        <Video source={{ uri: videoData.uri }}
          style={styles.videoPlayer}
          rate={rate}
          paused={paused}
          volume={volume}
          muted={muted}
          resizeMode={resizeMode}
          onLoad={onLoad}
          onProgress={onProgress}
          //onEnd={() => { alert('Done!') }}
          repeat={true}
        />
      </TouchableOpacity>

      {/*<View style={styles.videoControls}>
        <Text style={styles.videoTime}>{TimeFormat.fromS(Math.floor(parseFloat(this.state.currentTime)))}</Text>
        <View style={styles.videoProgress}>
          <View style={[styles.innerProgressCompleted, {flex: flexCompleted}]} />
          <View style={[styles.innerProgressRemaining, {flex: flexRemaining}]} />
        </View>
        <Text style={styles.videoTime}>{TimeFormat.fromS(Math.floor(parseFloat(this.state.duration)))}</Text>
        }<TouchableOpacity>
          <Image source={require('../../../img/btn-fs.png')} />
        </TouchableOpacity>
      </View>*/}

      <View style={styles.paddedContainer}>

        <View style={styles.textInput}>
          <TextInput
            style={styles.inputFieldFW}
            placeholder={'Enter a title'}
            placeholderTextColor={Colors.defaultInputText}
            value={title}
            onChangeText={(text) => setTitle(text)}
            underlineColorAndroid='transparent'
          />
        </View>

        <TouchableOpacity
          style={[styles.newMixElement, { marginBottom: 16 }]}
          onPress={() => { navigation.navigate('Categories', { filterID: categoryID, returnView: 'NewMixAddClip' }) }}
        >
          <View style={styles.listElementLeft}>
            <Text style={styles.mediumText}>{categoryID == 0 ? 'Choose a category' : categoryName}</Text>
          </View>
          <View style={styles.listElementRight}>
            <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
          </View>
        </TouchableOpacity>

        <Button
          text={'ADD VIDEO'}
          onPress={onAddVideoPress}
          opacity={isButtonAvailable() ? 1 : 0.3}
          disabled={isButtonAvailable() ? false : true}
        />

        <View style={{ height: 300, width: 100, backgroundColor: 'white' }} />

      </View>

    </ScrollView>
  );
}

export default NewMixAddClip;

import React, { useEffect, useState, useCallback, useLayoutEffect } from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

import { useFocusEffect } from '@react-navigation/native';
import { SwipeListView } from 'react-native-swipe-list-view';
import ImagePicker from 'react-native-image-picker';

import styles from '../../common/styles';
import API from '../../communication/api';

const NewMixChooseVid = ({ navigation, route }) => {

  const { returnView } = route.params;
  const [clipsData, setClipsData] = useState([]);
  const [filterText, setFilterText] = useState('Filter');
  const [filterID, setFilterID] = useState(-1);
  const [refreshingView, setRefreshView] = useState(false);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          style={{ marginRight: 20 }}
          hitSlop={{ top: 10, right: 10, bottom: 10, left: 10 }}
          onPress={() => selectVideo()} >
          <Image source={require('../../../img/btn-camera.png')} />
        </TouchableOpacity>
      )
    });
  },[navigation]);  

  useEffect(() => {
    let isSubscribed = true;
    API.videoClips()
      .then((responseJson) => {
        if (isSubscribed) {
          if (responseJson.error == 0) {
            // SUCCESS
            //console.log(responseJson.data);
            if (filterID != -1) {
              filterVideo(filterID);
            } else {
              setClipsData(responseJson.data);
            }
            setRefreshView(false);
          } else {
            // Unknown Registration error
            console.log('VIDEO CLIPS: UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('VIDEO CLIPS: ERROR CATCH:', error);
      });

    return () => isSubscribed = false;
  }, [refreshingView]);

  useFocusEffect(
    useCallback(() => {
    if (route.params?.refreshView) {
      let isSubscribed = true;
      // seems like the video does not always get added to the list immediately
      setTimeout(() => {
        API.videoClips()
          .then((responseJson) => {
            if (isSubscribed) {
              if (responseJson.error == 0) {
                // SUCCESS
                // console.log(responseJson);
                if (filterID != -1) {
                  filterVideo(filterID);
                } else {
                  setClipsData(responseJson.data);
                }
                setRefreshView(false);
              } else {
                // Unknown Registration error
                console.log('VIDEO CLIPS: UNKNOWN ERROR:', responseJson);
              }
            }
          })
          .catch((error) => {
            // Request sending/parsing error
            console.log('VIDEO CLIPS: ERROR CATCH:', error);
          });
      }, 2000)
    }

    if (route.params?.filter) {
      let filterChosen = route.params?.filter;
      if (filterChosen.id == -1) {
        setFilterText('Filter');
        setFilterID(-1);
        setRefreshView(true);
      } else {
        setFilterText(filterChosen.name);
        setFilterID(filterChosen.id);
        filterVideo(filterChosen.id);
      }
    }

      return () => {
        isSubscribed = false;
      };
    }, [route.params?.refreshView, route.params?.filter])
  );

  const pressClipsRow = (clipData) => {
    if (clipData.status == 3) {
      navigation.navigate(returnView, { clip: clipData });
    }
  };

  const getStatus = (statusID) => {
    if (statusID == 1) {
      // Uploaded - checksum not checked yet
      return 'CHECKING UPLOAD'
    } else if (statusID == 2) {
      // checksum ok and encoded started
      return 'ENCODING'
    } else if (statusID == 3) {
      // Success
      return ''
    } else if (statusID == 4) {
      // Encoding failed
      return 'ENCODING ERROR'
    }
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        { uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb }
      )
    }
  };

  const onFilterPress = () => {
    navigation.navigate('Categories', { filterID: filterID, returnView: 'NewMixChooseVid' });
  };

  const filterVideo = (filterID) => {
    let tmpFilteredClips = [];
    for (var i = 0; i < clipsData.length; i++) {
      if (filterID === clipsData[i].category.id) {
        tmpFilteredClips.push(clipsData[i]);
      }
    }
    setClipsData(tmpFilteredClips)
  };

  const selectVideo = () => {
    const options = {
      title: '',
      takePhotoButtonTitle: 'Take Video...',
      mediaType: 'video',
      videoQuality: 'high',
      allowsEditing: false
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response => ', response);

      if (response.didCancel) {
        console.log('User cancelled video picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // SUCCESS
        navigation.navigate('NewMixAddClip', { videoData: response });
      }
    });
  };

  const deleteClip = (clipID) => {
    API.videoClipDelete(clipID)
      .then((responseJson) => {
        // console.log(responseJson)
        if (responseJson.error == 0) {
          // SUCCESS
          setRefreshView(true);
        } else {
          console.log('DELETE CLIP: UNKNOWN ERROR:', responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('DELETE CLIP: ERROR CATCH:', error);
      });
  };

  return (
    <View style={styles.mainContainerUnderHeader}>
      <View style={styles.paddedContainer}>

        <TouchableOpacity style={styles.newMixElement} onPress={() => onFilterPress()}>
          <View style={styles.listElementLeft}>
            <Text style={styles.mediumText}>{filterText}</Text>
          </View>
          <View style={styles.listElementRight}>
            <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
          </View>
        </TouchableOpacity>

        {clipsData.length > 0 ?
          <SwipeListView
            useFlatList={true}
            data={clipsData}
            contentContainerStyle={[styles.clipsList, { marginTop: 10 }]}
            keyExtractor={(item, index) => index.toString()}
            onRefresh={() => setRefreshView(true)}
            refreshing={refreshingView}
            renderItem={(rowData, rowID) => (
              <TouchableOpacity onPress={() => pressClipsRow(rowData.item)} >
                <View style={styles.clipsContainer}>
                  <ImageBackground
                    source={getThumb(rowData.item.thumb)}
                    style={[styles.clipThumb]}
                  >
                    <View style={[styles.encoding, { opacity: rowData.item.status == 3 ? 0 : 0.9 }]}>
                      <Text style={[styles.smallText, { color: '#FFF' }]}>
                        {getStatus(rowData.item.status)}
                      </Text>
                    </View>
                  </ImageBackground>
                  <View style={styles.clipDetail}>
                    <Text style={styles.clipTitle} numberOfLines={1}>{rowData.item.title}</Text>
                    <Text style={styles.defaultText} numberOfLines={2}>{rowData.item.category.name}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            disableRightSwipe
            rightOpenValue={-75}
            renderHiddenItem={
              (rowData, rowID) => (
                <View style={styles.clipHiddenRow}>
                  <TouchableOpacity
                    onPress={() => deleteClip(rowData.item.id)}
                    style={styles.clipDelete}
                  >
                    <Text style={[styles.defaultText, { color: '#FFF' }]}>DELETE</Text>
                  </TouchableOpacity>
                </View>
              )
            }
          />
          :
          <View style={[styles.loadingContainer, { marginTop: 20 }]}>
            <Text style={styles.smallText}>{filterID === -1 ? 'You dont have any video clips yet, try uploading one!' : 'You dont have any video clips in this category'}</Text>
          </View>
        }

      </View>
    </View>
  );

};

export default NewMixChooseVid;

import React, {useState} from 'react';
import {
  View, TouchableOpacity, Image, Text, SafeAreaView
} from 'react-native';

import DraggableFlatList from 'react-native-draggable-flatlist';

import Button from '../../common/button';
import Colors from '../../common/colors';
import styles from '../../common/styles';

const MixOrderClips = ({navigation, route}) => {

  const {data, order, returnView} = route.params;
  const [sortedClips, setSortedClips] = useState(data);

  const onSavePress = () => {
    // var result = [];
    // for (var i = 0; i < data.length; i++) {
    //   result[i] = data[order[i]];
    // }
    //TODO check this
    navigation.navigate(returnView, {sortedClips: sortedClips});
  }

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };   

  return (
    <SafeAreaView style={styles.mainContainerUnderHeader}>
      <View style={[styles.paddedContainer, {paddingTop:0}]}>
        <DraggableFlatList
          style={[styles.clipsList, {marginTop: 16, marginBottom: 16, flex:1}]}
          data={sortedClips}
          keyExtractor={(item, index) => index.toString()}
          //order={order}
          //disableAnimatedScrolling={true}
          // onRowMoved={e => {
          //   order.splice(e.to, 0, order.splice(e.from, 1)[0]);
          //   //forceUpdate()
          // }}
          onDragEnd={({data}) => setSortedClips(data)}
          sortRowStyle={styles.sortRowStyle}
          renderItem={({item, index, drag, isActive}) => {
            return (
              <TouchableOpacity
                underlayColor={Colors.underlayColor}
                //delayLongPress={10}
                onLongPress={drag}
                >
                <View style={styles.clipsContainer}>
                  <Image
                    source={getThumb(item.thumb)}
                    style={styles.clipThumb}
                  />
                  <View>
                    <Text style={styles.clipTitle} numberOfLines={1}>{item.title}</Text>
                    <Text style={styles.defaultText} numberOfLines={2}>{item.category.name}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )
          }}
        />

        <Button
          text={'SAVE'}
          onPress={onSavePress}
        />

      </View>
    </SafeAreaView>
  );

};

export default MixOrderClips;

import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';

import styles from '../../common/styles';
import Colors from '../../common/colors';

const ClipsRowComponent = (props) => {

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };  
  
  // console.log(this.props.data)
  return (
    <TouchableOpacity
      underlayColor={Colors.underlayColor}
      delayLongPress={10}
      {...props.sortHandlers}>
      <View style={styles.clipsContainer}>
        <Image
          source={getThumb(props.data.thumb)}
          style={styles.clipThumb}
        />
        <View>
          <Text style={styles.clipTitle} numberOfLines={1}>{props.data.title}</Text>
          <Text style={styles.defaultText} numberOfLines={2}>{props.data.category.name}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );

};

export default ClipsRowComponent;

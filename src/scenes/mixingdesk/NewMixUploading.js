import React, { useState, useEffect } from 'react';
import {
  Alert,
  Text,
  View,
  Platform,
} from 'react-native';

var RNFS = require('react-native-fs');

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';

const NewMixUploading = ({ navigation, route }) => {

  const { videoData, videoTitle, category } = route.params;
  const [creatingHash, setCreatingHash] = useState(true);
  const [loadingBarWidth, setLoadingBarWidth] = useState(0);
  const [percentLoaded, setPercentLoaded] = useState(0);
  const [percentIcon, setPercentIcon] = useState('%');

  useEffect(() => {
    let isSubscribed = true;
    isSubscribed && createHash(videoData);
    return () => isSubscribed = false;
  }, []);

  const createHash = (videoData) => {
    //CREATE HASH
    const videoPath = getFileLocalPath(videoData);
    RNFS.hash(videoPath, 'sha1')
      .then((hashedResponse) => {
        setCreatingHash(false);
        uploadVideoToServer(videoData, videoTitle, category, hashedResponse);
      })
      .catch((e) => console.log('Error creating hash', e))
  };

  const getFileLocalPath = ({ path, uri }) => {
    // return Platform.OS === 'android' ? path : uri;
    // return Platform.OS === 'android' ? path : uri.slice(7);
    return Platform.OS === 'android' ? path : uri.replace('file://', '');
  };

  const uploadVideoToServer = (videoData, title, category, checkSum) => {
    const fileName = videoData.uri.replace(/^.*[\\\/]/, '');
    const contentType = Platform.OS === 'android' ? 'video/mp4' : 'video/quicktime';
    const videoPath = getFileLocalPath(videoData);
      API.videoClipCreate(videoPath, title, fileName, contentType, category, checkSum, progressHandler, responseHandler);
  };

  const progressHandler = (progress) => {
    let percent = Math.floor(progress * 100);
    setLoadingBarWidth(progress);
    setPercentLoaded(percent);
    if (progress == 1) {
      console.log("SUCCESS!!")
      setPercentLoaded('PROCESSING');
      setPercentIcon('');
    }
  };

  const responseHandler = (response) => {
    if (response.indexOf(': 1,') > -1) {
      let alertMessage = response.msg;
      Alert.alert(
        'Upload error',
        alertMessage,
        [
          {
            text: 'OK', onPress: () => {
              navigation.navigate('NewMixChooseVid');
            }
          },
        ]
      )
    } else {
        navigation.navigate('NewMixChooseVid', { refreshView: true });
      }
  };


  return (
    <View style={[styles.containerGeneric, { backgroundColor: '#fff' }]}>
      <View style={[styles.paddedContainer, { justifyContent: 'center' }]}>

        {creatingHash ?
          <View style={[styles.mainContainer, { justifyContent: 'center', alignItems: 'center' }]}>
            <Text style={[styles.loadingBarText, { marginBottom: 10, color: Colors.brandGreen }]}>PROCESSING...</Text>
          </View>
          :
          <View style={[styles.mainContainer, { justifyContent: 'center', alignItems: 'center' }]}>
            <Text style={[styles.loadingBarText, { marginBottom: 10, color: Colors.brandGreen }]}>UPLOADING...</Text>
            <View style={styles.loadingBar}>
              <View style={[styles.loadingBarTop, { flex: loadingBarWidth }]}>
                <Text style={styles.loadingBarText} numberOfLines={1}>{percentLoaded}{percentIcon}</Text>
              </View>
              <View style={[styles.loadingBarBot, { flex: 1 - loadingBarWidth }]} />
            </View>
          </View>
        }
      </View>
    </View>
  );

};

export default NewMixUploading;

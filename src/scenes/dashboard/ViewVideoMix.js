import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  FlatList,
  Animated,
  ScrollView,
  Alert,
  Platform,
  ActivityIndicator
} from 'react-native';

var TimeFormat = require('hh-mm-ss');

import VidPlayer from './components/VideoPlayer.js';
import VidPlayeriOS from './components/VideoPlayeriOS.js';
import Breakdown from './components/Breakdown.js';
import Rating from './components/Rating.js';
import MixesStorage from '../../sync/MixesStorage';

import Colors from '../../common/colors';
import styles from '../../common/styles';
import API from '../../communication/api';
import UserProxy from '../../communication/UserProxy';

const MIN_TITLE_HEIGHT = 40;
const VIDEO_URL = 'http://stream.maxmyminutes.com/live/31160df2-493b-4263-a87e-0c7308f3de9f.mp4' //- it's client file

const ViewVideoMix = ({navigation, route}) => {

  // static navigationOptions = ({ navigation }) => {
  //   if (navigation.state.params && !navigation.state.params.fullscreen) {
  //     //Hide Header by returning null
  //     return { header: null };
  //   }
  // }  

  const {syncedData, mixID} = route.params;

  const [isOffline, setIsOffline] = useState(false);
  const [loading, setLoading] = useState(true);
  //const [mixID, setMixID] = useState(null);
  const [mixTitle, setMixTitle] = useState('');
  const [mixAuthor, setMixAuthor] = useState(null);
  const [mixAuthorID, setMixAuthorID] = useState(null);
  const [mixAuthorImage, setMixAuthorImage] = useState(null);
  const [mixDescription, setMixDescription] = useState('');
  const [mixDetails, setMixDetails] = useState(null);
  const [mixBreakdown, setMixBreakdown] = useState(null);
  const [mixViews, setMixViews] = useState('');
  const [isFavourite, setIsFavourite] = useState(false);
  const [isFollowing, setIsFollowing] = useState(false);
  const [isSynced, setIsSynced] = useState(false);
  const [isPremiumMix, setIsPremiumMix] = useState(false);
  const [mixClips, setMixClips] = useState([]);
  const [mixRatings, setMixRatings] = useState([]);
  const [mixThumb, setMixThumb] = useState('');
  const [videoSource, setVideoSource] = useState(null);
  const [mixDuration, setMixDuration] = useState('');
  const [tagsExpanded, setTagsExpanded] = useState(false);
  const [mixRelatedMixes, setMixRelatedMixes] = useState([]);
  const [mixTags, setMixTags] = useState([]);
  const [onMixEnd, setOnMixEnd] = useState(false);
  const [videoDetailsStyle, setVideoDetailsStyle] = useState(styles.videoDetailsContainer);
  const [mainContainerStyle, setMainContainerStyle] = useState(null);
  const [fullscreen, setFullscreen] = useState(false)
  const [vidClipCount, setVidClipCount] = useState(0);
  
  //isDataLoaded: false,
  //duration: 0.0,
  let slidingPanelTagsAnim = new Animated.Value(0);
  let slidingPanelTagsHeight = 0;
  let goSeek = 0;

  // componentWillReceiveProps: function(nextProps) {
  //   //console.log("mixID", nextProps.mixID)
  //   //console.log("goFS", nextProps.goFS)
  //   if (nextProps.mixID) {
  //     this.props.mixID = nextProps.mixID;
  //     this.setState({
  //       isDataLoaded:false,
  //       mixRatings: [],
  //       mixClips: [],
  //       mixRelatedMixes: [],
  //       mixTags: [],
  //       mixDetails: null,
  //       mixBreakdown: null,
  //       duration: 0.0,
  //       vidClipCount: 0,
  //       isFavourite: false,
  //       isFollowing: false,
  //       mixTitle: '',
  //       mixViews: '',
  //       mixAuthor: '',
  //     });
  //     this.setTimeout(this.sendRequests, 100);
  //   }
  //   if (nextProps.goFS == true) {
  //     //console.log("SET TO FS LANDSCAPE")
  //     this.setState({
  //       fsState:true,
  //       goSeek: nextProps.goSeek
  //     });
  //   } else if (nextProps.goFS == false) {
  //     //console.log("SET TO NON-FS PORTRAIT")
  //     this.setState({
  //       fsState: false,
  //       goSeek: nextProps.goSeek
  //     });
  //     this.handleVideoFS(false);
  //   }
  // },

  useEffect(() => {
    let isSubscribed = true;
    let detailsData;
    let breakdownData;
    if (syncedData) {
      if (isSubscribed) {
        console.log('USE SYNCED DATA', syncedData);
        setIsOffline(true);
        setMixDetails(syncedData);
        setMixBreakdown(syncedData);
        prepareData(syncedData, syncedData);
      }
    } else {
      API.videoMixDetails(mixID)
      .then((responseJsonDetails) => {
        if (isSubscribed) {
          if (responseJsonDetails.error == 0) {
            // SUCCESS
            console.log("VIDEO MIX DETAILS:", responseJsonDetails.data);
            detailsData = responseJsonDetails.data;
            setMixDetails(detailsData);

            API.videoMixBreakdown(mixID)
            .then((responseJsonBreakdown) => {
              if (isSubscribed) {
                if (responseJsonBreakdown.error == 0) {
                  //SUCCESS
                  console.log("VIDEO MIX BREAKDOWN:", responseJsonBreakdown.data);
                  breakdownData = responseJsonBreakdown.data;
                  setMixBreakdown(breakdownData);
                  prepareData(detailsData, breakdownData)
                } else {
                  // Unknown mix breakdown error
                  console.log('VIDEO MIX BREAKDOWN: UNKNOWN ERROR:', responseJsonBreakdown);
                }            
              }
            })
            .catch((error) => {
              // Request sending/parsing error
              console.log('VIDEO MIX BREAKDOWN: ERROR CATCH:', error);
            });        

          } else {
            // Unknown mix details error
            console.log('VIDEO MIX DETAILS: UNKNOWN ERROR:', responseJsonDetails);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('VIDEO MIX DETAILS: ERROR CATCH:', error);
      });
    }
    return () => isSubscribed = false;
  },[route.params?.refreshView]);

  // const checkIfAllLoaded = () => {
  //   if (mixDetails && mixBreakdown) {
  //     // ALL LOADED
  //     console.log('loaded')
  //     prepareData(mixDetails, mixBreakdown);
  //   }
  // };

  const prepareData = (detailsData, breakdownData) => {

    console.log('mixDetails', detailsData)

    let tagsData = detailsData.tags ? detailsData.tags : [];
    //console.log('tagsData.length: ' + tagsData.length);
    if (tagsData.length > 0) {
      var tagsArray = [];
      var tagIDsArray = [];
      for (var i = 0; i < tagsData.length; i++) {
        tagsArray.push(tagsData[i].name);
        tagIDsArray.push(tagsData[i].id);
      }
      setMixTags(tagsArray);

      // get related video data based on the first tag ID
      API.search(tagIDsArray, 9999)
      //API.videoMixRelatedMixes(tagsData[0].id)
        .then((responseJsonRelatedMixes) => {
          if(responseJsonRelatedMixes.error == 0) {
            // check for same ID as current vid
            for (var i = 0; i < responseJsonRelatedMixes.data.length; i++) {
              if (responseJsonRelatedMixes.data[i].id == detailsData.id) {
                responseJsonRelatedMixes.data.splice(i,1);
              }
            }
            setMixRelatedMixes(responseJsonRelatedMixes.data);
          } else {
            console.log('RELATED MIXES: UNKNOWN ERROR:', responseJsonRelatedMixes);
          }
        })
        .catch((error) => {
          console.log('RELATED MIXES: ERROR CATCH:', error);
        });
    }

    API.favourites()
      .then((responseJsonFavourites) => {
        if (responseJsonFavourites.error == 0) {
          // check favourites list against mix ID
          for (var i = 0; i < responseJsonFavourites.data.length; i++) {
            responseJsonFavourites.data[i].id == detailsData.id ? setIsFavourite(true) : setIsFavourite(false);
          }
        } else {
          console.log('FAVOURITES: UNKNOWN ERROR:', responseJsonFavourites);
        }
      }
    )
    .catch((error) => {
      console.log('FAVOURITES: ERROR CATCH:', error);
    });

    // following
    API.followings(UserProxy.getId())
      .then((responseJsonFollowings) => {
        if(responseJsonFollowings.error == 0) {
          // check followings list against author ID
          for (var i = 0; i < responseJsonFollowings.data.length; i++) {
            if (responseJsonFollowings.data[i].id == detailsData.author.id) {
              setIsFollowing(true);
            }
          }
        } else {
          console.log('FOLLOWING: UNKNOWN ERROR:', responseJsonFollowings);
        }
      }
    )
    .catch((error) => {
      console.log('FOLLOWING: ERROR CATCH:', error);
    });

    // check is synced

    MixesStorage.checkIsSynced(mixID)
      .then((isSynced) => {
        setIsSynced(isSynced);
      })
      .catch((error) => {
        console.log("SOMETHING WENT WRONG", error)
      });

    let firstClip = breakdownData.videoClips.length > 0 ? "http://stream.maxmyminutes.com/live/" + breakdownData.videoClips[vidClipCount].fileUrl : VIDEO_URL;
    if (isOffline == true) {
      firstClip = breakdownData.videoClips[vidClipCount].fileUrl;
    }
    console.log("now playing " + firstClip);
    //console.log(breakdownData.videoClips)

    // calculate duration - its not being sent in request
    let mixDuration = 0;
    for (var iii = 0; iii < breakdownData.videoClips.length; iii++) {
      mixDuration += parseInt(breakdownData.videoClips[iii].duration);
    }

    setMixTitle(detailsData.title);
    setMixViews(detailsData.views);
    setMixAuthor(detailsData.author.username);
    setMixAuthorID(detailsData.author.id);
    setMixAuthorImage(detailsData.author.image);
    setMixDescription(detailsData.description);
    setIsPremiumMix(detailsData.isPremium);
    setMixClips(breakdownData.videoClips);
    //mixClipsPlaylist: breakdownData.videoClips,
    setMixRatings(detailsData.Ratings);
    setMixThumb(detailsData.thumb);
    setMixDuration(mixDuration);
    setVideoSource(firstClip);

    slidingPanelTagsAnim.setValue(MIN_TITLE_HEIGHT)//,
    setTimeout(setTagsHeight, 500);

    setLoading(false);
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  // const updatePlaylist = (clipID) => {
  //   //console.log("clipID", clipID);
  //   for (var i = 0; i < this.state.mixClipsPlaylist.length; i++) {
  //     if (clipID == this.state.mixClipsPlaylist[i].id) {
  //       //console.log("remove", clipID, i);
  //       //this.state.mixClipsPlaylist.splice(i, 1);
  //       /*var opacityState = {};
  //       opacityState[clipID] = 0.5;
  //       this.setState(opacityState);*/
  //       /*this.refs["clip" + clipID].setNativeProps({
  //         opacity:0.5
  //       });*/
  //     }
  //   }
  // },

  const handleOnVideoEnd = () => {
    // update videoSource
    //let clipCount = vidClipCount;
    console.log('checking end of vid:', vidClipCount, mixClips.length - 1);
    let vidSrc;
    if (vidClipCount < mixClips.length - 1) {
      vidSrc = "http://stream.maxmyminutes.com/live/" + mixClips[vidClipCount + 1].fileUrl;
      console.log('setting new vid source', vidSrc)
      setVideoSource(vidSrc);
      setVidClipCount(vidClipCount + 1);
    } else {
      // set flag to pause video, return to start
      vidSrc = "http://stream.maxmyminutes.com/live/" + mixClips[0].fileUrl;
      console.log('resetting vid source', vidSrc)
      setVideoSource(vidSrc);
      setOnMixEnd(true);
      // set play flag to true and send
      //console.log('setting play flag to true');
      //API.sendSecondsWatched(1, mixID, true, mixID, playFlagResponseHandler)
    }
  };

  const playFlagResponseHandler = (response) => {
    var responseJSON = JSON.parse(response);
    //console.log(responseJSON);
    if (responseJSON.error == 1) {
      console.log("error sending play flag");
    } else {
      console.log("play flag sent")
    }
  };

  const handleVideoFS = (fs) => {
    // required to show/hide details view when video goes FS - Android only
    //console.log('parent', fs)
    setFullscreen(fs);
    navigation.setOptions({
      headerShown: !fs
    });    
    if (fs) {
      setVideoDetailsStyle(styles.videoDetailsContainerFS);
      setMainContainerStyle(styles.videoPlayerContainerFS);
    } else {
      setVideoDetailsStyle(styles.videoDetailsContainer);
      setMainContainerStyle(styles.mainContainerUnderHeader);
    }
  };

  /** RELATED VIDEOS LIST **/

  const pressRelatedRow = (mixID) => {
    //to do: updated MixID
    //mixID:mixID,
  };

  /*
  renderRelatedRow(rowData: string, sectionID: number, rowID: number) {
    //console.log(rowData);
    return (

    );
  },
  */

 const getUserAvatar = () => {
    if (mixAuthorImage !== null) {
      return {uri: "http://stream.maxmyminutes.com/users-images/" + mixAuthorImage}
    } else {
      return require('../../../img/avatar-default.png')
    }
  };

  const toggleTagsPanel = () => {
      let initialValue    = tagsExpanded ? slidingPanelTagsHeight + MIN_TITLE_HEIGHT : MIN_TITLE_HEIGHT,
          finalValue      = tagsExpanded ? MIN_TITLE_HEIGHT : slidingPanelTagsHeight + MIN_TITLE_HEIGHT;

      setTagsExpanded(!tagsExpanded);

      slidingPanelTagsAnim.setValue(initialValue);
      Animated.spring(
          slidingPanelTagsAnim,
          {
              toValue: finalValue
          }
      ).start();
  };

  const setTagsHeight = () => {
    // todo
    // this.refs._tagPanel.measure(
    //   (ox, oy, width, height, px, py) => this.setState({ slidingPanelTagsHeight: height })
    // );
  };

  const onPressFollow = () => {
    if (!isFollowing) {
      if (UserProxy.getId() !== mixAuthorID) {
        API.follow(mixAuthorID)
          .then((responseJsonFollow) => {
            if(responseJsonFollow.error == 0) {
              //console.log(responseJsonFollow.data);
              setIsFollowing(true);
              //sendRequests(); needed?
            } else {
              console.log('FOLLOW: UNKNOWN ERROR:', responseJsonFollow);
            }
          }
        )
        .catch((error) => {
          console.log('FOLLOW: ERROR CATCH:', error);
        });
      }
    } else {
      if (UserProxy.getId() !== mixAuthorID) {
        API.unfollow(mixAuthorID)
          .then((responseJsonUnFollow) => {
            if(responseJsonUnFollow.error == 0) {
              //console.log(responseJsonUnFollow.data);
              setIsFollowing(false);
              //this.sendRequests(); needed?
            } else {
              console.log('FOLLOW: UNKNOWN ERROR:', responseJsonUnFollow);
            }
          }
        )
        .catch((error) => {
          console.log('FOLLOW: ERROR CATCH:', error);
        });
      }
    }
  };

  const onPressFavourite = () => {
    if (UserProxy.getId() !== mixAuthorID) {
      if (!isFavourite) {
        API.videoMixFavourite(mixID)
          .then((responseJsonFavourite) => {
            if(responseJsonFavourite.error == 0) {
              console.log("favourite onpress", responseJsonFavourite);
              setIsFavourite(true);
            } else {
              console.log('FAVOURITE: UNKNOWN ERROR:', responseJsonFavourite);
            }
          }
        )
        .catch((error) => {
          console.log('FAVOURITE: ERROR CATCH:', error);
        });
      } else {
        API.videoMixUnFavourite(mixID)
          .then((responseJsonFavourite) => {
            if(responseJsonFavourite.error == 0) {
              //console.log("unfavourite onpress", responseJsonFavourite);
              setIsFavourite(false);
            } else {
              console.log('FAVOURITE: UNKNOWN ERROR:', responseJsonFavourite);
            }
          }
        )
        .catch((error) => {
          console.log('FAVOURITE: ERROR CATCH:', error);
        });
      }
    }
  };

  const onToggleSync = () => {
    if (!isSynced) {
      MixesStorage.storeMixData(mixDetails, mixBreakdown, mixDuration);
    } else {
      MixesStorage.removeMixData(mixBreakdown);
    }
    setIsSynced(!isSynced);
  };

  const onPressReport = () => {
    API.videoMixReport(mixID)
      .then((responseJsonReport) => {
        if(responseJsonReport.error == 0) {
          setIsReported(true);
          Alert.alert(
            'Mix reported',
            'Mix has been reported and will be reviewed.',
            [
              {text: 'OK', onPress: () => null},
            ]
          )
        } else {
          console.log('REPORT: UNKNOWN ERROR:', responseJsonReport);
        }
      }
    )
    .catch((error) => {
      console.log('REPORT: ERROR CATCH:', error);
    });
  };

  if (loading) {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.profileTabContentVideo}>
          <View style={styles.loadingContainer}>
            <ActivityIndicator size='small' color={Colors.brandGreen}/>
            <Image source={require('../../../img/m3logo.png')} />
          </View>
        </View>
      </View>
    )
  } else {
    return (
      <View style={mainContainerStyle}>

        {Platform.OS === 'ios' ?
          <VidPlayeriOS
            videoSource = {videoSource}
            onEndHandler = {handleOnVideoEnd}
            onMixEnd = {onMixEnd}
            mixID = {mixID}
            goSeek = {goSeek}
            isOffline = {isOffline}
            isPremium = {isPremiumMix}
            //isOffline = {true}
          />
          :
          <VidPlayer
            videoSource = {videoSource}
            onEndHandler = {handleOnVideoEnd}
            handleVideoFS = {handleVideoFS}
            onMixEnd = {onMixEnd}
            mixID = {mixID}
            goSeek = {goSeek}
            isOffline = {isOffline}
            isPremium = {isPremiumMix}
            //isOffline = {true}
          />
        }
      <ScrollView style={videoDetailsStyle}>

        <Text style={styles.viewVideoTitle}>{mixTitle}</Text>
        <Text style={styles.smallText}>{mixViews} plays</Text>

        <View style={styles.userContainer}>
          <Image
            source={getUserAvatar()}
            style={styles.followerAvatar}
          />
          <View style={styles.userInfo}>
            <Text>{mixAuthor}</Text>
            <TouchableOpacity style={isFollowing ? styles.followButtonOn : styles.followButton} onPress={() => onPressFollow()}>
              <View style={{flexDirection:'row'}}>
                <Image source={isFollowing ? require('../../../img/icon-follow-selected.png') : require('../../../img/icon-follow.png')} style={{marginRight:6}} />
                <Text style={isFollowing ? styles.followTextOn : styles.followText}>
                  {isFollowing ? 'Tracking' : 'Track' }
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        {!syncedData &&
        <View style={styles.videoActionButtons}>
          <TouchableOpacity onPress={() => onPressFavourite()}>
            <View style={styles.videoActionButton}>
              <Image source={isFavourite ? require('../../../img/btn-favourite-on.png') : require('../../../img/btn-favourite.png') } />
              <Text style={[styles.smallText, {marginLeft:6}]}>Favourite</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => onToggleSync()}>
            <View style={styles.videoActionButton}>
              <Image source={isSynced ? require('../../../img/btn-sync-on.png') : require('../../../img/btn-sync.png')} />
              <Text style={[styles.smallText, {marginLeft:6}]}>Sync</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => onPressReport()}>
            <View style={styles.videoActionButton}>
              <Image source={require('../../../img/btn-report.png')} />
              <Text style={[styles.smallText, {marginLeft:6}]}>Report</Text>
            </View>
          </TouchableOpacity>
        </View>        
        }

        <Text style={styles.videoDescription}>{mixDescription}</Text>


        {/* BREAKDOWN */}
        <Breakdown mixClips={mixClips} />

        {/* RATING */}
        <Rating mixRatings={mixRatings} mixID={mixID} navigation={navigation} />

        <Animated.View style={[styles.slidingPanelContainer, {height:slidingPanelTagsAnim}]}>
          <TouchableOpacity style={styles.panelTitle} onPress={() => toggleTagsPanel()}>
            <Text style={[styles.panelIcon,
              {marginLeft: tagsExpanded ? 1 : 0,
              marginRight: tagsExpanded ? 1 : 0}]}>
                {tagsExpanded ? '-' : '+'}
            </Text>
            <Text style={styles.panelTitleText}>Tags</Text>
          </TouchableOpacity>

          <View 
            style={styles.slidingPanelContent} 
            //ref='_tagPanel'
          >
            <View style={styles.tagContainer}>
              {mixTags.length > 0 && 
                  mixTags.map(function(tag){
                    return (
                      <View style={styles.tag} key={tag}>
                        <Text style={styles.smallText}>{tag}</Text>
                      </View>
                    )
                })}
            </View>
          </View>
        </Animated.View>

        <Text style={[styles.relatedTitleText, {opacity: mixRelatedMixes.length > 0 ? 1 : 0}]}>Related videos</Text>

        {mixRelatedMixes.length > 0 && 
        <View>
          <FlatList
            data={mixRelatedMixes}
            contentContainerStyle = {styles.videoListRelated}
            keyExtractor = {(item, index) => item.id.toString() }
            renderItem={({item, index}) => (
              <TouchableOpacity onPress={() => pressRelatedRow(item.id)} >
                <View style={styles.videoContainerRelated}>
                  <ImageBackground
                    source={getThumb(item.thumb)}
                    style={styles.videoThumbRelated} >
                    <View style={styles.videoLength}>
                      <Text style={styles.videoLengthText}>
                        {item.mixDuration == null ? '0:00' : TimeFormat.fromS(parseInt(item.mixDuration))}
                      </Text>
                    </View>
                  </ImageBackground>
                  <Text style={styles.videoTitle} numberOfLines={1}>{item.title}</Text>
                  <Text style={styles.videoInfo}>{item.username}</Text>
                  <Text style={styles.videoInfo}>{item.views} plays</Text>
                </View>
              </TouchableOpacity>
              )
            }
          />
        </View>
        }

      </ScrollView>

    </View>
    )
  };

};

export default ViewVideoMix;
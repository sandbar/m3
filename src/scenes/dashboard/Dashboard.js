import React, {useEffect, useState, useLayoutEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  FlatList,
  Animated,
  Modal
} from 'react-native';

var TimeFormat = require('hh-mm-ss');

import SecondsWatchedStorage from '../../sync/SecondsWatchedStorage';
import Subscribe from '../Subscribe';
import styles from '../../common/styles';
import API from '../../communication/api';
import NetworkStatus from '../../utils/NetworkStatus';
import UserProxy from '../../communication/UserProxy';

const Dashboard = ({navigation}) => {

  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [offset, setOffset] = useState(0);
  const [mixID, setMixID] = useState(null);
  const [mixes, setMixes] = useState([]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          style={{left:20}}
          onPress={() => navigation.toggleDrawer()}
          hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        >
          <Image source={require('../../../img/btn-hamburger.png')} />
        </TouchableOpacity>
      ),
    });   
  },[navigation]);      

  useEffect(() => {
    let isSubscribed = true;
    if (NetworkStatus.isOnline()) {
      sendRequest(0, isSubscribed);
      checkForStoredSecondsWatched(isSubscribed);
    }
    return () => isSubscribed = false;
  },[]);

  // componentWillReceiveProps(nextProps) {
  //   if(NetworkStatus.isOnline()) {
  //     if (nextProps.search) {
  //       setTimeout(Actions.search, 1000);
  //     }
  //     //console.log("props", nextProps.trending);
  //     /*if (nextProps.trending !== null) {
  //       this.sendRequest(nextProps.trending);
  //     }*/
  //   }
  // },

  const pressRow = (mixID, isPremiumMix) => {
    if (isPremiumMix) {
      if (UserProxy.getIsSubscribed() || UserProxy.getIsPremium()) {
        navigation.navigate('ViewVideoMixDash', {mixID:mixID});
      } else {
        setModalVisible(true);
        setMixID(mixID);
      }
    } else {
      navigation.navigate('ViewVideoMixDash', {mixID:mixID});
    }
  };

  const imgLoaded = (index) => {
    //console.log("loaded", rowID);
    Animated.timing(
       animatedValues[index],
       {
         toValue: 1,
         duration: 500
       }
     ).start();
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  const getDuration = (duration) => {
    if (duration == null) {
      return (
        '0:00'
      )
    } else {
      var dur = TimeFormat.fromS(parseInt(duration));
      return (
        dur
      )
    }
  };

  const onEndReached = () => {
    //if (mixes.length > 9) {
      const page = parseInt(offset + 10);
      //console.log("page", page)
      setOffset(page);
      sendRequest(page);
    //}
  };

  const sendRequest = (offset, isSubscribed) => {
    //var param = trending ? 'orderByViews=1' : ''
    API.dashboard(offset)
      .then((responseJson) => {
        if (isSubscribed) {
          if (responseJson.error == 0) {
            // SUCCESS
            //console.log(responseJson.data)
            //prepareData(responseJson.data);
            setRefreshing(false);
            setLoading(false);
            setMixes(responseJson.data);
          } else {
            // Unknown Registration error
            console.log('DASHBOARD: UNKNOWN ERROR:', responseJson);
          }
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('DASHBOARD: ERROR CATCH:', error);
      });
  };

  // const prepareData = (mixes) => {
  //   if (mixes.length > 0) {
  //     var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  //     //var dataBlob = this.state.dashboardData.concat(mixes);
  //     var dataBlob = this.state.dashboardData == null ? [] : this.state.dashboardData;
  //     var animatedValues = this.state.animatedValues == null ? [] : this.state.animatedValues;
  //     for (var i = 0; i < mixes.length; i++) {
  //       dataBlob.push(mixes[i]);
  //       //create animated values for each mix image
  //       animatedValues.push(new Animated.Value(0));
  //     }
  //     this.setState({
  //       loaded: true,
  //       dashboardData: mixes,
  //       dataSource: ds.cloneWithRows(dataBlob),
  //       animatedValues: animatedValues
  //     });
  //   }
  // };

  const checkForStoredSecondsWatched = (isSubscribed) => {
    SecondsWatchedStorage.getStoredMixes()
      .then((mixes) => {
        //console.log(mixes)
        if (isSubscribed) {
          if (mixes.length == 0) {
            console.log('no offline seconds saved')
          } else {
            console.log('send and delete offline seconds!!!')
            for (var i = 0; i < mixes.length; i++) {
              console.log(mixes[i].secondsWatched, mixes[i].id);
              API.sendSecondsWatched(mixes[i].secondsWatched, mixes[i].id, false, mixes[i].id, responseHandler)
            }
          }
        }
      });
  };

  const responseHandler = (response) => {
    var responseJSON = JSON.parse(response);
    //console.log(responseJSON)
    if (responseJSON.error == 1) {
      console.log("error sending secondsWatched");
    } else {
      SecondsWatchedStorage.deleteStoredMix(responseJSON.data.id);
    }
  };

  const renderListView = () => {
    if (mixes.length > 0) {
      return (
        <View style={{flex:1}}>
          <Modal visible={modalVisible} animationType={"slide"} onRequestClose={() => setModalVisible(false)}>
            <Subscribe 
              requestClose={() => setModalVisible(false)} 
              mixID={mixID} 
              navigation={navigation} />
          </Modal>
          <View style={styles.mainContainerUnderHeader}>
            <View style={styles.profileTabContentVideo}>
              <FlatList
                data={mixes}
                numColumns={2}
                contentContainerStyle = {styles.videoList}
                keyExtractor={(utem, index) => index.toString()}
                onRefresh={() => sendRequest(0, true)}
                refreshing={refreshing}
                renderItem = {({item, index}) => (
                  <TouchableOpacity onPress={() => pressRow(item.id, item.isPremium)}>
                    <View style={styles.videoContainer}>
                      <ImageBackground
                        source={getThumb(item.thumb)}
                        style={[styles.videoThumb]} //{opacity: animatedValues[index]}
                        //onLoad={ () => imgLoaded(rowID) }
                      >
                        <View style={styles.videoLength}>
                          <Text style={styles.videoLengthText}>
                            {getDuration(item.mixDuration)}
                          </Text>
                        </View>
                        <Image
                          source={require('../../../img/icon-plus.png')}
                          style={[styles.videoPlus, {opacity: item.isPremium ? 1 : 0}]}
                          />
                      </ImageBackground>
                      <Text style={styles.videoTitle} numberOfLines={1}>{item.title}</Text>
                      <Text style={styles.videoInfo}>{item.author.username}</Text>
                      <Text style={styles.videoInfo}>{item.views == null ? '0' : item.views} plays</Text>
                    </View>
                  </TouchableOpacity>
                )}
                onEndReached={onEndReached}
                onEndReachedThreshold={9}
              />
            </View>
          </View>
        </View>
      )
    } else {
      return (
        <View style={styles.mainContainerUnderHeader}>
          <View style={styles.profileTabContentVideo}>
            <View style={styles.loadingContainer}>
              <Text style={styles.defaultText}>There are no mixes.</Text>
            </View>
          </View>
        </View>
      )
    }
  };  

  if (NetworkStatus.isOnline()) {
    if (loading) {
      return (
        <View style={styles.mainContainerUnderHeader}>
          <View style={styles.profileTabContentVideo}>
            <View style={styles.loadingContainer}>
              <Image source={require('../../../img/m3logo.png')} />
            </View>
          </View>
        </View>        
      )
    }
    return renderListView();
  } else {
    return (
      <View style={styles.containerGeneric}>
        <View style={styles.loadingContainer}>
          <View style={{flexDirection:'row', marginTop:10}}>
            <Image source={require('../../../img/icon-offline.png')} />
            <Text style={styles.offlineText}>Not available offline</Text>
          </View>
        </View>
      </View>
    )
  }

};

export default Dashboard;

import React, {useState, useEffect} from 'react';

import {
  View,
  Image,
  Text
} from 'react-native';

import API from '../../../communication/api';
import styles from '../../../common/styles';

const RaterView = (props) => {

  const {commentData} = props;
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = useState({ username:'', image: null });

  useEffect(() => {
    let isSubscribed = true;
    API.profile(commentData.user_id)
    .then((responseJson) => {
      if (isSubscribed) {
        if(responseJson.error == 0) {
          // SUCCESS
          //console.log("userData", responseJson.data);
          setUserData(responseJson.data);
          setLoading(false);
        } else {
          // Unknown Registration error
          console.log('PROFILE: UNKNOWN ERROR:', responseJson);
        }
      }
    })
    .catch((error) => {
      // Request sending/parsing error
      console.log('GET PROFILE: ERROR CATCH:', error);
    });
    return () => isSubscribed = false;
  },[]);

  const getAvatar = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../../img/avatar-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/users-images/" + thumb}
      )
    }
  };

  const genRatingStars = (stars) => {
    var rows = [];
    for (var i=0; i<5; i++) {
      if (i < stars) {
        rows.push(<Image source={require('../../../../img/icon-star-on.png')} style={styles.star} key={i} />);
      } else {
        rows.push(<Image source={require('../../../../img/icon-star-off.png')} style={styles.star} key={i} />);
      }
    }
    return rows;
  };

  if (loading) {
    return (
      <View></View>
    )
  }
  return (
    <View style={styles.ratingContainer} collapsable={false}>
      <Image
        source={getAvatar(userData.image)}
        style={styles.ratingAvatar}
      />
      <View style={styles.ratingInfo} collapsable={false}>
        <View style={styles.ratingStarsContainer}>

          {genRatingStars(commentData.rating)}

        </View>
        <Text style={styles.ratingRater}>

          {userData.username}

        </Text>
        {<Text style={styles.ratingDescription}>{commentData.comment !== null ? commentData.comment : ''}</Text>}
      </View>
    </View>
  );
  
};

export default RaterView;

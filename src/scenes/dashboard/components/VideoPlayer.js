import React, {useState, useEffect, useRef} from 'react';

import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
  Dimensions,
  StatusBar
} from 'react-native';

var TimeFormat = require('hh-mm-ss');
import Video from 'react-native-video';
import Orientation from 'react-native-orientation-locker';

import PlayerControls from '../components/PlayerControls';
import SecondsWatchedStorage from "../../../sync/SecondsWatchedStorage";
import styles from '../../../common/styles';
import API from '../../../communication/api';

const VidPlayer = (props) => {

  let timeOut;
  let currentSecond = 0;
  let FORWARD_DURATION = 7;
  let fsToggle = false;
  //const videoPlayerRef = useRef(null)

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.fsState == true) {
  //     this.addOrientationListener();
  //     Orientation.lockToLandscape();
  //     //this.videoPlayer.seek(nextProps.goSeek);
  //     this.setState({seekTime:nextProps.goSeek});
  //   } else if (nextProps.fsState == false) {
  //     Orientation.lockToPortrait();
  //     this.setState({
  //       fsLocked:false,
  //       //currentTime:nextProps.goSeek
  //     });
  //     this.setState({seekTime:nextProps.goSeek});
  //   }

  //   if (nextProps.onMixEnd) {
  //     this.setState({paused:true})
  //     if (this.state.fs) {
  //       //causes error when re-toggling
  //       //this.toggleFS();
  //     }
  //   }
  // },

  const {videoSource, onEndHandler, handleVideoFS, onMixEnd, mixID, goSeek, isOffline, isPremium} = props;

  const [fullscreen, setFullscreen] = useState(false);
  const [play, setPlay] = useState(false);
  const [showControls, setShowControls] = useState(true);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);

  const [videoControlsOpacity, setVideoControlsOpacity] = useState(1);
  const [paused, setPaused] = useState(false);
  const [rate, setRate] = useState(1);
  const [volume, setVolume] = useState(1);
  const [muted, setMuted] = useState(false);
  let [secondsWatched, setSecondsWatched] = useState(0);
  const [mixPlaybackTime, setMixPlaybackTime] = useState(0);
  const [resizeMode, setResizeMode] = useState('contain');
  const [elapsedTime, setElapsedTime] = useState(0);
  const [videoControlsStyle, setVideoControlsStyle] = useState(styles.videoControls);
  const [videoPlayPauseStyle, setVideoPlayPauseStyle] = useState(styles.videoPlayPauseContainer);
  const [videoPlayPauseIconStyle, setVideoPlayPauseIconStyle] = useState(styles.videoPayPauseIcon)
  const [videoMixTotalStyle, setVideoMixTotalStyle] = useState(styles.mixTotal);
  const [vidSrc, setVidSrc] = useState(videoSource);

  const vidRef = useRef(null);

  useEffect(() => {
    Orientation.addOrientationListener(handleOrientation);

    return () => {
      Orientation.removeOrientationListener(handleOrientation);
    };
  }, []);  

  useEffect(() => {
    if (props.onMixEnd) {
      console.log('we need to pause')
      setPlay(false);
    }
    if (props.videoSource) {
      console.log('we have a new videoSource')
      setVidSrc(props.videoSource);
      !props.onMixEnd && setPlay(true);
    }
  },[props.onMixEnd, props.videoSource]);  

  /** VIDEO HANDLING **/

  // const onLoad = (data) => {
  //   console.log(data, data.currentTime, data.duration)
  //   if (seekTime) {
  //     videoPlayer.seek(seekTime);
  //     setCurrentTime(seekTime);
  //     setDuration(data.duration);
  //   } else {
  //     setCurrentTime(data.currentTime);
  //     setDuration(data.duration);
  //   }

  //   //start timer
  //   const INTERVAL = 10000;
  //   //auto clearing interval, start only at the beginning of the mix
  //   if (elapsedTime == 0) {
  //     let secondsWatchedInterval = setInterval(sendSecondsWatched, INTERVAL);
  //   }
  // };

  // const onProgress = (data) => {

  //   let currentTime = Math.floor(data.currentTime)

  //   if (currentTime > currentSecond) {
  //     //console.log("NEXT SECOND", Math.floor(data.currentTime), currentSecond);
  //     currentSecond = currentTime;
  //     secondsWatched++;
  //   }

  //   var tmpTime = elapsedTime + data.currentTime;
  //   var endTime = parseInt(Math.floor(duration) - 0.1);

  //   if (currentTime > 0) {
  //     if (currentTime > endTime) {
  //       // console.log("Yo", currentTime, endTime);
  //       // don't do anything, currentTime will increase mixPlaybackTime too much
  //     } else {
  //       var mixPlaybackTime = TimeFormat.fromS(Math.ceil(tmpTime));
  //       setCurrentTime(data.currentTime);
  //       setSecondsWatched(secondsWatched);
  //       setMixPlaybackTime(mixPlaybackTime);
  //     }
  //   }
  // };

  // const getCurrentTimePercentage = () => {
  //   if (currentTime > 0) {
  //     return parseFloat(currentTime) / parseFloat(duration);
  //   } else {
  //     return 0;
  //   }
  // };

  // const completedPercentage = () => {
  //   getCurrentTimePercentage(currentTime, duration) * 100;
  // };

 

  // const getMixPlaybackTime = () => {
  //   let tmpTime = elapsedTime + currentTime;
  //   if (currentTime > 0) {
  //     return TimeFormat.fromS(Math.ceil(tmpTime));
  //   }
  // };

  

  return (
    <View style={fullscreen ? styles.videoPlayerContainerFS : styles.videoPlayerContainer} >
      <TouchableWithoutFeedback onPress={() => showVideoControls()}>
        <View>
          <Video source={{uri: vidSrc}}
            style={fullscreen ? styles.videoPlayerFS : styles.videoPlayer}
            rate={rate}
            paused={!play}
            volume={volume}
            muted={muted}
            resizeMode={resizeMode}
            controls={false}
            onLoad={onLoadEnd}
            onProgress={onProgress}
            onEnd={onVideoEnd}
            repeat={true}
            ref={vidRef}
          />
          {showControls && (
            <View style={styles.controlOverlay}>
              <TouchableOpacity
                onPress={() => handleFullscreen()}
                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                style={styles.fullscreenButton}>
                {fullscreen ? <Image source={require('../../../../img/btn-fs.png')} /> : <Image source={require('../../../../img/btn-fs.png')} />}
              </TouchableOpacity>
              <PlayerControls
                onPlay={handlePlayPause}
                onPause={handlePlayPause}
                playing={play}
                showPreviousAndNext={false}
                showSkip={true}
                skipBackwards={skipBackward}
                skipForwards={skipForward}
              />
              {/* <ProgressBar
                currentTime={currentTime}
                duration={duration > 0 ? duration : 0}
                onSlideStart={handlePlayPause}
                onSlideComplete={handlePlayPause}
                onSlideCapture={onSeek}
              /> */}
            </View>
          )}          
          {/* <View style={[videoPlayPauseStyle, {opacity:videoControlsOpacity}]}>
            <TouchableWithoutFeedback onPress={() => {toggleVideoPlayBack()}}>
              <Image 
                source={paused ? require('../../../../img/btn-play.png') : require('../../../../img/btn-pause.png')} 
                style={videoPlayPauseIconStyle} 
              />
            </TouchableWithoutFeedback>
            <Image
              source={require('../../../../img/icon-plus-mix.png')}
              style={[styles.videoPlusMix, {opacity: props.isPremium ? 1 : 0}]} />
          </View> */}
        </View>
      </TouchableWithoutFeedback>
{/* 
      <View style={[videoControlsStyle, {opacity:videoControlsOpacity}]}>
        <View style={styles.videoProgress}>
          <View style={[styles.progressBar]}> */}
            {/* <ProgressController
              duration={duration}
              currentTime={currentTime}
              percent={completedPercentage}
              onNewPercent={onProgressChanged}
              mixPlaybackTime={mixPlaybackTime}
            /> */}
            {/* </View>
        </View>
        <TouchableOpacity onPress={() => {toggleFS()}}>
          <Image source={require('../../../../img/btn-fs.png')} />
        </TouchableOpacity>
      </View> */}

      {/*<View style={this.state.videoMixTotalStyle}>
        <Text style={styles.mixTotalText}>{this.getMixPlaybackTime() == null ? '' : this.getMixPlaybackTime()}</Text>
      </View>*/}

    </View>
  );

  function handleOrientation(orientation) {
    console.log(orientation);
    if (orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT') {
      setFullscreen(true);
      StatusBar.setHidden(true);
      handleVideoFS(true);
    } else {
      setFullscreen(false);
      StatusBar.setHidden(false);
      handleVideoFS(false);
    }
  }  

  function handleFullscreen() {
    fsToggle = !fsToggle;
    fsToggle ?
    Orientation.lockToLandscapeLeft() :
    Orientation.lockToPortrait()
  }
  
  function handlePlayPause() {
    // If playing, pause and show controls immediately.
    if (play) {
      setPlay(false); setShowControls(true);
      return;
    }  

    setPlay(true);
    setTimeout(() => setShowControls(false), 2000);
  }    

  function skipBackward() {
    vidRef.current.seek(currentTime - 15);
    setCurrentTime(currentTime - 15);
  }

  function skipForward() {
    vidRef.current.seek(currentTime + 15);
    setCurrentTime(currentTime + 15);
  }

  function onSeek(data) {
    vidRef.current.seek(data.seekTime);
    setCurrentTime(data.seekTime);
  }

  function onLoadEnd(data) {
    setDuration(data.duration);
    setCurrentTime(data.currentTime);
  }

  function onProgress(data) {
    setCurrentTime(data.currentTime);
  }

  function onEnd() {
    setPlay(false);
    vidRef.current.seek(0);
  }

  function showVideoControls() {
    showControls
      ? setShowControls(false)
      : setShowControls(true)
  }

  function onProgress(data) {
    let currentTime = Math.floor(data.currentTime)
    if (currentTime > currentSecond) {
      currentSecond = currentTime;
      secondsWatched++;
      if (secondsWatched == 20) {
        sendSecondsWatched(secondsWatched);
        secondsWatched = 0;
      }
    }
  };

  function sendSecondsWatched(seconds) {
    if (!props.isOffline) {
      console.log("sending seconds watched", seconds);
      API.sendSecondsWatched(seconds, mixID, false, null, responseHandler)
    } else {
      SecondsWatchedStorage.storeMixData(mixID, seconds);
    }    
  };

  function onVideoEnd() {
    vidRef.current.seek(0);
    setVidSrc(null);
    if (onEndHandler) {
      onEndHandler();
    }
  };

  function responseHandler(response) {
    var responseJSON = JSON.parse(response);
    console.log("secondswatchJSON", responseJSON);
    if (responseJSON.error == 1) {
      console.log("error sending secondsWatched");
    } else {
      console.log('seconds watched sent successfully');
    }
  };

  function responseHandler(response) {
    var responseJSON = JSON.parse(response);
    console.log("secondswatchJSON", responseJSON);
    if (responseJSON.error == 1) {
      console.log("error sending secondsWatched");
    } else {
      console.log('seconds watched sent successfully');
    }
  };  

};

export default VidPlayer;

import React, {useState} from 'react';

import {
  FlatList,
  Image,
  Text,
  View,
  TouchableOpacity,
  Animated
} from 'react-native';

import styles from '../../../common/styles';
const MIN_TITLE_HEIGHT = 40;

const Breakdown = (props) => {

  const {mixClips} = props;
  const [breakdownExpanded, setBreakdownExpanded] = useState(true);
  const [slidingPanelBreakdownHeight, setSlidingPanelBreakdownHeight] = useState(0);
  let slidingPanelBreakdownAnim = new Animated.Value(MIN_TITLE_HEIGHT);

  const setMaxHeight = (event) => {
    // if panel height has not been measured, set the height once, then close
    if (!slidingPanelBreakdownHeight) {
      setSlidingPanelBreakdownHeight(event.nativeEvent.layout.height);
      slidingPanelBreakdownAnim.setValue(MIN_TITLE_HEIGHT);
    }
  };

  const getThumb = (thumb) => {
    if (thumb == null) {
      return (
        require('../../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  };

  const pressClipsRow = (clipID) => {
    /*
    if (this.props.updatePlaylist instanceof Function) {
      //setState of view breakdownRowStyleOn/breakdownRowStyleOff
      this.props.updatePlaylist(clipID);
    }*/
  };

  const toggleBreakdownPanel = () => {
      let initialValue    = breakdownExpanded ? slidingPanelBreakdownHeight + MIN_TITLE_HEIGHT : MIN_TITLE_HEIGHT,
          finalValue      = breakdownExpanded ? MIN_TITLE_HEIGHT : slidingPanelBreakdownHeight + MIN_TITLE_HEIGHT;

      setBreakdownExpanded(!breakdownExpanded);

      slidingPanelBreakdownAnim.setValue(initialValue);
      Animated.spring(
          slidingPanelBreakdownAnim,
          {
              toValue: finalValue,
              useNativeDriver: false
          }
      ).start();
  };

  return (
    <Animated.View style={[styles.slidingPanelContainer, {height:slidingPanelBreakdownAnim}]}>

      <TouchableOpacity style={styles.panelTitle} onPress={() => toggleBreakdownPanel()}>
        <Text style={[styles.panelIcon,
          {marginLeft: !breakdownExpanded ? 1 : 0,
          marginRight: !breakdownExpanded ? 1 : 0}]}>
            {!breakdownExpanded ? '-' : '+'}
        </Text>
        <Text style={styles.panelTitleText}>Breakdown</Text>
      </TouchableOpacity>

      <View onLayout={e => setMaxHeight(e)} style={{paddingBottom: 10}}>
        {mixClips.map((clip, index) => {
          return (
            <TouchableOpacity onPress={() => pressClipsRow(clip.id)} key={index.toString()}>
              <View style={styles.clipsContainer}>
                <Image
                  source={getThumb(clip.thumb)}
                  style={styles.clipThumb}
                />
                <View style={styles.clipDetail}>
                  <Text style={styles.defaultText}>{parseInt(index) + 1}</Text>
                  <Text style={styles.clipTitle}>{clip.title}</Text>
                  <Text style={styles.defaultText}>{clip.category.name}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )
        })}
      </View>
    </Animated.View>
  );

};

export default Breakdown;
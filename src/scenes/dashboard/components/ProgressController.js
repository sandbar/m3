import React, {useState, useEffect} from 'react';
import {
  Animated,
  PanResponder,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

import Colors from '../../../common/colors';
import styles from '../../../common/styles';

const ProgressController = (props) => {

  const {duration, currentTime, percent, onNewPercent, mixPlaybackTime} = props;
  const {width, height} = Dimensions.get('window');
  const [moving, setMoving] = useState(false);
  const [screenWidth, setScreenWidth] = useState(width);

  let lineX = new Animated.Value(0);
  let slideX = new Animated.Value(0);
  let holderPanResponder;
  

  // componentWillReceiveProps(nextProps) {
  //     if (!this.state.moving) {
  //         this.state.slideX.setValue(this.computeScreenX(nextProps.percent));
  //     }
  // },

  useEffect(() => {
    let isSubscribed = true;
    holderPanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderGrant: (e, gestureState) => {
        if (isSubscribed) {
          //let {slideX} = this.state;
          setMoving(true);
          slideX.setOffset(slideX._value);
          slideX.setValue(0);
        }
      },
      onPanResponderMove: (e, gestureState) => {
        if (isSubscribed) {
          let totalX = slideX._offset + gestureState.dx;
          let newPercent = (totalX / screenWidth) * 100;
          notifyPercentChange(newPercent, true);
          Animated.event([
              null, {dx: slideX}
          ])(e, gestureState);
        }
      },
      onPanResponderRelease: (e, gesture) => {
        if (isSubscribed) {
          slideX.flattenOffset();
          let newPercent = (slideX._value / screenWidth) * 100;
          setMoving(false);
          notifyPercentChange(newPercent, false);
        }
      }
    }); 
    return () => isSubscribed = false; 
  },[]);

  const computeScreenX = (percent) => {
    return percent * screenWidth / 100;
  };

  const notifyPercentChange = (newPercent, paused) => {
    if (onNewPercent instanceof Function) {
      onNewPercent(newPercent, paused);
    }
  };

  const onLayout = (e) => {
    setScreenWidth(e.nativeEvent.layout.width - (10));
  };

  const getHolderStyle = () => {
    if (screenWodth > 0 && slideX._value > 0) {
      var interpolatedAnimation = slideX.interpolate({
        inputRange: [0, width],
        outputRange: [0, width],
        extrapolate: "clamp"
      });
      return [styles.videoProgressHolder, moving && styles.videoProgressActiveHolder,
          {transform: [{translateX: interpolatedAnimation}]}
      ];
    } else {
      return [styles.videoProgressHolder];
    }
  };

  const onLinePressed = (e) => {
    let newPercent = (e.nativeEvent.locationX / screenWidth) * 100;
    notifyPercentChange(newPercent, false);
  };

  return (
    <View style={{alignSelf:'stretch', flexDirection: 'row', height:32, alignItems: 'center'}}>
      {/*<Text style={[styles.videoProgressTimeText, {marginRight: 10}]}>{TimeFormat.fromS(Math.ceil(currentTime))}</Text>*/}
      <View style={styles.videoProgressBarView}
        onLayout={(e) => onLayout(e)} {...holderPanResponder.panHandlers}>
        <View style={{flex: 1, flexDirection: 'row', top: 5, alignItems: 'center'}}>
          <TouchableOpacity style={[styles.videoProgressLine, {flex: percent, backgroundColor: Colors.brandGreen}]}
            onPress={onLinePressed}/>
          <TouchableOpacity style={[styles.videoProgressLine, {flex: 100 - percent, backgroundColor: 'white'}]}
            onPress={onLinePressed}/>
          <Animated.View style={getHolderStyle()}/>
        </View>
      </View>
      {/*<Text style={[styles.videoProgressTimeText, {marginLeft: 10}]}>{TimeFormat.fromS(Math.ceil(duration))}</Text>*/}
      <Text style={[styles.videoProgressTimeText, {marginLeft: 10}]}>{mixPlaybackTime}</Text>
    </View>
  )

};

export default ProgressController;

import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  SafeAreaView
} from 'react-native';

// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Colors from '../../../common/colors';
import Button from '../../../common/button';
import StarRating from './StarRating';
import styles from '../../../common/styles';
import API from '../../../communication/api';


const Rate = ({navigation, route}) => {
    
  const {mixID} = route.params;
  const [comment, setComment] = useState('');
  const [rating, setRating] = useState(0);
  const [modalVisible, setModalVisible] = useState(true);

  const onStarRatingPress = (_rating) => {
    setRating(_rating);
  };

  const onRatePress = () => {
    API.rate(mixID, rating, comment)
      .then((responseJson) => {
        if(responseJson.error == 0) {
          // Actions.pop();
          // Actions.refresh({getData:true});
          navigation.navigate('ViewVideoMixDash', {refreshView: true});
        } else {
          // Unknown error
          console.log('UNKNOWN ERROR:', responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:', error);
      });
  };

  const onClosePress = () => {
    setModalVisible(false);
    navigation.goBack();
  };
  
  return (
    <Modal visible={modalVisible} animationType={"slide"} onRequestClose={() => onClosePress()}>
      <SafeAreaView style={styles.mainContainer}>
        <View style={styles.modalHeader}>
          <View style={styles.modalHeaderTitleContainer}>
            <Text style={styles.modalHeaderTitle}>RATE</Text>
          </View>
          <TouchableOpacity onPress={() => onClosePress()} style={styles.btnClose}>
            <Image source={require('../../../../img/btn-close-blue.png')} style={styles.btnCloseIcon} />
          </TouchableOpacity>
        </View>
        <View style={styles.loginScrollContainer}>
          <View style={styles.rateMainContainer}>

            <Text style={[styles.defaultText, {marginTop:6, marginBottom:12}]}>Select stars to rate</Text>
            <View style={[styles.ratingStarsContainer, {marginBottom:18}]}>
              <StarRating
                maxStars={5}
                rating={rating}
                selectedStar={(rating) => onStarRatingPress(rating)}
              />
            </View>

            <TextInput
              style={[styles.inputField, {marginBottom:18}]}
              placeholder={'Add a comment'}
              placeholderTextColor={Colors.defaultInputText}
              value={comment}
              onChangeText={(text) => setComment(text)}
              underlineColorAndroid='transparent'
            />

            <Button
              text={'RATE'}
              onPress={onRatePress}
              //opacity={loading ? 0.5 : 1}
              //disabled={loading ? true : false}
              stretch={'stretch'}
            />
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );

};

export default Rate;

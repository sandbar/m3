import React, {useState, useEffect, useRef} from 'react';

import {
  View,
} from 'react-native';

var TimeFormat = require('hh-mm-ss');
import Video from 'react-native-video';

import SecondsWatchedStorage from "../../../sync/SecondsWatchedStorage";
import styles from '../../../common/styles';
import API from '../../../communication/api';

const VidPlayeriOS = (props) => {

  let currentSecond = 0;
  const {videoSource, onEndHandler, onFS, onMixEnd, mixID, fsState, goSeek, isOffline, isPremium} = props;
  const vidRef = useRef(null);

  const [vidSrc, setVidSrc] = useState(videoSource);
  //const [videoControlsOpacity, setVideoControlsOpacity] = useState(1);
  const [paused, setPaused] = useState(false);
  const [rate, setRate] = useState(1);
  const [volume, setVolume] = useState(1);
  const [muted, setMuted] = useState(false);
  //const [currentTime, setCurrentTime] = useState(0.0);
  //const [duration, setDuration] = useState(0.0);
  //let [secondsWatched, setSecondsWatched] = useState(0);
  //const [mixPlaybackTime, setMixPlaybackTime] = useState(0);
  //const [resizeMode, setResizeMode] = useState('contain');
  //const [elapsedTime, setElapsedTime] = useState(0);
  const [videoContainerStyle, setVideoContainerStyle] = useState(styles.videoPlayerContainer);
  const [videoStyle, setVideoStyle] = useState(styles.videoPlayer);
  let [secondsWatched, setSecondsWatched] = useState(0);

  useEffect(() => {
    if (props.onMixEnd) {
      console.log('we need to pause')
      setPaused(true);
    }
    if (props.videoSource) {
      console.log('we have a new videoSource')
      setVidSrc(props.videoSource);
      !props.onMixEnd && setPaused(false);
    }
  },[props.onMixEnd, props.videoSource]);

  // useEffect(() => {
  //   let secondsWatchInterval = setInterval(() => {
  //     setSecondsWatched(secondsWatched + 1);
  //     console.log(secondsWatched)
  //     if (secondsWatched == 20) {
  //       console.log('set interval sending', secondsWatched);
  //       sendSecondsWatched(secondsWatched);
  //       setSecondsWatched(0);
  //     }
  //   }, 1000);
  //   return () => clearInterval(secondsWatchInterval);
  // },[]);

  const onProgress = (data) => {

    let currentTime = Math.floor(data.currentTime)
    //console.log(currentTime);
    if (currentTime > currentSecond) {
      currentSecond = currentTime;
      //console.log(currentSecond);
      secondsWatched++;
      console.log(secondsWatched);
      if (secondsWatched == 20) {
        sendSecondsWatched(secondsWatched);
        secondsWatched = 0;
      }
    }
  };

  const sendSecondsWatched = (seconds) => {
    if (!props.isOffline) {
      console.log("sending seconds watched", seconds);
      API.sendSecondsWatched(seconds, mixID, false, null, responseHandler)
    } else {
      // we're watching offline, save seconds watched to storage
      SecondsWatchedStorage.storeMixData(mixID, seconds);
    }    
  };

  const onVideoEnd = () => {
    // update videoSource
    vidRef.current.seek(0);
    setVidSrc(null);
    if (onEndHandler) {
      onEndHandler();
    }
    //setElapsedTime(totalMixTime);
    //setCurrentTime(0);
    //currentSecond = 0;
    //secondsWatched = 0;
  };

  const responseHandler = (response) => {
    var responseJSON = JSON.parse(response);
    console.log("secondswatchJSON", responseJSON);
    if (responseJSON.error == 1) {
      console.log("error sending secondsWatched");
    } else {
      console.log('seconds watched sent successfully');
    }
  };

  if (vidSrc) {
    return (
      <View style={videoContainerStyle} >
        <Video 
          source={{uri: vidSrc}}
          style={videoStyle}
          rate={rate}
          paused={paused}
          volume={volume}
          muted={muted}
          resizeMode={'contain'}
          controls={true}
          //onLoad={onLoad}
          onProgress={onProgress}
          onEnd={onVideoEnd}
          ref={vidRef}
          //repeat={true}
        />
      </View>
    );
  } else {
    return (
      <View style={videoContainerStyle} ></View>
    )
  }


};

export default VidPlayeriOS;
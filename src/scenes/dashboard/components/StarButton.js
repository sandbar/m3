import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
  TouchableOpacity,
  Image,
} from 'react-native';

var styles = require('../../../common/styles');

class StarButton extends Component {

  constructor(props) {
    super(props);

    this.onButtonPress = this.onButtonPress.bind(this);
  }

  onButtonPress() {
    this.props.onStarButtonPress(this.props.rating);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.onButtonPress}
      >
        <Image
          source={this.props.starSelected ? require('../../../../img/icon-star-on.png') : require('../../../../img/icon-star-off.png')}
          style={styles.star}
        />
      </TouchableOpacity>
    );
  }
}

StarButton.propTypes = {
  rating: PropTypes.number,
  onStarButtonPress: PropTypes.func,
  starSelected: PropTypes.bool,
};

export default StarButton;

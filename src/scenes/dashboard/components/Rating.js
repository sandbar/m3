import React, {useState, useEffect} from 'react';

import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Animated
} from 'react-native';

import Button from '../../../common/button';
import RaterView from './RaterView.js';
import styles from '../../../common/styles';

const Rating = (props) => {

  const MIN_TITLE_HEIGHT = 44;

  const {mixRatings, mixID, navigation} = props;
  const [ratingsAverage, setRatingsAverage] = useState(0);
  const [ratingExpanded, setRatingExpanded] = useState(true);
  const [firstLoad, setFirstLoad] = useState(true);
  const [slidingPanelRatingHeight, setSlidingPanelRatingHeight] = useState(MIN_TITLE_HEIGHT)

  let slidingPanelRatingAnim = new Animated.Value(MIN_TITLE_HEIGHT);
    

  useEffect(() => {
    let isSubscribed = true;
    if (mixRatings.length && isSubscribed > 0) {
      let ratingsTotal = 0;
      for (var i = 0; i < mixRatings.length; i++) {
        ratingsTotal += mixRatings[i].rating;
      }
      setRatingsAverage(Math.floor(ratingsTotal / mixRatings.length));
    }
    return () => isSubscribed = false;
  },[]);


  const setMaxHeight = (event) => {
    // if panel height has not been measured or more content has loaded to make the panel larger, set the height once, then close
    if (!slidingPanelRatingHeight || event.nativeEvent.layout.height > slidingPanelRatingHeight) {
      // console.log("setting max height", event.nativeEvent.layout.height)
      setSlidingPanelRatingHeight(event.nativeEvent.layout.height)
      // if the content has just loaded, close the panel
      if (firstLoad) {
        setFirstLoad(false);
        slidingPanelRatingAnim.setValue(MIN_TITLE_HEIGHT);
      }
    }
  };

  const toggleRatingPanel = () => {
    let initialValue    = ratingExpanded ? slidingPanelRatingHeight + MIN_TITLE_HEIGHT : MIN_TITLE_HEIGHT,
        finalValue      = ratingExpanded ? MIN_TITLE_HEIGHT : slidingPanelRatingHeight + MIN_TITLE_HEIGHT;

    setRatingExpanded(!ratingExpanded);

    slidingPanelRatingAnim.setValue(initialValue);
    Animated.spring(
        slidingPanelRatingAnim,
        {
            toValue: finalValue,
            useNativeDriver: false
        }
    ).start();
  };

  const genRatingStars = (stars) => {
    var rows = [];
    for (var i=0; i<5; i++) {
      if (i < stars) {
        rows.push(<Image source={require('../../../../img/icon-star-on.png')} style={styles.star} key={i} />);
      } else {
        rows.push(<Image source={require('../../../../img/icon-star-off.png')} style={styles.star} key={i} />);
      }
    }
    return rows;
  };

  return (
    <Animated.View
      style={[styles.slidingPanelContainer, {height:slidingPanelRatingAnim}]}>

      <TouchableOpacity style={styles.panelTitle} onPress={() => toggleRatingPanel()}>
        <Text style={[styles.panelIcon,
          {marginLeft: !ratingExpanded ? 1 : 0,
          marginRight: !ratingExpanded ? 1 : 0}]}>
            {!ratingExpanded ? '-' : '+'}
        </Text>
        <View style={styles.ratingStarsContainer}>
          <Text style={[styles.panelTitleText, {marginRight:6}]}>Rating({mixRatings.length})</Text>
            {genRatingStars(ratingsAverage)}
        </View>
      </TouchableOpacity>

      <View onLayout={e => setMaxHeight(e)}>
        <Button
          text={'RATE'}
          onPress={() => navigation.navigate('Rate', {mixID:mixID})}
        />

        {
          mixRatings.length > 0 &&
            mixRatings.map((rating) => {
              return (
                <RaterView commentData={rating} />
              )
            })
        }
      </View>
    </Animated.View>
  );

};

export default Rating;

import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ListView
} from 'react-native';

import {Actions} from 'react-native-router-flux';
import Colors from '../../common/colors';
var TimeFormat = require('hh-mm-ss');
var styles = require('../../common/styles');
var API = require('../../communication/api');
var NetworkStatus = require('../../utils/NetworkStatus');


module.exports = React.createClass({

  getInitialState() {
    return {
      searchData: [],
      loaded: false,
    };
  },

  componentDidMount() {
    if (this.props.searchData) {
      this.prepareData(this.props.searchData);
    }
  },

  prepareData(data) {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    var dataBlob = [];
    if (data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        dataBlob.push(data[i]);
      }
      this.setState({
        loaded: true,
        searchData: data,
        dataSource: ds.cloneWithRows(dataBlob)
      });
    } else {
      this.setState({
        loaded: true,
        searchData: data,
      });
    }
  },

  pressRow(mixID: number) {
    Actions.viewVideoMixDash({mixID:mixID});
  },

  renderRow(rowData, sectionID, rowID) {
    return (
      <TouchableOpacity onPress={() => this.pressRow(rowData.id)}>
        <View style={styles.videoContainer}>
          <Image
            source={this.getThumb(rowData.thumb)}
            style={styles.videoThumb} >
            <View style={styles.videoLength}>
              <Text style={styles.videoLengthText}>
                {this.getDuration(rowData.mixDuration)}
              </Text>
            </View>
          </Image>
          <Text style={styles.videoTitle} numberOfLines={1}>{rowData.title}</Text>
          <Text style={styles.videoInfo}>{rowData.username}</Text>
          <Text style={styles.videoInfo}>{rowData.views == null ? 'no' : rowData.views} plays</Text>
        </View>
      </TouchableOpacity>
    );
  },

  getThumb(thumb) {
    if (thumb == null) {
      return (
        require('../../../img/thumb-default.png')
      )
    } else {
      return (
        {uri: "http://stream.maxmyminutes.com/livethumbs/" + thumb}
      )
    }
  },

  getDuration(duration) {
    if (duration == null) {
      return (
        '0:00'
      )
    } else {
      var dur = TimeFormat.fromS(parseInt(duration));
      return (
        dur
      )
    }
  },

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }
    return this.renderListView();
  },

  renderLoadingView() {
    return (
      <View style={styles.mainContainerUnderHeader}>
        <View style={styles.profileTabContentVideo}>
          <View style={styles.loadingContainer}>
            <Text style={styles.defaultText}>Loading...</Text>
          </View>
        </View>
      </View>
    )
  },

  renderListView() {
    if (this.state.searchData.length > 0) {
      return (
        <View style={styles.mainContainerUnderHeader}>
          <View style={styles.profileTabContentVideo}>
            <ListView
              dataSource={this.state.dataSource}
              contentContainerStyle = {styles.videoList}
              renderRow={this.renderRow}
            />
          </View>
        </View>
      )
    } else {
      return (
        <View style={styles.mainContainerUnderHeader}>
          <View style={styles.profileTabContentVideo}>
            <View style={styles.loadingContainer}>
              <Text style={styles.mediumText}>Your search returned no results</Text>
              <TouchableOpacity
                onPress={() => {
                  Actions.pop({popNum:2});
                  setTimeout(() => {
                    Actions.refresh({ search:true });
                  }, 10);
                }}>
                <Text
                  style={{
                    marginTop: 20,
                    color: Colors.brandGreen,
                    fontSize: 15,
                    fontFamily: 'Gotham-Medium',
                  }}
                  >Try again</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  Actions.pop({popNum:2});
                }}>
                <Text
                  style={{
                    marginTop: 20,
                    color: Colors.brandGreen,
                    fontSize: 15,
                    fontFamily: 'Gotham-Medium',
                  }}
                  >Dashboard</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )
    }
  },

});

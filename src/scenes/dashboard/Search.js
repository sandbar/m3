import React from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  Picker,
  Animated,
  Keyboard,
  LayoutAnimation,
} from 'react-native';

import {Actions} from 'react-native-router-flux';
import Colors from '../../common/colors';
import Button from '../../common/button';
var styles = require('../../common/styles');
var API = require('../../communication/api');
var NetworkStatus = require('../../utils/NetworkStatus');

const {width, height} = require('Dimensions').get('window');
const Item = Picker.Item;

module.exports = React.createClass({

  getInitialState() {
    return {
      modalVisible: true,
      loading: true,
      currentTags: [],
      currentTagsIDs: [],
      allTags: [],
      allTagIDs: [],
      matchingTags: [],
      tag: '',
      showTagOverlay: false,
      overlayPosition: width,
      selectedTime: 'All',
      pickerOpen: false,
      pickerCoverHeight: 0,
      pickerCoverOpacity: 0,
      pickerCoverAnim: new Animated.Value(0),
      pickerBottom: 0,
      pickerAnim: new Animated.Value(height),
      tagHolderHeight: height - 60,
    };
  },

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    //get all tags
    if (NetworkStatus.isOnline()) {
      API.tag()
        .then((responseJson) => {
          if(responseJson.error == 0) {
            // SUCCESS
            //console.log(responseJson);
            this.parseResponse(responseJson.data);
          } else {
            console.log('TAGS: UNKNOWN ERROR:');
            console.warn(responseJson);
          }
        })
        .catch((error) => {
          console.log('TAGS: ERROR CATCH:');
          console.warn(error);
        });
    }
  },

  componentWillUnmount () {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  },

  keyboardDidShow (e) {
    let newSize = height - e.endCoordinates.height;
    let keyboardHeight = height - newSize;
    this.setState({
      tagHolderHeight: height - keyboardHeight - 128,
    });
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeNone);
  },

  keyboardDidHide (e) {
    this.setState({
      tagHolderHeight : height - 60,
    });
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeNone);
  },

  parseResponse(data) {
    for (var i=0; i<data.length; i++) {
      this.state.allTags.push(data[i].name);
      this.state.allTagIDs.push(data[i].id);
    }
  },

  render() {
    return (
      <Modal visible={this.state.modalVisible} animationType={"slide"} onRequestClose={() => this.onClosePress()}>
        <View style={styles.mainContainer}>
          <View style={styles.modalHeader}>
            <View style={styles.modalHeaderTitleContainer}>
              <Image source={require('../../../img/icon-search-blue.png')} style={{marginRight:10}} />
              <Text style={styles.modalHeaderTitle}>SEARCH</Text>
            </View>
            <TouchableOpacity onPress={this.onClosePress} style={styles.btnClose}>
              <Image source={require('../../../img/btn-close-blue.png')} style={styles.btnCloseIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.loginScrollContainer}>
            <View style={styles.modalPaddedContainer}>

            {this.getContent()}

            </View>
          </View>
        </View>

        <Animated.View style={[styles.pickerCover, {opacity: this.state.pickerCoverAnim, width:width, height:this.state.pickerCoverHeight}]}>
        <TouchableOpacity
          onPress={() => this.togglePicker()}
          style={{flex:1}}
        />
        </Animated.View>

        <Animated.View style={[styles.pickerContainer, {bottom: this.state.pickerAnim}]}>
          <View style={styles.view320}>
            <Picker style={styles.pickerBottom}
              selectedValue={this.state.selectedTime}
              onValueChange={this.onValueChange.bind(this, 'selectedTime')}>
              <Item label="No limit" value="All" />
              <Item label="less than 60m" value="< 60m" />
              <Item label="less than 50m" value="< 50m" />
              <Item label="less than 40m" value="< 40m" />
              <Item label="less than 30m" value="< 30m" />
              <Item label="less than 20m" value="< 20m" />
              <Item label="less than 10m" value="< 10m" />
            </Picker>
          </View>
        </Animated.View>

      </Modal>
  )},

  getContent() {
    if (NetworkStatus.isOnline()) {
      return (
        <View>
        <View style={[styles.textInput, {alignSelf:'stretch', marginBottom:10}]}>
          <TextInput
            style={styles.inputFieldFW}
            placeholder={'Enter a tag'}
            placeholderTextColor={Colors.defaultInputText}
            onChangeText={(text) => this.compareTags(text)}
            onFocus={(text) => this.compareTags('')}
            value={this.state.tag}
            ref='tagInput'
            underlineColorAndroid='transparent'
          />
        </View>

        <View style={{marginTop:0, marginBottom:0}}>
            {
              this.state.currentTags.map((tag, i) => {
                return (
                  <View style={[styles.tagContainer, {alignSelf:'stretch'}]} key={i}>
                    <View style={styles.tag}>
                      <Text style={styles.smallText}>{tag}</Text>
                    </View>
                    <TouchableOpacity style={styles.deleteTag} onPress={() => this.onDeleteTagPress(tag)}>
                      <Text style={styles.smallText}>x</Text>
                    </TouchableOpacity>
                  </View>
                )
              })
            }
        </View>

        <TouchableOpacity
          onPress={() => this.togglePicker()}
          underlayColor={Colors.underlayColor}
          style={[styles.listElement, {marginBottom:0, borderBottomWidth:0}]}
          >
          <View style={styles.listElementInner}>
            <View style={styles.listElementLeft}>
              <Text style={styles.mediumText}>
                Mix length
              </Text>
            </View>
            <View style={styles.listElementRight}>
              <Text style={styles.mediumText}>{this.state.selectedTime}</Text>
              <Image source={require('../../../img/icon-arrow.png')} style={styles.listArrow} />
            </View>
          </View>
        </TouchableOpacity>

        <Button
          text={'SEARCH'}
          onPress={this.onSearchPress}
          opacity={this.state.loading ? 0.5 : 1}
          disabled={this.state.loading ? true : false}
        />

        <ScrollView
          style={[styles.tagOverlayContainer, {left:this.state.overlayPosition, top: 50, height: this.state.tagHolderHeight, paddingBottom:10}]}
          keyboardShouldPersistTaps={'always'}
        >
          {
            this.state.matchingTags.map((tag, i) => {
              return (
                <TouchableOpacity style={{marginBottom:20}} key={i} onPress={() => this.onTagPress(tag)}>
                  <Text style={styles.defaultText}>{tag}</Text>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
        </View>
      )
    } else {
      return (
        <View style={styles.containerGeneric}>
          <View style={styles.loadingContainer}>
            <View style={{flexDirection:'row', marginTop:10}}>
              <Image source={require('../../../img/icon-offline.png')} />
              <Text style={styles.offlineText}>Not available offline</Text>
            </View>
          </View>
        </View>
      )
    }
  },

  compareTags(text) {
    this.setState({tag: text, overlayPosition: width});
    //reset matching tags array
    this.state.matchingTags.splice(0, this.state.matchingTags.length);
    var tmpText = text.toLowerCase();
    var allTags = this.state.allTags;
    for (var i=0; i<allTags.length; i++) {
      //console.log(allTags[i], tmpText);
      var tmpCompareText = allTags[i].toLowerCase();
      //console.log(tmpCompareText.indexOf(tmpText))
      if (tmpCompareText.indexOf(tmpText) > -1) {
        //console.log("match");
        this.state.matchingTags.push(allTags[i]);
      }
    }
    // if there is a match, show overlay
    if (this.state.matchingTags.length > 0) {
      this.setState({overlayPosition: width <= 375 ? -10 : -20});
    }
    //console.log(this.state.matchingTags)
  },

  getCurrentTags() {
    return this.state.currentTags.map(function(tag, i){
      //console.log(tag.name);
      return (
        <View style={[styles.tagContainer, {alignSelf:'stretch'}]} key={i}>
          <View style={styles.tag}>
            <Text style={styles.smallText}>{tag}</Text>
          </View>
          <TouchableOpacity style={styles.deleteTag}>
            <Text style={styles.smallText}>x</Text>
          </TouchableOpacity>
        </View>
      )
    });
  },

  onTagPress(tag) {
    //console.log(tag);
    if (this.state.currentTags.indexOf(tag) == -1) {
      this.state.currentTags.push(tag);
      // keep track of the tag ID for creating mix
      this.state.currentTagsIDs.push(this.state.allTagIDs[this.state.allTags.indexOf(tag)]);
      this.setState({overlayPosition: width, loading: false});
    }
  },

  onDeleteTagPress(tag) {
    var index = this.state.currentTags.indexOf(tag);
    if (index !== -1) {
        this.state.currentTags.splice(index, 1);
        // keep track of the tag ID for creating mix
        this.state.currentTagsIDs.splice(index, 1);
        this.setState({overlayPosition: width, loading: false});
    }
  },

  togglePicker() {

    if(this.refs.tagInput.isFocused()) {
      this.refs.tagInput.blur();
    }

    let initialCoverValue = this.state.pickerOpen ? 1 : 0,
        finalCoverValue = this.state.pickerOpen ? 0 : 1;
    let initialPickerValue = this.state.pickerOpen ? 0 : -216,
        finalPickerValue = this.state.pickerOpen ? -216 : 0;

    this.state.pickerCoverAnim.setValue(initialCoverValue);
    Animated.spring(
      this.state.pickerCoverAnim,
      {
        toValue: finalCoverValue
      }
    ).start();

    this.state.pickerAnim.setValue(initialPickerValue);
    Animated.spring(
      this.state.pickerAnim,
      {
        toValue: finalPickerValue
      }
    ).start();

    this.setState({
      pickerCoverHeight: this.state.pickerOpen ? 0 : height,
      pickerOpen: !this.state.pickerOpen,
    });

  },

  onValueChange: function(key: string, value: string) {
    const newState = {};
    newState[key] = value;
    this.setState(newState);
  },

  onSearchPress() {

    var secondsSelected = 0;
    if (this.state.selectedTime.indexOf('<') != -1) {
      var secondsSelectedString = this.state.selectedTime.slice(2, -1);
      secondsSelected = parseInt(secondsSelectedString) * 60;
    } else {
      secondsSelected = 9999;
    }

    API.search(this.state.currentTagsIDs, secondsSelected)
      .then((responseJson) => {
        if(responseJson.error == 0) {
          //console.log(responseJson);
          this.setState({modalVisible: false});
          Actions.searchResults({searchData:responseJson.data});
        } else {
          // Unknown error
          console.log('UNKNOWN ERROR:');
          console.warn(responseJson);
        }
      })
      .catch((error) => {
        // Request sending/parsing error
        console.log('ERROR CATCH:');
        //console.warn(error);
      });
  },

  onClosePress() {
    Actions.pop();
  },

});

import React from 'react';
import { PropTypes } from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import {
  Actions,
  ActionConst
} from 'react-native-router-flux';
import Button from 'react-native-button';

const contextTypes = {
  drawer: React.PropTypes.object,
};

const propTypes = {
  name: PropTypes.string,
  sceneStyle: View.propTypes.style,
  title: PropTypes.string,
};

var styles = require('../../common/styles');

const DrawerView = (props, context) => {
  const drawer = context.drawer;
  return (
    <View style={[styles.drawerContainer, props.sceneStyle ]}>
      <View style={styles.drawerLine}>
        <TouchableOpacity style={styles.drawerImgContainer}
          onPress={() => {
            drawer.close();
            Actions.DASHBOARD({type: ActionConst.RESET});
            Actions.refresh({trending: false});
          }
        }>
          <Image source={require('../../../img/icon-nav-dashboard.png')} />
        </TouchableOpacity>
        <Button style={styles.drawerLink}
          onPress={() => {
            drawer.close();
            Actions.DASHBOARD({type: ActionConst.RESET});
            Actions.refresh({trending: false});
          }
        }>PLAY</Button>
      </View>
      <View style={styles.drawerLine}>
        <TouchableOpacity style={styles.drawerImgContainer}
          onPress={() => {
            drawer.close();
            Actions.PROFILE({type: ActionConst.RESET});
          }
        }>
          <Image source={require('../../../img/icon-nav-profile.png')} />
        </TouchableOpacity>
        <Button style={styles.drawerLink}
          onPress={() => {
            drawer.close();
            Actions.PROFILE({type: ActionConst.RESET});
          }
        }>TRACK</Button>
      </View>
      <View style={styles.drawerLine}>
        <TouchableOpacity style={styles.drawerImgContainer}
          onPress={() => {
            drawer.close();
            Actions.CHALLENGES({type: ActionConst.RESET});
        }}>
          <Image source={require('../../../img/icon-nav-challenges.png')} />
        </TouchableOpacity>
        <Button style={styles.drawerLink}
          onPress={() => {
            drawer.close();
            Actions.CHALLENGES({type: ActionConst.RESET});
          }
        }>COMPETE</Button>
      </View>
      <View style={styles.drawerLine}>
        <TouchableOpacity style={styles.drawerImgContainer}
          onPress={() => {
            drawer.close();
            Actions.MIXINGDESK({type: ActionConst.RESET});
          }
        }>
          <Image source={require('../../../img/icon-nav-mixingdesk.png')} />
        </TouchableOpacity>
        <Button style={styles.drawerLink}
          onPress={() => {
            drawer.close();
            Actions.MIXINGDESK({type: ActionConst.RESET});
          }
        }>MIX</Button>
      </View>
    </View>
  );
};

/*
<View style={[styles.drawerLine, {borderBottomWidth: 0, paddingBottom: 10}]}>
<View style={styles.drawerSubLinkContainer}>
  <Button style={styles.drawerSubLink}
    onPress={() => {
      drawer.close();
      Actions.DASHBOARD({type: ActionConst.RESET});
      Actions.refresh({trending: true});
    }
  }>TRENDING MIXES</Button>
</View>
<Button style={styles.drawerSubLink}
  onPress={() => {
    drawer.close();
    Actions.DASHBOARD({type: ActionConst.RESET});
    Actions.refresh({trending: true});
  }
}>TRENDING CHANNELS</Button>
*/

DrawerView.contextTypes = contextTypes;
DrawerView.propTypes = propTypes;

export default DrawerView;

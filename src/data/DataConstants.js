var countries = [
  {id: 0, name: 'United Kingdom', code: 'UK', phoneCode: '44', uri: require('../../img/flag-UK.png')},
  {id: 1, name: 'United States', code: 'US', phoneCode: '01', uri: require('../../img/flag-US.png')},
  {id: 2, name: 'Germany', code: 'DE', phoneCode: '49', uri: require('../../img/flag-DE.png')},
  {id: 3, name: 'Brazil', code: 'BR', phoneCode: '55', uri: require('../../img/flag-BR.png')},
  {id: 4, name: 'United Arab Emirates', code: 'UAE', phoneCode: '971', uri: require('../../img/flag-UAE.png')},
  {id: 5, name: 'Australia', code: 'AUS', phoneCode: '61', uri: require('../../img/flag-AUS.png')},
  {id: 6, name: 'New Zealand', code: 'NZ', phoneCode: '64', uri: require('../../img/flag-NZ.png')},
  {id: 7, name: 'Spain', code: 'ES', phoneCode: '34', uri: require('../../img/flag-ES.png')},
  {id: 8, name: 'France', code: 'FR', phoneCode: '33', uri: require('../../img/flag-FR.png')},
  {id: 9, name: 'Italy', code: 'IT', phoneCode: '39', uri: require('../../img/flag-IT.png')},
  {id: 10, name: 'Denmark', code: 'DN', phoneCode: '45', uri: require('../../img/flag-DN.png')},
  {id: 11, name: 'Rest of the World', code: 'W', phoneCode: '', uri: require('../../img/flag-W.png')}
];

module.exports = {
  getCountries() {
    return countries;
  },
  getCountry(index) {
    return countries[index];
  }
}

import {
  NativeModules,
  Platform,
  Alert
} from 'react-native';

const InAppBilling = require("react-native-billing");
const { InAppUtils } = NativeModules
import iapReceiptValidator from 'iap-receipt-validator';

var API = require('../../communication/api');
var AsyncStorage = require('../../communication/AsyncStorage');
var NetworkStatus = require('../../utils/NetworkStatus');
var UserProxy = require('../../communication/UserProxy');

const USER_DATA_KEY = '@UserData:user';

exports.getUser = async function() {
  if (NetworkStatus.isOnline()) {
    // We're online get user data
    console.log("Online, all good");
    return _sendGetUserRequest();

  } else {
    // We're offline
    console.log("Looks like we're offline");
    return _tryToGetStoredUserData();
  }

  return null;
};


var _sendGetUserRequest = async function() {
  return API.user()
    .then((responseJson) => {
      //console.log("_sendGetUserRequest =>", responseJson);
      if (responseJson.error == 0) {
        // getSubscriptionStatus from App Stores
        if (Platform.OS === 'ios') {
            return getiOSSubscriptionStatus()
              .then(async function(receiptData) {
                const isSubscribed = await validate(receiptData);
                //console.log('isSubscribed',isSubscribed);
                responseJson.data.isSubscribed = isSubscribed
                //console.log(responseJson.data);
                const hasUpdated = await sendSubscriptionStatus(isSubscribed);
                //console.log(hasUpdated);
                return await storeUserData(responseJson.data);

              })
              .catch(async function(error){
                console.log(error)
                responseJson.data.isSubscribed = false;
                const hasUpdated = await sendSubscriptionStatus(false);
                return await storeUserData(responseJson.data)
              })
        } else {
          InAppBilling.open()
            .then(() => InAppBilling.isSubscribed('subscription.1m'))
            .then((isSubscribed) => {
              //console.log("isSubscribed", isSubscribed);
              responseJson.data.isSubscribed = isSubscribed;
              InAppBilling.close();
            }, (error) => {
              console.log(error)
              InAppBilling.close();
            });
          // Store userData object for the future
          return AsyncStorage.storeObject(USER_DATA_KEY, responseJson.data)
            .then(() => {
              UserProxy.storeUserData(responseJson.data);
              return responseJson.data;
            })
        }
      } else {
        return null;
      }

    })
    .catch((error) => {
      console.log('USER: ERROR CATCH:', error);
    });
}

var getiOSSubscriptionStatus = async function() {
  return new Promise(function (resolve, reject) {
    InAppUtils.receiptData(function (error, receiptData) {
      if(error) {
        console.log('itunes Error', 'Receipt not found.');
        return reject(error);
      } else {
        //console.log('send to validation server');
        return resolve(receiptData);
      }
    })
  })
}

var validate = async function(receiptData) {
  //console.log('validate')
  const password = 'd6886b9101bd4098ab3d8f6c0013444e'
  const production = false;
  const validateReceipt = iapReceiptValidator(password, production);
  try {
    const validationData = await validateReceipt(receiptData);
    var totalReceipts = validationData.latest_receipt_info.length;
    var dateNow = Date.now();
    // check if Auto-Renewable Subscription is still valid
    //console.log()
    var lastReceipt = validationData.latest_receipt_info.length - 1;
    if (validationData.latest_receipt_info[lastReceipt].expires_date_ms > dateNow) {
      console.log('Subscribed')
      return true;
    } else {
      console.log('Subscription ended')
      return false;
    }
  } catch(err) {
    console.log(err.valid, err.error, err.message)
    return false;
  }
}

var _tryToGetStoredUserData = async function() {
  var userData = await AsyncStorage.getObject(USER_DATA_KEY);
  console.log("offline user");
  console.log(userData);
  if(userData !== null) {
    UserProxy.storeUserData(userData);
    return userData;
  }

  return null;
}

var storeUserData = async function(responseData) {
  return AsyncStorage.storeObject(USER_DATA_KEY, responseData)
    .then(() => {
      UserProxy.storeUserData(responseData);
      return responseData;
    })
}

var sendSubscriptionStatus = async function(status) {
  return API.userUpdateSubscription(status)
    .then((responseJson) => {
      console.log(responseJson)
      if (responseJson.error == 0) { return true } else { return false }
    })
    .catch((error) => { return false } )
}

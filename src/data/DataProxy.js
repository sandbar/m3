var _token = null;

module.exports = {
  getToken: function() {
    //console.log('user proxy get token =', _token)
    return _token;
  },

  storeToken: function(token) {
    //console.log('user proxy token =', token)
    _token = token;
  }
}

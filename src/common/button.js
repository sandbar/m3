import React from 'react';
import {
  Text,
  TouchableHighlight
} from 'react-native';

import styles from './styles';
import Colors from './colors';

const Button = (props) => {

  const {opacity, stretch, width, onPress, text} = props;

  return (
    <TouchableHighlight
     style={[styles.button, {opacity:opacity, alignSelf:stretch, width:width}]}
     underlayColor={Colors.brandGreen}
     onPress={onPress}
    >
      <Text style={styles.buttonText}>{text}</Text>
    </TouchableHighlight>
  )
};

export default Button;

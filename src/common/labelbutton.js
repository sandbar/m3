import React from 'react';
import {
  Text,
  TouchableHighlight
} from 'react-native';

import Colors from './colors';

const LabelButton = (props) => {
  return (
    <TouchableHighlight
      underlayColor={Colors.brandGreen}
      onPress={props.onPress}
      >
      <Text style={props.btnStyle}>{props.text}</Text>
    </TouchableHighlight>
  )
};
export default LabelButton;

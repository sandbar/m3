'use strict';

let Colors = {
  brandGreen: '#5bd2ab',
  brandLightGreen: '#c3e8e4',
  defaultText: '#333333',
  darkBlue: '#3b4f6d',
  greyText: '#626262',
  inputBG: '#f0faf6',
  inputBGerror: '#fee5e5',
  defaultInputText: '#7f7f7f',
  errorText: '#ff0000',
  fbBlue: '#3c5a96',
  disabledGreyText: '#b7b7b7',
  disabledGrey: '#dbdbdb',
  underlayColor: 'rgba(91,210,171,0.1)',
  black50pc: 'rgba(0,0,0,0.5)',
  tagBG: '#f0f0f0',
  brandLightGreen50pc: 'rgba(195,232,228,0.5)'
};

export default Colors;

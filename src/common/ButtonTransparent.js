import React, { Component } from 'react';
import {
  Text,
  TouchableHighlight
} from 'react-native';

import styles from './styles';
import Colors from './colors';

const ButtonTransparent = (props) => {

  const {opacity, stretch, width, onPress, text} = props;

  return (
    <TouchableHighlight
     style={[styles.buttonTransparent, {opacity:opacity, alignSelf:stretch, width:width}]}
     underlayColor={Colors.brandGreen}
     onPress={onPress}
    >
      <Text style={styles.buttonTextAlt}>{text}</Text>
    </TouchableHighlight>
  )
};

export default ButtonTransparent;

// module.exports = React.createClass({

//   setNativeProps(nativeProps) {
//     this._root.setNativeProps(nativeProps);
//   },

//   render() {
//     return (
//       <TouchableHighlight
//         style={[styles.buttonTransparent, {opacity:this.props.opacity, alignSelf:this.props.stretch, width:this.props.width}]}
//         underlayColor={Colors.brandGreen}
//         onPress={this.props.onPress}
//         ref={component => this._root = component} {...this.props}
//         >
//         <Text style={styles.buttonTextAlt}>{this.props.text}</Text>
//       </TouchableHighlight>
//     )
//   }
// });

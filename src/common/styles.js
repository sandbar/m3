import React, { Component } from 'react';
import {
  Navigator,
  PixelRatio,
  StyleSheet,
  Platform,
  Dimensions
} from 'react-native';

import Colors from './colors';

const {width, height} = Dimensions.get('window');

// create different padding sizes for tablet and phones based on iPhone breakpoints
// const PADDING = width <= 375 ? 10 : 10;
const PADDING = width < 768 ? 10 : 30;
const isiPhoneX = (Platform.OS === 'ios') && (height === 812) ? true : false;
const iPhoneXMargin = (Platform.OS === 'ios') && (height === 812) ? 20 : 0;

// fonts
const FontMedium = 'Gotham-Medium';
const FontBook = 'Gotham-Book';

module.exports = StyleSheet.create({

  /** DEFAULT **/

  containerGeneric: {
    flex: 1,
  },
  mainContainer:{
    flex:1,
    backgroundColor: '#FFF',
  },
  mainContainerUnderHeader: {
    //marginTop: 60 + (iPhoneXMargin * 2),
    flex:1,
    backgroundColor: '#FFF',
  },
  paddedContainer: {
    padding: PADDING,
    flex:1,
  },
  bottomContainer: {
    padding: PADDING,
    alignItems: 'center'
  },
  inputField: {
    //width: 300,
    width: width - PADDING * 2,
    height: 40,
    padding: 10,
    alignSelf: 'center',
    backgroundColor: Colors.inputBG,
    fontSize: 15,
    fontFamily: FontBook,
    borderWidth: 0,
    color: Colors.defaultText
  },
  inputFieldMultiline: {
    padding: 10,
    paddingTop: 20,
    height: 200,
    backgroundColor: Colors.inputBG,
    fontSize: 15,
    fontFamily: FontBook,
    borderWidth: 0,
    color: Colors.defaultText
  },
  inputFieldError: {
    width: width - PADDING * 2,
    height: 40,
    padding: 10,
    alignSelf: 'center',
    backgroundColor: Colors.inputBGerror,
    fontSize: 15,
    fontFamily: FontBook,
    color: Colors.defaultText
  },
  inputFieldFW: {
    height: 40,
    padding: 10,
    backgroundColor: Colors.inputBG,
    fontSize: 15,
    alignSelf:'stretch',
    fontFamily: FontBook,
    color: Colors.defaultText
  },
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.brandGreen,
    //marginBottom: 24,
  },
  textInputError: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.errorText,
    marginBottom: 5,
  },
  errorContainer: {
    //width: 300,
    width: width - PADDING * 2,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 5,
  },
  errorText: {
    color: Colors.errorText,
    fontSize: 11,
    marginRight: 5,
  },
  button: {
    height: 50,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: Colors.brandGreen,
    padding: 5,
  },
  buttonText: {
    color: Colors.brandGreen,
    fontSize: 15,
    //fontWeight: 'bold',
    fontFamily: FontMedium,
  },
  buttonTextAlt: {
    color: '#FFFFFF',
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: FontMedium,
  },
  buttonTransparent: {
    height: 50,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: '#ffffff',
    padding: 5,
  },
  buttonTextSmall: {
    color: Colors.brandGreen,
    fontSize: 12,
    fontWeight: 'bold',
  },
  defaultText: {
    color: Colors.defaultText,
    fontSize: 12,
    fontFamily: FontBook,
  },
  defaultTextRight: {
    color: Colors.defaultText,
    fontSize: 12,
    alignSelf: 'flex-end',
    fontFamily: FontBook,
  },
  mediumText: {
    color: Colors.defaultText,
    fontSize: 15,
    fontFamily: FontBook,
  },
  header1: {
    fontSize: 15,
    fontFamily: FontMedium,
    color: Colors.defaultText,
  },
  smallText: {
    fontSize: 11,
    fontFamily: FontBook,
    color: Colors.defaultText,
  },
  modalHeader: {
    height: 60 + (iPhoneXMargin * 2),
    backgroundColor: Colors.brandLightGreen,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: PADDING,
    paddingTop: PADDING + (iPhoneXMargin * 2),
  },
  modalHeaderTitleContainer: {
    flex:1,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent: 'center',
    marginLeft: 30,
  },
  modalHeaderTitle: {
    fontFamily: FontMedium,
    color: Colors.darkBlue,
    textAlign: 'center',
    fontSize: 15,
  },
  modalPaddedContainer: {
    padding: PADDING,
  },
  modalClose: {
    position: 'absolute', 
    top: 30 + (iPhoneXMargin * 2), 
    right: 30
  },
  headerTitle: {
    fontFamily: FontMedium,
    color: '#FFF',
    flex: 1,
    textAlign: 'center',
    fontSize: 15,
  },
  btnClose: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnCloseIcon: {
    width: 10,
    height: 10,
  },
  list: {
    paddingLeft: PADDING / 2,
    paddingRight: PADDING / 2
  },
  listElement: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.disabledGreyText,
    height: 50,
    //justifyContent: 'flex-start',
    paddingLeft: PADDING,
    paddingRight: PADDING,
    flexDirection: 'row',
    alignItems: 'center',
  },
  listElementInner: {
    flex:1,
    justifyContent:'center',
    flexDirection:'row',
  },
  listElementInnerLeft: {
    flex:1,
    justifyContent:'flex-start',
    flexDirection:'row',
  },
  listElementLeft: {
    flex: 3,
    justifyContent: 'center',
  },
  listElementRight: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flex: 1,
    alignItems: 'center',
  },
  listArrow: {
    marginLeft: 5,
  },
  listSeparator: {
    height: 1,
    backgroundColor: Colors.disabledGreyText
  },
  loadingContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  offlineText: {
    fontFamily: FontMedium,
    fontSize: 15,
    color: '#898989',
    marginLeft: 6,
  },

  /** NAVBAR **/

  navBar: {
    backgroundColor:Colors.brandGreen,
    borderBottomWidth: 0,
    //height: 60 + (iPhoneXMargin * 2),
  },
  navBarTitle:{
    marginTop: (Platform.OS === 'ios') ? 1 : 1,
    color:'#FFFFFF',
    fontSize: 15,
    fontFamily: FontMedium,
  },
  barButtonTextStyle:{
    color:'#FFFFFF'
  },
  barButtonIconStyle:{
    paddingTop:0,
    marginBottom: 14,
    marginLeft: (Platform.OS === 'ios') ? PADDING : 10,
    marginTop: (Platform.OS === 'ios') ? 0 + (iPhoneXMargin * 3.8) : 20,
  },
  leftButtonIconStyle: {
    tintColor: 'rgba(255,255,255,1)',
    top: (Platform.OS === 'ios') ? -10 - (iPhoneXMargin * 2) : -18,
    left: PADDING - 4
  },
  rightButtonIconStyle: {
    top: (Platform.OS === 'ios') ? -10 + (iPhoneXMargin * 2) : 2,
  },
  backButton: {
    left: (Platform.OS === 'ios') ? 20 : 0
  },

  /** DRAWER **/

  drawerContainer: {
    //flex: 1,
    //justifyContent: 'flex-start',
    //alignItems: 'flex-start',
    backgroundColor: '#333333',
    //paddingTop: iPhoneXMargin * 2
    //padding: PADDING * 2,
  },
  drawerLine: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(255,255,255,0.3)',
    alignSelf: 'stretch',
    paddingTop: 20,
    paddingBottom: 20,
    alignItems: 'center',
  },
  drawerImgContainer: {
    //marginRight: PADDING,
    width: 50,
    alignItems: 'center',
  },
  drawerLink: {
    fontSize: 15,
    fontFamily: FontMedium,
    color: '#FFFFFF',
    margin: 0,
    padding: 0,
    marginLeft: -20
  },
  drawerSubLinkContainer: {
    alignItems: 'flex-start',
    alignSelf: 'stretch',
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(255,255,255,0.3)',
  },
  drawerSubLink: {
    //flex: 1,
    fontSize: 12,
    fontFamily: FontMedium,
    color: '#FFFFFF',
    marginLeft: 55,
    paddingTop: 10,
    paddingBottom: 10,
  },

  /** LOGIN **/

  splashIconsContainer: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: PADDING + 50
  },
  splashIconRow: {
    flexDirection: 'row',
    marginBottom: 20
  },
  splashIcon: {
    width: 70,
    alignItems: 'center',
    justifyContent: 'center'
  },
  splashIconsText: {
    color: '#ffffff',
    backgroundColor: 'transparent',
    fontFamily: FontBook,
    fontSize: 30
  },
  splashButtonContainer: {
    position: 'absolute',
    bottom: PADDING + iPhoneXMargin + 50,
    width: width - (2 * PADDING)
  },


  /*m3logo: {
    width: 260,
    height: 37,
    marginBottom: 40,
    //marginTop: 10,
  },*/
  /*loginScrollContainer: {
    flex: 1,
  },*/
  /*loginMainContainer: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    //flex: 1,
  },*/
  loginBottomContainer: {
    position: 'absolute',
    bottom: 0,
    padding: PADDING + iPhoneXMargin,
    width: width,
    //alignItems: 'center'
  },
  loginBottomContainerButtons: {
    flexDirection: 'row',
    marginBottom: 10,
  },

  loginVideoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  loginLogo: {
    position: 'absolute',
    width: width,
    top: 70,
    alignItems: 'center'
  },
  countryChoiceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  phoneCodePlus: {
    fontFamily: FontBook,
    color: Colors.defaultInputText,
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 10,
    top: 10
  },
  countryRowContainer: {
    flexDirection: 'row',
    padding: 20
  },
  loginInputCode: {
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },

  view320: {
    width: 320,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  loginChooseAltButton: {
    flex: 1,
    height: 50,
    backgroundColor: Colors.brandGreen,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
  },
  loginChooseButton: {
    flex: 1,
    height: 50,
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderColor: Colors.brandGreen,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
  },
  swiper: {
    height: height * 0.5,
  },
  swiperRect: {
    width: width * 0.5,
    height: height * 0.25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  swiperText: {
    color: '#FFF',
    marginTop: 10,
    fontFamily: FontBook,
  },

  slide1: {
  },
  slide2: {
    flex: 1,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  slideText: {
    color: '#333333',
    fontSize: 20,
    fontFamily: FontBook,
    alignSelf: 'center',
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    overflow: 'hidden',
  },
  slideImage: {
    flex: 1,
  },

  /** SIGNUP **/

  paging: {
    marginTop: 10,
    //color: '#FFF',
  },
  picker: {
    alignSelf: 'center',
    width: 80,
  },
  pickerBG: {
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 216,
    backgroundColor: Colors.inputBG,
    marginBottom: 20,
  },
  form320: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 320,
    height: 50,
    marginBottom: 20,
  },
  genderButton: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    borderWidth: 2,
    padding: 5,
    height: 50,
    margin: 5,
  },
  genderIcon: {
    marginLeft: 5,
    marginRight: 25,
  },
  genderButtonText: {
    alignSelf: 'center',
  },
  tsandcsTextContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 20,
  },
  termsContainer: {
    padding: PADDING,
  },

  /** PROFILE **/

  profileHeader: {
    height: 150,
    backgroundColor: Colors.brandLightGreen,
    alignSelf: 'stretch',
  },
  profileHeaderMainRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 6,
  },
  followerButton: {
    width: 75,
    height: 75,
    borderRadius: 38,
    borderWidth: 1,
    borderColor: Colors.brandGreen,
    alignItems: 'center',
    justifyContent: 'center',
  },
  followerAmount: {
    fontSize: 18,
    fontFamily: FontMedium,
    color: Colors.defaultText
  },
  followerType: {
    fontSize: 11,
    fontFamily: FontBook,
    color: Colors.defaultText
  },
  profileAvatar: {
  },
  profileAvatarImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  profileUsername: {
    alignSelf: 'center',
    fontFamily: FontMedium,
    color: Colors.defaultText
  },
  profileTabContent: {
    flex:1,
    alignItems: 'center',
    padding: PADDING,
  },
  profileTabContentVideo: {
    flex:1,
    alignItems: 'center',
    backgroundColor: '#fff'
    //padding: PADDING * 0.5,
  },
  tabBar: {
    backgroundColor: Colors.brandLightGreen,
  },
  tabBarUnderlineStyle: {
    backgroundColor: Colors.defaultText,
  },
  tabBarTextStyle: {
    fontSize: 15,
    fontFamily: FontBook,
    color: Colors.defaultText,
    margin: 0
  },
  profileRow: {
    flexDirection:'row',
    //flex:1,
    //alignSelf: 'stretch',
    //alignItems: 'center',
  },
  profileRowLeft: {
    flex:1,
    marginTop: 12,
  },
  profileRowRight: {
    //flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 12,
  },
  profileEditText: {
    fontSize: 11,
    fontFamily: FontBook,
    color: Colors.defaultText,
    marginRight: 6,
  },
  profileBar: {
    flex:1,
    flexDirection: 'row',
    height: 5,
    backgroundColor: 'rgba(195,232,228,0.5)',
    borderRadius: 3,
    marginTop: 8,
  },
  profileBarTop: {
    height: 5,
    borderRadius: 3,
  },
  editAvatar: {
    position: 'absolute',
    right: 100,
    top: 40,
  },
  fitCheckQuestionNum: {
    width: 100,
    height: 100,
    borderRadius: 50,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: Colors.brandGreen,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  currentQuestionNum: {
    fontSize: 36,
    fontFamily: FontBook,
  },
  totalQuestionNum: {
    fontSize: 18,
    paddingTop: 11,
    fontFamily: FontBook,
  },
  fitCheckTitle: {
    fontFamily: FontMedium,
    fontSize: 18,
    paddingTop: 6,
    paddingBottom: 6,
  },
  fitCheckBody: {
    fontFamily: FontBook,
    lineHeight: 24,
  },
  fitCheckBottomContainer: {
      height: 100,
      padding: PADDING,
  },
  followerAvatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: Colors.brandLightGreen
  },
  followerInfo: {
    justifyContent: 'center',
    marginLeft: 10,
  },
  pickerCover: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    width: width,
    height: height,
    position: 'absolute',
    top:0,
    overflow: 'hidden',
  },
  pickerContainer: {
    backgroundColor: '#FFF',
    position: 'absolute',
    bottom: 0,
    width: width,
    overflow: 'hidden',
    alignItems: 'center',
  },
  pickerBottom: {
    alignSelf: 'center',
    width: 300,
  },
  profilePlayList: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: width,
    padding:0,
    //backgroundColor: 'blue',
    //flex:1
  },
  profilePlayItem: {
    width: Math.floor(width * 0.5)-1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 175,
    margin:0.5,
    overflow: 'hidden',
  },
  profilePlayCircle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 110,
    height: 110,
    borderRadius: 55,
    marginBottom: 16,
  },
  playItemHoursText: {
    fontFamily: FontMedium,
    fontSize: 30,
    color: Colors.defaultText,
  },

  /*** VIDEO LIST ***/

  videoList: {
    //justifyContent: 'flex-start',
    //alignItems: 'flex-start',
    //flexDirection: 'row',
    //flexWrap: 'wrap',
    width: width,
    padding:0,
  },
  videoContainer: {
    width: Math.floor((width * 0.5) - PADDING),
    overflow: 'hidden',
    margin: (PADDING * 0.5),
    backgroundColor: '#FFF'
  },
  videoThumb: {
    width: Math.floor((width * 0.5) - PADDING),
    height: Math.floor(((width * 0.5) - PADDING) / 1.778),
  },
  videoLength: {
    right: 5,
    bottom: 5,
    position: 'absolute',
    backgroundColor: Colors.black50pc,
    padding: 2,
  },
  videoLengthText: {
    color: '#FFF',
    fontSize: 11,
    fontFamily: FontBook,
  },
  videoPlus: {
    position: 'absolute',
    right: 5,
    top: 5
  },
  videoTitle: {
    fontSize: 18,
    fontFamily: FontBook,
    color: Colors.defaultText,
    marginTop: 5,
    letterSpacing: -1,
  },
  videoInfo: {
    fontSize: 11,
    fontFamily: FontBook,
    color: Colors.defaultText,
    marginTop: 5,
  },
  videoHiddenRow: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  videoDelete: {
    alignItems: 'center',
    bottom: (PADDING * 0.5),
    justifyContent: 'center',
    position: 'absolute',
    top: (PADDING * 0.5),
    width: 75,
    right: (PADDING * 0.5),
    backgroundColor: '#FF0000'
  },

  /*** VIDEO PLAYBACK ***/

  videoPlayer: {
    width: width,
    //height: Math.floor(width / 1.778),
    height: width * (9/16)
  },
  videoPlayerFS: {
    // position: 'absolute',
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
    // flex: 1
    width: height,
    height: width,
  },
  videoPlayerContainer: {
    //flex:1,
    backgroundColor: 'black',
    height: Math.floor(width / 1.778),
  },
  videoPlayerContainerFS: {
    backgroundColor: 'black',
    flex:1,
    // position: 'absolute',
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
  },
  videoPlayPauseContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  videoPlayPauseContainerFS: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  videoPlayPauseIcon: {

  },
  videoPlayPauseIconFS: {
    //alignSelf: 'center',
    //marginTop: 200,
  },
  videoPlusMix: {
    position: 'absolute',
    left: PADDING/2,
    top: PADDING/2,
  },
  videoControls: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 32,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: PADDING,
    paddingRight: PADDING,
  },
  fullscreenButton: {
    position: 'absolute',
    bottom: 10,
    right: 10
  },
  controlOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#000000c4',
    backgroundColor: Colors.black50pc,
    justifyContent: 'space-between',
  },  
  videoControlsFS: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 32,
    backgroundColor: Colors.black50pc,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: PADDING,
    paddingRight: PADDING,
  },
  videoControlOption: {
    color: '#FFF',
  },
  videoProgress: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    marginLeft: 10,
    marginRight: 10,
  },
  progressBar: {
    alignSelf: 'stretch',
    flex:1,
  },
  innerProgressCompleted: {
    height: 1,
    backgroundColor: Colors.brandGreen,
  },
  innerProgressRemaining: {
    height: 1,
    backgroundColor: '#FFF',
  },
  videoTime: {
    color: '#FFF',
    width: 50,
    textAlign: 'center',
  },
  mixTotal: {
    position: 'absolute',
    right: PADDING/2,
    top: PADDING/2,
    width: 44,
    height: 18,
    backgroundColor: Colors.black50pc,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mixTotalFS: {
    position: 'absolute',
    right: PADDING/2,
    top: -(44 + PADDING),
    width: 44,
    height: 18,
    backgroundColor: Colors.black50pc,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mixTotalText: {
    fontSize: 11,
    fontFamily: FontBook,
    color: '#FFF',
  },

  /** PROGRESS BAR **/
  videoProgressTimeText: {
    color: '#FFF',
    fontFamily: FontBook,
    fontSize: 13,
    width: 42
  },
  videoProgressBarView: {
    alignSelf: 'stretch',
    flex: 1,
    justifyContent: 'center',
    marginBottom: 12,
    //backgroundColor: 'red'
  },
  videoProgressLine: {
    height: 1,
    padding: 0,
    //position: 'absolute'
  },
  videoProgressHolder: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: Colors.brandGreen,
    position: 'absolute'
    //marginTop: -10,
  },
  videoProgressActiveHolder: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: Colors.brandGreen,
    position: 'absolute'
    //marginTop: -10,
  },

  videoDetailsContainer: {
    padding: PADDING,
    position: 'relative',
    backgroundColor: '#fff'
  },
  videoDetailsContainerFS: {
    position: 'absolute',
    top: width * 2,
    left: -height * 2,
    width: 0,
    backgroundColor: '#000'
  },
  viewVideoTitle: {
    fontSize: 24,
    fontFamily: FontBook,
    color: Colors.defaultText,
    letterSpacing: -1,
  },
  userContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  userInfo: {
    justifyContent: 'center',
    marginLeft: 10,
  },
  followButton: {
    backgroundColor: Colors.inputBG,
    borderRadius: 4,
    paddingTop: 4,
    paddingRight: 10,
    paddingBottom: 4,
    paddingLeft: 10,
    marginTop: 8,
    alignItems: 'center',
  },
  followButtonOn: {
    backgroundColor: Colors.brandGreen,
    borderRadius: 4,
    paddingTop: 4,
    paddingRight: 10,
    paddingBottom: 4,
    paddingLeft: 10,
    marginTop: 8,
    alignItems: 'center',
  },
  followText: {
    color: Colors.defaultText,
    fontSize: 11,
    fontFamily: FontBook,
  },
  followTextOn: {
    color: '#FFF',
    fontSize: 11,
    fontFamily: FontBook,
  },
  videoActionButtons: {
    flexDirection: 'row',
    marginTop: 10,
  },
  videoActionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 30,
  },
  videoDescription: {
    lineHeight: 18,
    fontSize: 12,
    fontFamily: FontBook,
    color: Colors.defaultText,
    marginTop: 10,
  },
  panelIcon: {
    color: Colors.brandGreen,
    fontWeight: 'bold',
    paddingRight: 6,
  },
  panelTitle: {
    flexDirection: 'row',
    paddingTop: 12,
    paddingBottom: 12,
    alignItems: 'center'
  },
  panelTitleText: {
    fontSize: 12,
    fontFamily: FontMedium,
  },
  slidingPanelContainer: {
    overflow: 'hidden',
    //borderBottomWidth: 1,
    borderBottomColor: Colors.disabledGreyText,
  },
  clipsList: {
    //width: width - PADDING,
    padding:0,
  },
  clipsContainer: {
    flexDirection: 'row',
    marginBottom: PADDING,
    backgroundColor: '#FFF',
    overflow: 'hidden'
  },
  clipDetail: {
    width: (width * 0.5) - (PADDING * 2),
  },
  clipThumb: {
    width: Math.floor((width * 0.5) - PADDING),
    height: Math.floor(((width * 0.5) - PADDING) / 1.778),
    marginRight: PADDING,
  },
  clipTitle: {
    fontFamily: FontMedium,
  },
  clipHiddenRow: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  clipDelete: {
    alignItems: 'center',
    bottom: PADDING,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    right: 0,
    backgroundColor: '#FF0000'
  },
  breakdownRowStyleOff: {
    opacity: 0.5,
  },
  breakdownRowStyleOn: {
    opacity: 1,
  },
  ratingStarsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  star: {
    margin: 2,
  },
  ratingContainer: {
    flexDirection: 'row',
    flex:1,
  },
  ratingInfo: {
    flex:1,
    marginBottom: PADDING,
  },
  ratingAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    overflow: 'hidden',
    marginRight: PADDING,
    borderWidth: 1,
    borderColor: Colors.brandLightGreen
  },
  ratingRater: {
    fontSize: 12,
    fontFamily: FontBook,
    color: Colors.defaultText,
    marginTop: 4,
    height: 18
  },
  ratingDescription: {
    overflow: 'visible',
    lineHeight: 18,
    fontSize: 12,
    fontFamily: FontBook,
    color: Colors.defaultText,
  },
  tagContainer: {
    flexDirection: 'row',
    //paddingBottom: 
  },
  tag: {
    backgroundColor: Colors.tagBG,
    paddingTop: 5,
    paddingRight: 10,
    paddingBottom: 5,
    paddingLeft: 10,
    borderRadius: 4,
    marginRight: 4,
  },
  relatedTitleText: {
    fontSize: 12,
    fontFamily: FontMedium,
    marginTop: 18,
    marginBottom: 18,
  },
  videoListRelated: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    //flexWrap: 'wrap',
    width: width - (PADDING * 2),
    padding:0,
  },
  videoContainerRelated: {
    width: Math.floor((width * 0.5) - (PADDING * 1)),
    overflow: 'hidden',
    marginBottom: PADDING,
  },
  videoThumbRelated: {
    width: Math.floor((width * 0.5) - PADDING * 2),
    height: Math.floor(((width * 0.5) - PADDING * 2) / 1.778),
  },
  rateMainContainer: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoDetailsStyle: {
    marginBottom: 200,
    flex: 1,
    backgroundColor: 'red'
  },

  /*** MIXING DESK ***/

  fsHint: {
    flex: 1,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fsHintText: {
    fontSize: 15,
    color: '#FFF',
    marginBottom: 30,
    fontFamily: FontBook,
  },
  fsHintButton: {
    width: 300,
    height: 50,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    padding: 5,
  },
  mixingDeskTitle: {
    fontFamily: FontMedium,
    marginTop: 20,
    marginBottom: 12,
    marginLeft: PADDING,
    color: Colors.defaultText,
  },
  newMixElement: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: Colors.disabledGreyText,
    height: 50,
    alignItems: 'center',
    padding: PADDING,
  },
  loadingBar: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    height: 30,
    backgroundColor: Colors.brandLightGreen,
  },
  loadingBarTop: {
    height: 30,
    backgroundColor: Colors.brandGreen,
    justifyContent: 'center',
    padding: 2,
    overflow: 'hidden',
    alignItems: 'flex-end',
  },
  loadingBarBot: {
    height: 30
  },
  loadingBarText: {
    color: '#FFF',
    overflow: 'hidden',
  },
  tagOverlayContainer: {
    position: 'absolute',
    top: 70,
    width: width-(PADDING*2),
    marginLeft: PADDING,
    marginRight: PADDING,
    paddingLeft: PADDING,
    alignSelf: 'stretch',
    backgroundColor: '#FFF',
    borderBottomWidth: 1,
    borderBottomColor: Colors.disabledGreyText,
  },
  deleteTag: {
    backgroundColor: Colors.tagBG,
    paddingTop: 5,
    paddingRight: 10,
    paddingBottom: 5,
    paddingLeft: 10,
    borderRadius: 4,
    marginRight: 4,
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
  encoding: {
    height: 15,
    width: Math.floor(width * 0.5) - PADDING,
    backgroundColor: Colors.brandGreen,
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  sortRowStyle: {
    opacity: 0.6
  },

  /*** CHALLENGES ***/

  challengeContainer: {
    backgroundColor: Colors.brandLightGreen50pc,
    alignSelf: 'stretch',
    height: 80,
    marginBottom: 5,
    flexDirection: 'row',
    paddingLeft: 10,
    alignItems: 'center',
  },
  challengeThumb: {
    marginRight: 10,
    width: 53,
    height: 64,
  },
  challengeActivity: {
    fontFamily: FontMedium,
    color: Colors.defaultText,
    fontSize: 12,
  },
  challengeTitle: {
    color: Colors.defaultText,
    marginTop: 3,
    marginBottom: 3,
  },
  challengeList: {
    marginTop: 5,
  },
  challengeElement: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: Colors.disabledGreyText,
    height: 70,
    alignItems: 'center',
    padding: PADDING,
  },
  challengeNumCircle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: Colors.brandLightGreen,
    alignItems: 'center',
    justifyContent: 'center',
  },
  challengeAvatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    overflow: 'hidden',
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 1,
    borderColor: Colors.brandLightGreen
  },
  challengeInfoContainer: {
    flexDirection: 'row'
  },
  challengeNum: {
    color: Colors.defaultText,
    fontSize:12,
    fontFamily: FontMedium,
  },
  challenger: {
    color: Colors.defaultText,
    fontSize: 14,
    fontFamily: FontBook,
  },
  challengeInfo: {
    color: Colors.defaultText,
    fontSize: 11,
    marginRight: 10,
    fontFamily: FontBook,
  },
  challengeHeader: {
    backgroundColor: Colors.brandLightGreen,
    alignSelf: 'stretch',
    padding: PADDING,
  },
  challengeDescription: {
    marginTop: 10,
    marginBottom: 10,
  },
  challengeDescriptionArrow: {
    position: 'absolute',
    right: 0,
    top: 2,
  },
  challengeDescriptionText: {
    fontSize: 11,
    color: Colors.defaultText,
    marginRight: 10,
    marginBottom: 10,
    fontFamily: FontBook,
  },
  challengeJoinButton: {
    backgroundColor: Colors.brandGreen,
    borderRadius: 4,
    width: 100,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  challengeJoinButtonText: {
    color: '#FFF',
    fontSize: 12,
    fontFamily: FontMedium,
  },
  challengeJoinedButton: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    width: 100,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  challengeJoinedButtonText: {
    color: Colors.brandGreen,
    fontSize: 12,
    fontFamily: FontMedium,
  },

  /* SUBSCRIBE */

  subscribeBottomContainer: {
    margin: PADDING,
    alignItems: 'center',
    position: 'absolute',
    bottom: 20,
  }


});
